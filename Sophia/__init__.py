def launch_sophia_in_chimera():
    from .simulation import SophiaSimulationFactory
    from .simulator import ThreadedSophiaSimulator
    from .chimeraUI import ChimeraUI
    from .recipe import RecipeFactory
    from .logging import SophiaLogger
    from .mmtk_patcher import patch_mmtk

    patch_mmtk()
    simulation_factory = SophiaSimulationFactory(
                            recipe_factory=RecipeFactory())
    simulator = ThreadedSophiaSimulator()
    ui = ChimeraUI()
    logger = SophiaLogger()
    mediator = SophiaMediator(simulation_factory, simulator, ui, logger)

class SophiaMediator(object):
    """
    SophiaMediator encapsulates how the user interface and the simulator interact,
    to avoid tight coupling between them.
    """
    def __init__(self, simulation_factory, simulator, user_interface, logger):
        self.simulation_factory = simulation_factory
        self.simulation = None
        self.simulator = simulator
        self.ui = user_interface
        self.logger = logger
        self.simulator.connect_to_mediator(self)
        self.ui.connect_to_mediator(self)
        self.universe_args = None
        self.logger.sophia_start()
        self.loaded_file_prefix = None

    def reset_dialogs(self):
        self.ui.reset_dialogs()

    def get_box_length(self):
        if self.simulation:
            return self.simulation.get_box_length()
        else:
            return 10.0

    # ===========================
    # = Main GUI Event Listener =
    # ===========================
    def new_simulation(self):
        self.simulation = self.simulation_factory.new_simulation()
        self.simulation.connect_to_mediator(self)
        self.reset_dialogs()
        self.ui.status('New simulation initialized')
        self.logger.new_simulation()
        self.loaded_file_prefix = None

    def load_simulation(self, file_prefix):
        try:
            self.simulation = \
                self.simulation_factory.load_simulation(file_prefix, mediator=self)
            self.simulation.connect_to_mediator(self)
            self.reset_dialogs()
            self.ui.display_universe_details(self.simulation.universe)
            # in a loaded sim, the universe is already defined
            # so we lock the universe panel
            self.ui.lock_universe_panel()
            self.ui.status('Universe loaded')
            for ingredient in self.simulation.iter_recipe():
                self.display_new_ingredient(ingredient)
            self.render_new_universe(self.simulation.universe)
            self.render_trajectory()
            self.ui.restore_angles(file_prefix)
            self.ui.restore_bonds(file_prefix)
            self.ui.status('Simulation loaded')
            self.logger.load_simulation(file_prefix)
            self.universe_args = None
            self.loaded_file_prefix = file_prefix
        except Exception, e:
            self.logger.log_exception(e)
            raise

    def save_simulation(self, file_prefix):
        self.logger.save_simulation(file_prefix)
        self.simulation.save(file_prefix)
        self.ui.save_bonds(file_prefix)
        self.ui.save_angles(file_prefix)
        self.ui.status('Simulation saved')

    def quit_sophia(self):
        self.logger.sophia_quit()

    # =====================
    # = UI Event Listener =
    # =====================
    def get_force_fields(self):
        return self.simulation.get_force_fields()

    def load_universe_from_pdb(self, pdb_filename, force_field_name,
                               ff_mod_filename, box_length, cutoff):
        self.logger.load_universe_from_pdb(pdb_filename, force_field_name,
                                           ff_mod_filename, box_length, cutoff)
        try:
            self.simulation.load_universe_from_pdb(
                pdb_filename, force_field_name, ff_mod_filename,
                box_length, cutoff)
            self.ui.status('Universe loaded')
            # after universe is loaded, it can't be changed
            # without starting a new sim (but reset button is allowed)
            self.ui.lock_universe_panel()
            self.universe_args = [pdb_filename, force_field_name,
                                  ff_mod_filename, box_length, cutoff]
        except (RuntimeError, KeyError), e:
            self.logger.log_exception(e)
            self.ui.status('Universe error')

    def reset_universe(self):
        if self.universe_args:
            self.logger.reset_universe()
            self.ui.new_simulation()
            self.load_universe_from_pdb(*self.universe_args)
            self.ui.display_universe_details(self.simulation.universe)
        elif self.loaded_file_prefix:
            self.load_simulation(self.loaded_file_prefix)
        else:
            pass

    def start_simulation(self, mode):
        self.logger.start_simulation(mode)
        self.simulator.start_simulation(self.simulation, mode)

    def stop_simulation(self):
        self.logger.stop_simulation()
        self.simulator.stop_simulation()

    def save_structure(self, frame_number, filename):
        self.logger.save_structure(frame_number, filename)
        self.simulation.save_structure(frame_number, filename)

    def move_ingredient_up(self, ingredient_id):
        self.simulation.move_ingredient_up(ingredient_id)

    def move_ingredient_down(self, ingredient_id):
        self.simulation.move_ingredient_down(ingredient_id)

    def create_new_ingredient(self, algorithm_name):
        self.simulation.create_new_ingredient(algorithm_name)

    def load_recipe(self, filename):
        self.logger.load_recipe(filename)
        recipe = self.simulation_factory.load_recipe(filename)
        self.simulation.set_recipe(recipe)

    def save_recipe(self, filename):
        self.logger.save_recipe(filename)
        self.simulation.save_recipe(filename, reset_ingredient_status=True)

    def clone_ingredient(self, ingredient_id):
        self.simulation.clone_ingredient(ingredient_id)

    def delete_ingredient(self, ingredient_id):
        self.simulation.delete_ingredient(ingredient_id)

    def update_ingredient_parameter(self, ingredient_id, name, value,
                                    value_str=None, is_float=False):
        self.simulation.update_ingredient_parameter(ingredient_id, name, value,
                                                    value_str, is_float)

    def ingredient_selected(self, ingredient_id):
        self.simulation.ingredient_selected(ingredient_id)

    def compute_bond_distances(self, bonds):
        self.simulation.compute_bond_distances(bonds)

    def compute_angle_measures(self, angles):
        self.simulation.compute_angle_measures(angles)

    def set_ingredients_deletability(self, are_deletable):
        self.simulation.set_ingredients_deletability(are_deletable)

    def is_langevin_available(self):
        is_available = self.simulator.is_langevin_available()
        if is_available:
            pass
        else:
            self.logger.langevin_not_available()
        return is_available

    def is_atomic_mc_available(self):
        is_available = self.simulator.is_atomic_mc_available()
        if is_available:
            pass
        else:
            self.logger.atomic_mc_not_available()
        return is_available

    # =============================
    # = Simulation Event Listener =
    # =============================
    def render_new_universe(self, universe):
        self.logger.render_new_universe(universe)
        self.ui.render_new_universe(universe)

    def display_new_ingredient(self, ingredient):
        self.ui.display_new_ingredient(ingredient)

    def hide_deleted_ingredient(self, ingredient_id, ingr_is_done):
        self.ui.hide_deleted_ingredient(ingredient_id, ingr_is_done)

    def refresh_ingredient_labels(self, updated_label_dict):
        self.ui.refresh_ingredient_labels(updated_label_dict)

    def refresh_ingredient_display(self, ingredient_id, name, value):
        self.ui.refresh_ingredient_display(ingredient_id, name, value)

    def display_ingredient_parameters(self, ingredient):
        self.ui.display_ingredient_parameters(ingredient)

    def update_ingredient_position(self, ingredient_id, new_position):
        self.ui.update_ingredient_position(ingredient_id, new_position)

    # =============================
    # = Simulator Event Listener =
    # =============================
    def render_trajectory(self):
        t = self.simulation.get_trajectory()
        self.logger.render_trajectory(t)
        self.ui.render_trajectory(t)
        self.ui.status('Simulation complete')

    def starting_next_epoch(self, epoch_num):
        self.ui.status('Simulating epoch %d...' % epoch_num)

    def simulation_stopped_early(self):
        self.ui.status('Simulation stopped')

    def update_progress(self, msg):
        self.ui.update_progress(msg)
