import json


class JSONReader(object):
    """docstring for JSONReader"""
    def __init__(self):
        pass

    def read(self, recipe_filename, recipe):
        with open(recipe_filename, 'r') as f:
            json.load(f, object_hook=recipe.create_new_ingredient)
        return recipe


class JSONWriter(object):
    """docstring for JSONWriter"""
    def __init__(self):
        pass

    def serialize_ingredient(self, ingredient):
        return ingredient.serialize()

    def write(self, recipe_filename, recipe):
        recipe_ingredients = recipe.get_ingredients()
        with open(recipe_filename, 'w') as f:
            json.dump(recipe_ingredients, f, indent=2,
                      default=self.serialize_ingredient)
