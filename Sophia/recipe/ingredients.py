from copy import deepcopy

# allowed values of 'status' field in ingredients
STATUS = type('status', (object,), {'Initialized': 'Initialized',
              'Running': 'Running', 'Done': 'Done',
              'choices': ['Initialized', 'Running', 'Done']})()
# allowed values of 'algorithms' field in ingredients
ALGORITHMS = type('algorithms', (object,), {'MD': 'MD', 'EM': 'EM',
                                            'LD': 'LD', 'MC': 'MC'})()
# allowed values of 'minimizers' field in ingredients
MINIMIZERS = type('minimizers', (object,), {'SD': 'SD', 'CG': 'CG',
                                            'choices': ['SD', 'CG']})()
# allowed values of 'thermostat' field in ingredients
THERMOSTAT = type('thermostats', (object,), {'Off': 'Off',
                  'Rescale': 'Rescale', 'Nose': 'Nose'})()
# allowed values of 'barostat' field in ingredients
BAROSTAT = type('barostats', (object,), {'Off': 'Off', 'Andersen': 'Andersen'})()
# allowed values of 'initialize_velocities' field in ingredients
VELOCITY = type('velocity', (object,), {'Off': 'Off', 'x': 'x',
                                        'xy': 'xy', 'xyz': 'xyz'})()


BASIC_PARAMS = {
    "algorithm": ALGORITHMS.MD,
    "status": STATUS.Initialized,
    "num_steps": 10000,
    "output_interval": 100
}

MD_PARAMS = {
    "time_step": 1,
    "remove_translation_interval": 100,
    "remove_rotation_interval": 100,
    "thermostat": THERMOSTAT.Rescale,
    "initial_T": 1.0,
    "final_T": 100.0,
    "thermostat_interval": 50,
    "thermostat_relaxation_time": 0.2,
    "barostat": BAROSTAT.Off,
    "pressure": 1.0,
    "barostat_relaxation_time": 1.5,
    "friction": 0.1,
    "initialize_velocities": VELOCITY.xyz
}

EM_PARAMS = {
    "minimization_type": MINIMIZERS.SD,
    "convergence": -9.0,
    "initial_step_size": 0.1
}

DEFAULT_INGREDIENT_PARAMS = dict(BASIC_PARAMS.items() + MD_PARAMS.items() + \
                                 EM_PARAMS.items()
                            )


class Ingredient(object):
    """docstring for Ingredient"""
    def __init__(self, param_dict):
        if param_dict:
            self.dict = param_dict
        else:
            self.dict = deepcopy(DEFAULT_INGREDIENT_PARAMS)
        self.id = ''
        self.label = ''

    def __str__(self):
        return str(self.dict)

    def get(self, key):
        return self.dict.get(key)

    def set(self, key, value):
        self.dict[key] = value

    def is_md(self):
        return self.get('algorithm') == ALGORITHMS.MD

    def is_em(self):
        return self.get('algorithm') == ALGORITHMS.EM

    def is_ld(self):
        return self.get('algorithm') == ALGORITHMS.LD

    def is_mc(self):
        return self.get('algorithm') == ALGORITHMS.MC

    def is_done(self):
        return self.get('status') == STATUS.Done

    def reset_status(self):
        self.set('status', STATUS.Initialized)

    def init_missing_params_with_default_values(self):
        for key, value in DEFAULT_INGREDIENT_PARAMS.iteritems():
            if self.dict.has_key(key):
                pass
            else:
                self.dict[key] = value

    def serialize(self):
        keys_to_serialize = None
        if self.is_md():
            keys_to_serialize = BASIC_PARAMS.keys() + MD_PARAMS.keys()
        elif self.is_em():
            keys_to_serialize = BASIC_PARAMS.keys() + EM_PARAMS.keys()
        elif self.is_ld():
            keys_to_serialize = BASIC_PARAMS.keys() + MD_PARAMS.keys()
        elif self.is_mc():
            keys_to_serialize = BASIC_PARAMS.keys() + \
                                ['initial_step_size', 'initial_T', 'final_T']
        else:
            raise RuntimeError("Unexpected algorithm %s" % \
                               self.get('algorithm'))
        
        serialized_ingr = {k: self.dict[k] for k in keys_to_serialize}
        return serialized_ingr
