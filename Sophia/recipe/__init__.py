from os.path import join, exists
from uuid import uuid4
from copy import copy, deepcopy

from .ingredients import Ingredient
from .json_io import JSONReader, JSONWriter

RECIPE_FILENAME = 'recipe.json'

class RecipeFactory(object):
    """docstring for RecipeFactory"""

    class MissingRecipeError(Exception):
        """Exception raised for trying to load a recipe that doesn't exist.

        Attributes:
            filename -- recipe filename
            message  -- explanation of the error
        """
        def __init__(self, filename):
            self.filename = filename
            self.message = "No recipe at %s" % filename
    
    def __init__(self):
        self.reader = JSONReader()
        self.writer = JSONWriter()

    def new_recipe(self):
        r = Recipe(self.writer)
        return r

    def load_recipe_from_dir(self, file_prefix):
        recipe_filename = file_prefix + '_' + RECIPE_FILENAME
        recipe = self.load_recipe_from_file(recipe_filename)
        return recipe

    def load_recipe_from_file(self, recipe_filename):
        if not exists(recipe_filename):
            raise self.MissingRecipeError(recipe_filename)
        recipe = self.new_recipe()
        recipe = self.reader.read(recipe_filename, recipe)
        return recipe


class Recipe(object):
    """docstring for Recipe"""
    def __init__(self, writer):
        self.writer = writer
        self.ingredients_list = []
        self.ingredients_dict = {}
        self.done_ingredients_are_deletable = False

    def __iter__(self):
        for ingredient in self.ingredients_list:
            yield ingredient

    def copy(self):
        r = Recipe(self.writer)
        r.ingredients_list = copy(self.ingredients_list)
        r.ingredients_dict = copy(self.ingredients_dict)
        r.done_ingredients_are_deletable = self.done_ingredients_are_deletable
        return r

    def iter_unfinished_ingredients(self):
        for ingredient in self.ingredients_list:
            if ingredient.is_done():
                pass
            else:
                yield ingredient

    def save_to_dir(self, file_prefix):
        filename = file_prefix + '_' + RECIPE_FILENAME
        self.save_to_file(filename, reset_ingredient_status=False)

    def save_to_file(self, filename, reset_ingredient_status):
        if reset_ingredient_status:
            # set status of all ingredients to Initialized before saving to file
            copied_recipe = self.copy()
            for ingredient in copied_recipe:
                ingredient.reset_status()
            self.writer.write(filename, copied_recipe)
        else:
            # keep status of all ingredients
            self.writer.write(filename, self)

    def move_ingredient_up(self, ingredient_id):
        ingr = self.ingredients_dict[ingredient_id]
        ingr_list = self.ingredients_list
        current_position = ingr_list.index(ingr)
        new_position = current_position - 1
        if ingr.is_done() or new_position < 0 or \
           ingr_list[new_position].is_done():
            # Done ingredients are part of the simulation's history
            # and cannot be moved. Or, if the ingredient is not
            # done but is already first, it can't be moved any
            # higher. Also, we can't swap positions with a Done
            # ingredient.
            return current_position
        else:
            ingr_list[current_position], ingr_list[new_position] = \
                ingr_list[new_position], ingr_list[current_position]
            return new_position

    def move_ingredient_down(self, ingredient_id):
        ingr = self.ingredients_dict[ingredient_id]
        ingr_list = self.ingredients_list
        current_position = ingr_list.index(ingr)
        new_position = current_position + 1
        if ingr.is_done() or new_position >= len(ingr_list) or \
           ingr_list[new_position].is_done():
            # Done ingredients are part of the simulation's history
            # and cannot be moved. Or, if the ingredient is not
            # done but is already last, it can't be moved any
            # lower. Also, we can't swap positions with a Done
            # ingredient.
            return current_position
        else:
            ingr_list[new_position], ingr_list[current_position] = \
                ingr_list[current_position], ingr_list[new_position]
            return new_position

    def create_new_ingredient(self, ingredient_data):
        new_ingredient = Ingredient(ingredient_data)
        new_ingredient.id = uuid4().hex
        new_ingredient.label = len(self.ingredients_list) + 1
        new_ingredient.init_missing_params_with_default_values()
        self.ingredients_list.append(new_ingredient)
        self.ingredients_dict[new_ingredient.id] = new_ingredient
        return new_ingredient

    def clone_ingredient(self, ingredient_id):
        ingr = self.ingredients_dict[ingredient_id]
        cloned_ingredient = deepcopy(ingr)
        cloned_ingredient.id = uuid4().hex
        cloned_ingredient.label = len(self.ingredients_list) + 1
        cloned_ingredient.reset_status()
        self.ingredients_list.append(cloned_ingredient)
        self.ingredients_dict[cloned_ingredient.id] = cloned_ingredient
        return cloned_ingredient

    def delete_ingredient(self, ingredient_id):
        ingr = self.ingredients_dict[ingredient_id]
        id_num = self.ingredients_list.index(ingr)
        ingr_is_done = ingr.is_done()
        ingr_was_deleted = False
        if self._ingredient_can_be_deleted(ingr):
            self.ingredients_list.remove(ingr)
            self.ingredients_dict[ingredient_id] = None
            ingr_was_deleted = True
        else:
            ingr_was_deleted = False
        return ingr_was_deleted, ingr_is_done, id_num

    def _ingredient_can_be_deleted(self, ingredient):
        # Done ingredients are part of the simulation's history
        # and cannot be deleted, unless that lock has been
        # removed.
        if ingredient.is_done():
            return self.done_ingredients_are_deletable
        else:
            return True

    def set_ingredients_deletability(self, are_deletable):
        self.done_ingredients_are_deletable = are_deletable

    def update_ingredient_labels(self):
        updated_label_dict = {}
        for i, ingr in enumerate(self.ingredients_list):
            ingr.label = i + 1
            assert ingr.label == self.ingredients_dict[ingr.id].label
            updated_label_dict[ingr.id] = ingr.label
        return updated_label_dict

    def update_ingredient_parameter(self, ingredient_id, name, value):
        ingredient = self.ingredients_dict[ingredient_id]
        if ingredient.is_done():
            return False
        else:
            ingredient.set(name, value)
            return True

    def get_ingredient(self, ingredient_id):
        return self.ingredients_dict[ingredient_id]

    def get_ingredients(self):
        return self.ingredients_list
