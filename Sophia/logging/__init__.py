import logging
from os.path import join, abspath, pardir

sophia_dir = abspath(join(abspath(__file__), pardir, pardir, pardir))
log_file_path = join(sophia_dir, 'sophia_log.txt')

'''
Setup logger
'''
log_level = logging.INFO
logger = logging.getLogger(__name__)
logger.setLevel(log_level)
# create a file handler
handler = logging.FileHandler(log_file_path, mode='a')
handler.setLevel(log_level)
# create a logging format
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s',
                              datefmt='%d-%b-%y %H:%M:%S')
handler.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(handler)


class SophiaLogger(object):
    """Listens to Sophia events and logs them."""

    def __init__(self):
        self.logger = logger

    def log_exception(self, e):
        self.logger.error(e.message)

    def sophia_start(self):
        self.logger.info('Sophia start')

    def sophia_quit(self):
        self.logger.info('Sophia quit')

    # =====================
    # = UI Event Listener =
    # =====================
    def new_simulation(self):
        self.logger.info('New simulation initialized')

    def load_universe_from_pdb(self, pdb_filename, force_field_name,
                               ff_mod_filename, box_length, cutoff):
        msg_str = \
            ('Load universe\n'
             'pdb file: %s\n'
             'force field: %s\n'
             'force field modification: %s\n'
             'box length: %.1f\n'
             'cutoff: %.1f' % (pdb_filename, force_field_name, ff_mod_filename,
                               box_length, cutoff))
        self.logger.info(msg_str)

    def reset_universe(self):
        self.logger.info('Reset universe')

    def start_simulation(self, mode):
        msg_str = 'Start simulation'
        if mode == 'all':
            msg_str += ', run all ingredients'
        elif mode == 'next':
            msg_str += ', run next ingredient'
        else:
            self.logger.warning(('Encountered unexpected simulation mode, %s,'
                                 'while trying to log start_simulation' % mode))

        self.logger.info(msg_str)

    def load_simulation(self, file_prefix):
        self.logger.info('Load simulation %s' % file_prefix)

    def save_simulation(self, file_prefix):
        self.logger.info('Save simulation %s' % file_prefix)

    def stop_simulation(self):
        self.logger.info('Stop simulation')

    def save_structure(self, frame_number, filename):
        self.logger.info('Save structure, frame %d, %s' % (frame_number, filename))

    def load_recipe(self, filename):
        self.logger.info('Load recipe %s' % filename)

    def save_recipe(self, filename):
        self.logger.info('Save recipe %s' % filename)

    def langevin_not_available(self):
        self.logger.warning('Langevin Dynamics module failed to load')

    def atomic_mc_not_available(self):
        self.logger.warning('Monte Carlo module failed to load')

    # =============================
    # = Simulator Event Listener =
    # =============================
    def render_new_universe(self, universe):
        self.logger.debug('Simulation Event: render new universe')

    def render_trajectory(self, trajectory):
        self.logger.debug('Simulation Event: render trajectory')
