from MMTK import Units
from MMTK.Trajectory import TrajectoryOutput

from ..exceptions import AtomicMCNotCompiledError

try:
    from .atomic_mc import AtomicMonteCarlo
except:
    raise AtomicMCNotCompiledError('Need to compile atomic_mc module.')


def create_mc_generator(universe, ingredient):
    initial_T = ingredient.get('initial_T') * Units.K
    final_T = ingredient.get('final_T') * Units.K
    initial_step_size = ingredient.get('initial_step_size')
    mc_generator = \
        AtomicMonteCarlo(universe, initial_temperature=initial_T,
                         final_temperature=final_T,
                         step_size=initial_step_size,
                         update_step_size_interval=100)
    return mc_generator

def create_sim_actions(trajectory, ingredient):
    sa = []
    output_interval = ingredient.get('output_interval')
    sa.append(
        TrajectoryOutput(trajectory, ['configuration', 'energy', 'temperature'],
                         first=0, last=None, skip=output_interval)
    )
    return sa

def start_mc_thread(mc_generator, actions, ingredient, is_threaded):
    num_steps = ingredient.get('num_steps')
    thread = mc_generator(steps=num_steps, actions=actions,
                          background=is_threaded)
    return thread

def run_mc(ingredient, universe, epoch, is_threaded):
    mmtk_universe = universe.mmtk_universe
    mmtk_trajectory = epoch.mmtk_trajectory
    mc_generator = create_mc_generator(mmtk_universe, ingredient)
    actions = create_sim_actions(mmtk_trajectory, ingredient)
    thread = start_mc_thread(mc_generator, actions, ingredient, is_threaded)
    return thread
