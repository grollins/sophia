import numpy as N
cimport numpy as N
import cython

cimport MMTK_trajectory_generator
from MMTK import Units, ParticleProperties
from MMTK.Random import gaussian, uniform
import MMTK_trajectory
import MMTK_forcefield

include "MMTK/python.pxi"
include "MMTK/numeric.pxi"
include "MMTK/core.pxi"
include "MMTK/universe.pxi"
include "MMTK/trajectory.pxi"
include "MMTK/forcefield.pxi"


#
# Velocity Verlet integrator
#
cdef class AtomicMonteCarlo(MMTK_trajectory_generator.EnergyBasedTrajectoryGenerator):
    def __init__(self, universe, **options):
        MMTK_trajectory_generator.EnergyBasedTrajectoryGenerator.__init__(
            self, universe, options, "Atomic Metropolis Monte Carlo")
        self.features = []

    default_options = {'steps': 10000, 'step_size': 0.001,
                       'initial_temperature': 1.0,
                       'final_temperature': 100.0,
                       'update_step_size_interval': 100,
                       'background': False, 'threads': None,
                       'mpi_communicator': None, 'actions': []}

    available_data = ['configuration', 'energy', 'temperature']

    restart_data = ['configuration', 'energy', 'temperature']

    # Cython compiler directives set for efficiency:
    # - No bound checks on index operations
    # - No support for negative indices
    # - Division uses C semantics
    @cython.boundscheck(False)
    @cython.wraparound(False)
    @cython.cdivision(True)
    cdef start(self):
        cdef N.ndarray[double, ndim=2] current_x, g
        cdef N.ndarray[double, ndim=1] random_move
        cdef energy_data energy
        cdef double ACCEPTANCE_LOWER_BOUND, ACCEPTANCE_UPPER_BOUND
        cdef double step_size, prev_E, delta_E, acceptance_rate, T
        cdef int natoms, nsteps, step, update_step_size_interval, selected_atom
        cdef int num_accepted

        ACCEPTANCE_LOWER_BOUND = 0.4
        ACCEPTANCE_UPPER_BOUND = 0.6
        num_accepted = 0
        trial_E = 0.0

        # Gather state variables and parameters
        configuration = self.universe.configuration()
        gradients = ParticleProperties.ParticleVector(self.universe)
        step_size = self.getOption('step_size')
        nsteps = self.getOption('steps')
        update_step_size_interval = self.getOption('update_step_size_interval')
        initial_T = self.getOption('initial_temperature')
        final_T = self.getOption('final_temperature')
        natoms = self.universe.numberOfAtoms()

        delta_T_in_K = final_T - initial_T
        if abs(delta_T_in_K) >= 1.0:
            dT_per_step = (1. * delta_T_in_K) / nsteps
        else:
            dT_per_step = 0.0

        # For efficiency, the Cython code works at the array
        # level rather than at the ParticleProperty level.
        current_x = configuration.array
        g = gradients.array
        random_move = N.zeros(3, N.float)
        T = initial_T

        # Force constants and gradients are not requested.
        energy.gradients = <void *>g
        energy.gradient_fn = NULL
        energy.force_constants = NULL
        energy.fc_fn = NULL

        # Declare the variables accessible to trajectory actions.
        self.declareTrajectoryVariable_array(
            g, "gradients", "Energy gradients:\n", energy_gradient_unit_name,
            PyTrajectory_Gradients)
        self.declareTrajectoryVariable_double(
            &energy.energy,
            "potential_energy", "Potential energy: %lf\n",
            energy_unit_name, PyTrajectory_Energy)
        self.declareTrajectoryVariable_double(
            &T, "temperature", "Temperature: %lf\n",
            temperature_unit_name, PyTrajectory_Thermodynamic)
        self.initializeTrajectoryActions()

        # Acquire the write lock of the universe. This is necessary to
        # make sure that the integrator's modifications to positions
        # and velocities are synchronized with other threads that
        # attempt to use or modify these same values.
        #
        # Note that the write lock will be released temporarily
        # for trajectory actions. It will also be converted to
        # a read lock temporarily for energy evaluation. This
        # is taken care of automatically by the respective methods
        # of class EnergyBasedTrajectoryGenerator.
        self.acquireWriteLock()

        self.foldCoordinatesIntoBox()
        self.calculateEnergies(current_x, &energy, 0)
        prev_E = energy.energy
        for step in range(nsteps):
            self.trajectoryActions(step)
            selected_atom = uniform(0, natoms)
            random_move[0] = gaussian(0., step_size)
            random_move[1] = gaussian(0., step_size)
            random_move[2] = gaussian(0., step_size)
            current_x[selected_atom, 0] += random_move[0]
            current_x[selected_atom, 1] += random_move[1]
            current_x[selected_atom, 2] += random_move[2]
            prev_E = energy.energy
            self.calculateEnergies(current_x, &energy, 1)
            delta_energy = energy.energy - prev_E
            if uniform(0, 1) <= N.exp( -delta_energy / (T * Units.k_B) ):
                # if trial configuration meets Boltzmann criterion,
                # save it to trajectory
                num_accepted += 1
                self.foldCoordinatesIntoBox()
            else:
                # if trial configuration does not meet Boltzmann criterion,
                # restore to previous configuration and energy
                energy.energy = prev_E
                current_x[selected_atom, 0] -= random_move[0]
                current_x[selected_atom, 1] -= random_move[1]
                current_x[selected_atom, 2] -= random_move[2]

            T += dT_per_step

            if step > 0 and step % update_step_size_interval == 0:
                acceptance_rate = (1. * num_accepted) / step
                if acceptance_rate < ACCEPTANCE_LOWER_BOUND:
                    step_size *= 0.5
                elif acceptance_rate > ACCEPTANCE_UPPER_BOUND:
                    step_size *= 1.1
                else:
                    pass
            else:
                pass
        # End of for loop

        self.trajectoryActions(nsteps)

        # Release the write lock.
        self.releaseWriteLock()

        # Finalize all trajectory actions (close files etc.)
        self.finalizeTrajectoryActions(nsteps)
