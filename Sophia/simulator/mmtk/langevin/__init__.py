from numpy import array
from Scientific import N
from MMTK import Units, Random, ParticleProperties
from MMTK.Environment import NoseThermostat, AndersenBarostat
from MMTK.Trajectory import TrajectoryOutput
from MMTK.Dynamics import VelocityScaler, TranslationRemover, RotationRemover, \
                          Heater

from .langevin import LangevinIntegrator

def initialize_temperature_and_pressure(universe, ingredient):
    initial_T = ingredient.get('initial_T') * Units.K
    initialize_velocities = ingredient.get('initialize_velocities')
    if initialize_velocities == 'Off':
        pass
    elif initialize_velocities == 'x':
        initialize_x_velocities_only(universe, initial_T)
    elif initialize_velocities == 'xy':
        initialize_xy_velocities_only(universe, initial_T)
    else:
        universe.initializeVelocitiesToTemperature(initial_T)

def initialize_x_velocities_only(universe, initial_T):
    universe.configuration()
    masses = universe.masses()
    if universe._atom_properties.has_key('velocity'):
        del universe._atom_properties['velocity']
    fixed = universe.getParticleBoolean('fixed')
    np = universe.numberOfPoints()
    velocities = N.zeros((np, 3), N.Float)
    for i in xrange(np):
        m = masses[i]
        if m > 0. and not fixed[i]:
            v = Random.randomVelocity(initial_T, m)
            velocities[i] = array([v.x(), 0.0, 0.0])
    universe._atom_properties['velocity'] = \
        ParticleProperties.ParticleVector(universe, velocities)
    universe.adjustVelocitiesToConstraints()

def initialize_xy_velocities_only(universe, initial_T):
    universe.configuration()
    masses = universe.masses()
    if universe._atom_properties.has_key('velocity'):
        del universe._atom_properties['velocity']
    fixed = universe.getParticleBoolean('fixed')
    np = universe.numberOfPoints()
    velocities = N.zeros((np, 3), N.Float)
    for i in xrange(np):
        m = masses[i]
        if m > 0. and not fixed[i]:
            v = Random.randomVelocity(initial_T, m)
            velocities[i] = array([v.x(), v.y(), 0.0])
    universe._atom_properties['velocity'] = \
        ParticleProperties.ParticleVector(universe, velocities)
    universe.adjustVelocitiesToConstraints()

def create_integrator(universe, ingredient):
    time_step_in_ps = ingredient.get('time_step') * Units.fs
    friction = ingredient.get('friction') * universe.masses() / Units.ps
    initial_T = ingredient.get('initial_T') * Units.K
    integrator = \
        LangevinIntegrator(universe, delta_t=time_step_in_ps,
                           friction=friction, temperature=initial_T)
    return integrator

def create_sim_actions(universe, trajectory, ingredient):
    sa = []
    initial_T = ingredient.get('initial_T') * Units.K
    final_T = ingredient.get('final_T') * Units.K
    delta_T_in_K = final_T - initial_T
    thermostat = ingredient.get('thermostat')
    if thermostat == 'Rescale':
        thermostat_interval = ingredient.get('thermostat_interval')
        if abs(delta_T_in_K) >= 1.0:
            time_step_fs = ingredient.get('time_step')
            time_step_ps = time_step_fs * Units.fs
            steps_per_ps = 1./time_step_ps
            num_steps = ingredient.get('num_steps')
            dT_per_step = delta_T_in_K / num_steps
            dT_per_ps = dT_per_step * steps_per_ps
            gradient = dT_per_ps
            sa.append( Heater(initial_T, final_T, gradient, 0, None,
                              thermostat_interval) )
        else:
            sa.append( VelocityScaler(initial_T, 0.1*initial_T, 0, None,
                                      thermostat_interval) )
    elif thermostat == 'Nose':
        relaxation_time = ingredient.get('thermostat_relaxation_time')
        universe.thermostat = NoseThermostat(initial_T, relaxation_time)
    elif thermostat == 'Off':
        pass

    barostat = ingredient.get('barostat')
    if barostat == 'Andersen':
        pressure = ingredient.get('pressure') * Units.atm
        relaxation_time = ingredient.get('barostat_relaxation_time')
        universe.barostat = AndersenBarostat(pressure, relaxation_time)
    else:
        pass

    rti = ingredient.get('remove_translation_interval')
    if rti > 0:
        sa.append( TranslationRemover(0, None, rti) )
    else:
        pass

    rri = ingredient.get('remove_rotation_interval')
    if rri > 0:
        sa.append( RotationRemover(0, None, rri) )
    else:
        pass

    output_interval = ingredient.get('output_interval')
    data_to_record = ("time", "configuration", "energy", "thermodynamic")
    sa.append(
        TrajectoryOutput(trajectory, data=data_to_record,
                         first=0, last=None, skip=output_interval)
    )
    return sa

def start_ld_thread(universe, integrator, actions, ingredient, is_threaded):
    num_steps = ingredient.get('num_steps')
    thread = integrator(steps=num_steps, actions=actions,
                        background=is_threaded)
    return thread

def run_ld(ingredient, universe, epoch, is_threaded):
    mmtk_universe = universe.mmtk_universe
    mmtk_trajectory = epoch.mmtk_trajectory
    initialize_temperature_and_pressure(mmtk_universe, ingredient)
    integrator = create_integrator(mmtk_universe, ingredient)
    actions = create_sim_actions(universe, mmtk_trajectory, ingredient)
    thread = start_ld_thread(mmtk_universe, integrator, actions, ingredient,
                             is_threaded)
    return thread
