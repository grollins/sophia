from MMTK import Units
from MMTK.Trajectory import TrajectoryOutput, StandardLogOutput
from MMTK.Minimization import SteepestDescentMinimizer
from MMTK.Minimization import ConjugateGradientMinimizer


def create_minimizer(universe, ingredient):
    minimization_type = ingredient.get('minimization_type')
    if minimization_type == 'SD':
        minimizer = SteepestDescentMinimizer(universe)
    elif minimization_type == 'CG':
        minimizer = ConjugateGradientMinimizer(universe)
    else:
        minimizer = None
    return minimizer

def create_actions(trajectory, ingredient):
    sa = []
    output_interval = ingredient.get('output_interval')
    data_to_record = ("configuration", "energy", "gradients")
    sa.append(
        TrajectoryOutput(trajectory, data=data_to_record,
                         first=0, last=None, skip=output_interval)
    )
    # sa.append(
    #     StandardLogOutput(output_interval)
    # )
    return sa

def start_em_thread(universe, minimizer, actions, ingredient, is_threaded):
    num_steps = ingredient.get('num_steps')
    step_size_nm = ingredient.get('initial_step_size') * Units.nm
    convergence_kj_mol_nm = 10 ** ingredient.get('convergence')
    thread = minimizer(steps=num_steps, actions=actions, step_size=step_size_nm,
                       convergence=convergence_kj_mol_nm, background=is_threaded)
    return thread

def run_em(ingredient, universe, epoch, is_threaded):
    mmtk_universe = universe.mmtk_universe
    mmtk_trajectory = epoch.mmtk_trajectory
    minimizer = create_minimizer(mmtk_universe, ingredient)
    actions = create_actions(mmtk_trajectory, ingredient)
    thread = start_em_thread(mmtk_universe, minimizer, actions, ingredient, is_threaded)
    return thread
