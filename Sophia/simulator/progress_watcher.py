from chimera import triggers


class ProgressWatcher(object):
    """docstring for ProgressWatcher"""
    def __init__(self, thread, initial_callback, running_callback, done_callback):
        super(ProgressWatcher, self).__init__()
        self.thread = thread
        self.running_callback = running_callback
        self.done_callback = done_callback
        self.update_interval = 10
        self.current_frame = 0
        initial_callback()
        self.handler = triggers.addHandler("check for changes", self.check_progress, None)

    def check_progress(self, triggerName, myData, triggerData):
        self.current_frame += 1
        if self.current_frame < self.update_interval:
            return
        else:
            self.current_frame = 0
            if self.thread.is_alive():
                state = self.thread.copyState()
                self.running_callback(state)
            else:
                if self.handler is not None:
                    triggers.deleteHandler("check for changes", self.handler)
                    self.handler = None
                self.done_callback()
