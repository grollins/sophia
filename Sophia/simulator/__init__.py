from MMTK import Units

from .progress_watcher import ProgressWatcher
from .mmtk import LangevinNotCompiledError, AtomicMCNotCompiledError
from .mmtk import run_md, run_em

try:
    from .mmtk.langevin import run_ld
    LANGEVIN_AVAILABLE = True
except LangevinNotCompiledError:
    run_ld = None
    LANGEVIN_AVAILABLE = False
    print "langevin integrator not available"

try:
    from .mmtk.monte_carlo import run_mc
    ATOMIC_MC_AVAILABLE = True
except AtomicMCNotCompiledError:
    run_mc = None
    ATOMIC_MC_AVAILABLE = False
    print "atomic_mc not available"

SIM_MODE = \
    type('simulation_mode', (object,), {'next': 'next', 'all': 'all'})()


class SophiaSimulator(object):
    """
    The SophiaSimulator turns a Recipe of Ingredients into a Trajectory of Epochs. 
    It sends simulation signals to and receives signals from the SophiaMediator.
    """
    def __init__(self):
        self.md_chef = run_md
        self.em_chef = run_em
        self.ld_chef = run_ld
        self.mc_chef = run_mc
        self.mediator = None

    def connect_to_mediator(self, mediator):
        self.mediator = mediator

    # =====================
    # = UI Event Listener =
    # =====================
    def start_simulation(self, simulation, mode):
        recipe_iterator = \
            enumerate(simulation.iter_unfinished_ingredients())
        recipe_list = list(recipe_iterator)
        if len(recipe_list) > 0 and simulation.is_universe_loaded():
            for i, ingredient in recipe_list:
                self.mediator.starting_next_epoch(i+1)
                self._run_next_ingredient(simulation, ingredient)
                if mode == SIM_MODE.next:
                    # stop after running next ingredient
                    break
                elif mode == SIM_MODE.all:
                    # run all remaining ingredients
                    continue
                else:
                    raise ValueError('Unexpected simulation mode %s' % mode)
            self._finalize()
        else:
            # can't run due to missing ingredients and/or universe
                pass

    def stop_simulation(self):
        pass

    def _run_next_ingredient(self, simulation, ingredient):
        simulation.trajectory.start_new_epoch()
        epoch = simulation.get_current_epoch()
        universe = simulation.get_universe()
        simulation.update_ingredient_parameter(
                ingredient.id, 'status', 'Running')
        if ingredient.is_md():
            self.md_chef(ingredient, universe, epoch, is_threaded=False)
        elif ingredient.is_em():
            self.em_chef(ingredient, universe, epoch, is_threaded=False)
        elif ingredient.is_ld() and self.is_langevin_available():
            self.ld_chef(ingredient, universe, epoch, is_threaded=False)
        elif ingredient.is_mc() and self.is_atomic_mc_available():
            self.mc_chef(ingredient, universe, epoch, is_threaded=False)
        else:
            pass
        simulation.update_ingredient_parameter(ingredient.id, 'status', 'Done')
        simulation.trajectory.finish_current_epoch()

    def _finalize(self):
        self.mediator.update_progress('')
        self.mediator.render_trajectory()

    def is_langevin_available(self):
        return LANGEVIN_AVAILABLE

    def is_atomic_mc_available(self):
        return ATOMIC_MC_AVAILABLE


class ThreadedSophiaSimulator(object):
    """
    The SophiaSimulator turns a Recipe of Ingredients into a Trajectory of Epochs. 
    It sends simulation signals to and receives signals from the SophiaMediator.
    """
    def __init__(self):
        self.md_chef = run_md
        self.em_chef = run_em
        self.ld_chef = run_ld
        self.mc_chef = run_mc
        self.mediator = None
        self.simulation_running = False
        self.coordinator = ThreadedCoordinator(self, self.md_chef, 
                            self.em_chef, self.ld_chef, self.mc_chef)

    def connect_to_mediator(self, mediator):
        self.mediator = mediator

    # =====================
    # = UI Event Listener =
    # =====================
    def start_simulation(self, simulation, mode):
        if self.simulation_running:
            # already running, do nothing
            pass
        else:
            # not running yet
            recipe_iterator = \
                enumerate(simulation.iter_unfinished_ingredients())
            recipe_list = list(recipe_iterator)
            if len(recipe_list) > 0 and simulation.is_universe_loaded():
                self.mediator.update_progress('Simulation running')
                self.simulation_running = True
                self.coordinator.stop_signal_received = False
                self.coordinator.recipe_list = recipe_list
                self.coordinator.ingredient_idx = 0
                self.coordinator._run_next_ingredient(simulation, mode)
            else:
                # can't run due to missing ingredients and/or universe
                pass

    def stop_simulation(self):
        self.coordinator.stop_signal_received = True

    def is_langevin_available(self):
        return LANGEVIN_AVAILABLE

    def is_atomic_mc_available(self):
        return ATOMIC_MC_AVAILABLE


class ThreadedCoordinator(object):
    """docstring for ThreadedCoordinator()"""
    def __init__(self, parent, md_chef, em_chef, ld_chef, mc_chef):
        self.parent = parent
        self.md_chef = md_chef
        self.em_chef = em_chef
        self.ld_chef = ld_chef
        self.mc_chef = mc_chef
        self.stop_signal_received = False
        self.recipe_list = None
        self.ingredient_idx = 0

    def _run_next_ingredient(self, simulation, mode):
        if self.stop_signal_received or self.ingredient_idx >= len(self.recipe_list):
            # user asked us to stop early or there are no more ingredients
            self.parent.simulation_running = False
            self.stop_signal_received = False
            self._finalize()

        elif self.ingredient_idx < len(self.recipe_list):
            # if there is a next ingredient, run it
            i, ingredient = self.recipe_list[self.ingredient_idx]
            assert self.ingredient_idx == i
            self.parent.mediator.starting_next_epoch(self.ingredient_idx+1)
            simulation.trajectory.start_new_epoch()
            epoch = simulation.get_current_epoch()
            universe = simulation.get_universe()
            if ingredient.is_md():
                thread = self.md_chef(ingredient, universe, epoch, is_threaded=True)
                time_step = ingredient.get('time_step')
                def running_cb(state):
                    time_in_ps = state['time']
                    step = time_in_ps / Units.fs * time_step
                    progress_msg = "epoch %d, %d steps" % (self.ingredient_idx+1, step)
                    self.parent.mediator.update_progress(progress_msg)
            elif ingredient.is_em():
                thread = self.em_chef(ingredient, universe, epoch, is_threaded=True)
                def running_cb(state):
                    potential_energy_kj_mol = state['potential_energy']
                    progress_msg = "epoch %d, %.2f kj/mol" % \
                                   (self.ingredient_idx+1, potential_energy_kj_mol)
                    self.parent.mediator.update_progress(progress_msg)
            elif ingredient.is_ld() and self.parent.is_langevin_available():
                thread = self.ld_chef(ingredient, universe, epoch, is_threaded=True)
                time_step = ingredient.get('time_step')
                def running_cb(state):
                    time_in_ps = state['time']
                    step = time_in_ps / Units.fs * time_step
                    progress_msg = "epoch %d, %d steps" % (self.ingredient_idx+1, step)
                    self.parent.mediator.update_progress(progress_msg)
            elif ingredient.is_mc() and self.parent.is_atomic_mc_available():
                thread = self.mc_chef(ingredient, universe, epoch, is_threaded=True)
                def running_cb(state):
                    potential_energy_kj_mol = state['potential_energy']
                    progress_msg = "epoch %d, %.2f kj/mol" % \
                                   (self.ingredient_idx+1, potential_energy_kj_mol)
                    self.parent.mediator.update_progress(progress_msg)
            else:
                thread = None

            def init_cb():
                simulation.update_ingredient_parameter(
                    ingredient.id, 'status', 'Running')
            def done_cb():
                simulation.update_ingredient_parameter(
                    ingredient.id, 'status', 'Done')
                simulation.trajectory.finish_current_epoch()
                self.ingredient_idx += 1
                if mode == SIM_MODE.next:
                    # stop running after completing one
                    self.stop_signal_received = True
                elif mode == SIM_MODE.all:
                    # run next ingredient
                    pass
                else:
                    raise ValueError('Unexpected simulation mode %s' % mode)
                self._run_next_ingredient(simulation, mode)

            # the watcher will trigger the callback functions
            # defined above, at the appropriate times
            pw = ProgressWatcher(
                    thread,
                    initial_callback=init_cb,
                    running_callback=running_cb,
                    done_callback=done_cb
            )

    def _finalize(self):
        self.parent.mediator.update_progress('')
        self.parent.mediator.render_trajectory()
