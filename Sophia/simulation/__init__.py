import atexit
from tempfile import mkdtemp
from shutil import rmtree
from os.path import basename

from .mmtk import MMTKTrajectoryFactory, MMTKUniverseFactory, MMTKGeometer
from .log_writer import LogWriter


class SophiaSimulationFactory(object):
    """docstring for SophiaSimulationFactory"""

    def __init__(self, recipe_factory):
        self.recipe_factory = recipe_factory
        self.universe_factory = MMTKUniverseFactory()
        self.trajectory_factory = MMTKTrajectoryFactory()

    def new_simulation(self):
        temp_dir = mkdtemp()
        atexit.register(rmtree, temp_dir)
        recipe = self.recipe_factory.new_recipe()
        trajectory = self.trajectory_factory.new_trajectory(temp_dir)
        sim = SophiaSimulation(recipe, trajectory, self.universe_factory)
        return sim

    def load_simulation(self, file_prefix, mediator):
        temp_dir = mkdtemp()
        atexit.register(rmtree, temp_dir)
        universe = self.universe_factory.load_universe_from_dir(file_prefix)
        recipe = self.recipe_factory.load_recipe_from_dir(file_prefix)
        trajectory = self.trajectory_factory.load_trajectory(
                        file_prefix, temp_dir, universe)
        sim = SophiaSimulation(recipe, trajectory, self.universe_factory)
        return sim

    def load_recipe(self, filename):
        '''
        The assumption here is that we're trying to apply an
        old, saved recipe to a new simulation. As such, we need
        to reset the status of each ingredient to "Initialized".
        We don't care what the status of each ingredient was when
        the recipe was saved because we're running a new sim now.

        This is different than what we do in load_simulation above.
        There, we retain the status of each ingredient because
        we're loading a recipe from a saved simulation and want to
        show the history of the sim, i.e. which ingredients have
        already been run.
        '''
        recipe = self.recipe_factory.load_recipe_from_file(filename)
        for ingredient in recipe:
            ingredient.reset_status()
        return recipe


class SophiaSimulation(object):
    """docstring for SophiaSimulation"""
    def __init__(self, recipe, trajectory, universe_factory):
        self.recipe = recipe
        self.trajectory = trajectory
        self.universe_factory = universe_factory
        if trajectory.is_linked_to_universe():
            self.universe = trajectory.universe
        else:
            self.universe = None
        self.mediator = None
        self.geometer = MMTKGeometer()

    def connect_to_mediator(self, mediator):
        self.mediator = mediator

    def save(self, file_prefix):
        self.recipe.save_to_dir(file_prefix)
        self.trajectory.save(file_prefix)
        first_epoch_filename = self.trajectory.epoch_basenames[0]
        first_epoch_pdbname =  first_epoch_filename + '.pdb'
        self.universe.save(file_prefix, first_epoch_pdbname)
        self._save_sophia_file(file_prefix)

    def _save_sophia_file(self, file_prefix):
        with open(file_prefix + '.sophia', 'w') as f:
            f.write("%s\n" % basename(file_prefix))

    def get_trajectory(self):
        return self.trajectory

    def get_current_epoch(self):
        return self.trajectory.get_current_epoch()

    def get_universe(self):
        return self.universe

    def get_recipe(self):
        return self.recipe

    def set_recipe(self, recipe):
        self.recipe = recipe
        for ingredient in self.iter_recipe():
            self.mediator.display_new_ingredient(ingredient)

    def iter_recipe(self):
        for ingredient in self.recipe:
            yield ingredient

    def iter_unfinished_ingredients(self):
        for ingredient in self.recipe.iter_unfinished_ingredients():
            yield ingredient

    def is_universe_loaded(self):
        if self.universe is None:
            return False
        else:
            return True

    # =====================
    # = UI Event Listener =
    # =====================
    def get_force_fields(self):
        return self.universe_factory.get_force_fields()

    def load_universe_from_pdb(self, pdb_filename, force_field_name,
                               ff_mod_filename, box_length, cutoff):
        if self.universe:
            raise RuntimeError("Simulation already has a universe.")
        else:
            self.universe = self.universe_factory.load_universe_from_pdb(
                                pdb_filename, force_field_name,
                                ff_mod_filename, box_length, cutoff)
            self.trajectory.initialize_with_universe(self.universe)
            self.trajectory.start_new_epoch()
            self.mediator.render_new_universe(self.universe)

    def save_recipe(self, filename, reset_ingredient_status=False):
        self.recipe.save_to_file(filename, reset_ingredient_status)

    def save_structure(self, frame_number, filename):
        frame = self.trajectory.get_frame(epoch_number, frame_number)
        self.universe.write_pdb(frame.configuration, filename)

    def move_ingredient_up(self, ingredient_id):
        new_position = self.recipe.move_ingredient_up(ingredient_id)
        updated_label_dict = self.recipe.update_ingredient_labels()
        self.mediator.refresh_ingredient_labels(updated_label_dict)
        self.mediator.update_ingredient_position(ingredient_id, new_position)

    def move_ingredient_down(self, ingredient_id):
        new_position = self.recipe.move_ingredient_down(ingredient_id)
        updated_label_dict = self.recipe.update_ingredient_labels()
        self.mediator.refresh_ingredient_labels(updated_label_dict)
        self.mediator.update_ingredient_position(ingredient_id, new_position)

    def create_new_ingredient(self, algorithm_name):
        ingredient_data = {'algorithm': algorithm_name}
        new_ingredient = self.recipe.create_new_ingredient(ingredient_data)
        self.mediator.display_new_ingredient(new_ingredient)

    def clone_ingredient(self, ingredient_id):
        cloned_ingredient = self.recipe.clone_ingredient(ingredient_id)
        self.mediator.display_new_ingredient(cloned_ingredient)

    def delete_ingredient(self, ingredient_id):
        was_deleted, ingr_is_done, ingr_id_num = \
            self.recipe.delete_ingredient(ingredient_id)
        if was_deleted:
            self.trajectory.delete_epoch(ingr_id_num)
            self.mediator.hide_deleted_ingredient(ingredient_id, ingr_is_done)
            updated_label_dict = self.recipe.update_ingredient_labels()
            self.mediator.refresh_ingredient_labels(updated_label_dict)
        else:
            pass

    def update_ingredient_parameter(self, ingredient_id, name, value,
                                    value_str=None, is_float=False):
        was_updated = self.recipe.update_ingredient_parameter(
                        ingredient_id, name, value)
        if was_updated:
            value_to_display = value_str if is_float else value
            self.mediator.refresh_ingredient_display(
                ingredient_id, name, value_to_display)
        else:
            pass

    def ingredient_selected(self, ingredient_id):
        ingredient = self.recipe.get_ingredient(ingredient_id)
        self.mediator.display_ingredient_parameters(ingredient)

    def compute_bond_distances(self, bonds):
        '''
        expected format of bonds
        [[mmtk_atom1, mmtk_atom2], [mmtk_atom1, mmtk_atom2], ...]
        '''
        self.geometer.add_bond_distances_to_trajectory(bonds, self.trajectory)

    def compute_angle_measures(self, angles):
        '''
        expected format of angles
        [[mmtk_atom1, mmtk_atom2, mmtk_atom3], ...]
        '''
        self.geometer.add_angle_measures_to_trajectory(angles, self.trajectory)

    def set_ingredients_deletability(self, are_deletable):
        self.recipe.set_ingredients_deletability(are_deletable)

    def get_box_length(self):
        return self.get_universe().get_box_length()
