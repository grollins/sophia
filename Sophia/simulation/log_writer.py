from numpy import zeros, savetxt

class LogWriter(object):
    """docstring for LogWriter"""
    def __init__(self):
        self.fmt = '%.4e'
        self.delimiter = ','

    def write_data(self, destination, data_columns_to_write, data_types):
        # convert data to structured array format
        num_rows = len(data_columns_to_write[0])
        data_array = zeros((num_rows,), dtype=data_types)
        labels = [dt[0] for dt in data_types]

        for i, label in enumerate(labels):
            data_array[label] = data_columns_to_write[i]

        # save array to file
        header = self.delimiter.join(labels)
        savetxt(destination, data_array, fmt=self.fmt,
                delimiter=self.delimiter, header=header,
                comments='')
