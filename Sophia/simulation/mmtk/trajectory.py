from glob import glob
from os.path import join, basename, dirname
from numpy import zeros_like, arange
from numpy import save as npy_save
from numpy import load as npy_load
from MMTK import Units
from MMTK.Trajectory import Trajectory, TrajectoryOutput
from MMTK.DCD import DCDReader, writeDCD
from MMTK.PDB import PDBOutputFile

from ..log_writer import LogWriter


EPOCH_BASENAME = 'epoch'


class MMTKTrajectoryFactory(object):
    """docstring for MMTKTrajectoryFactory"""

    def __init__(self):
        self.log_writer = LogWriter()

    def new_trajectory(self, working_dir):
        return MMTKTrajectory(working_dir, self.log_writer)

    def load_trajectory(self, file_prefix, working_dir, universe):
        t = MMTKTrajectory(working_dir, self.log_writer, universe)
        t.load_completed_epochs(file_prefix)
        return t


class MMTKTrajectory(object):
    """docstring for MMTKTrajectory"""

    class Frame(object):
        def __init__(self, mmtk_frame):
            self.frame = mmtk_frame

        @property
        def configuration(self):
            return self.frame['configuration']


    def __init__(self, working_dir, log_writer, universe=None):
        self.working_dir = working_dir
        self.universe = universe
        self.epoch_factory = EpochFactory(self.working_dir)
        self.log_writer = log_writer
        self.completed_epochs = []
        self.current_epoch = None

    def initialize_with_universe(self, universe):
        if self.is_linked_to_universe():
            raise RuntimeError("Trajectory already linked to a Universe.")
        else:
            self.universe = universe

    def start_new_epoch(self):
        if not self.is_linked_to_universe():
            raise RuntimeError("Can't start Epoch. No Universe found.")
        else:
            if self.current_epoch:
                self.finish_current_epoch()
            self.current_epoch = \
                self.epoch_factory.new_epoch(
                    self.universe.mmtk_universe, self.num_epochs)

    def load_completed_epochs(self, file_prefix):
        dcd_pattern = file_prefix + '_' + "%s*.dcd" % EPOCH_BASENAME
        dcd_filenames = glob(dcd_pattern)
        dcd_filenames.sort()
        if len(dcd_filenames) == 0:
            raise RuntimeError("No files match %s" % dcd_pattern)
        else:
            for dcd in dcd_filenames:
                epoch = self.epoch_factory.epoch_from_file(
                            self.universe.mmtk_universe, dcd, file_prefix)
                self.completed_epochs.append(epoch)

    def get_current_epoch(self):
        return self.current_epoch

    def finish_current_epoch(self):
        self.current_epoch.finish()
        self.current_epoch.open_for_reading()
        self.completed_epochs.append(self.current_epoch)
        self.current_epoch = None

    def delete_epoch(self, idx):
        if idx < len(self.completed_epochs):
            self.completed_epochs.pop(idx)
        else:
            pass

    def save(self, file_prefix):
        self._export_dcd(file_prefix)
        self._save_logs(file_prefix)
        self._export_energies(file_prefix)
        self._export_environmental_vars(file_prefix)
        self._export_bonds(file_prefix)
        self._export_angles(file_prefix)

    def _export_dcd(self, file_prefix):
        for epoch in self.completed_epochs:
            file_basename = epoch.basename_for_saving
            dcd_filename = file_prefix + '_' + file_basename + '.dcd'
            pdb_filename = file_prefix + '_' + file_basename + '.pdb'
            mmtk_configuration = epoch.mmtk_trajectory.configuration
            occupancy_dict = self.universe.get_occupancy_dict()
            writeDCDPDB(mmtk_configuration, dcd_filename, pdb_filename,
                        occupancy_dict)

    def _save_logs(self, file_prefix):
        for epoch in self.completed_epochs:
            file_basename = epoch.basename_for_saving
            destination = file_prefix + '_' + file_basename + '_log.csv'
            epoch.save_to_log(self.log_writer, destination)

    def _export_energies(self, file_prefix):
        for epoch in self.completed_epochs:
            epoch.export_energies(file_prefix)

    def _export_environmental_vars(self, file_prefix):
        for epoch in self.completed_epochs:
            epoch.export_environmental_vars(file_prefix)

    def _export_bonds(self, file_prefix):
        for epoch in self.completed_epochs:
            epoch.export_bonds(self.log_writer, file_prefix)

    def _export_angles(self, file_prefix):
        for epoch in self.completed_epochs:
            epoch.export_angles(self.log_writer, file_prefix)

    def get_frame(self, epoch_number, frame_number):
        f = self.completed_epochs[epoch_number].frame(frame_number)
        return self.Frame(f)

    def get_epoch_length(self, epoch_number):
        length = None
        try:
            length = len(self.completed_epochs[epoch_number])
        except IndexError:
            length = None
        return length

    def get_cumulative_frame_number(self, epoch_number, frame_number):
        prev_epochs = self.completed_epochs[:epoch_number]
        cumulative_frame_num = sum( [len(t) for t in prev_epochs] )
        cumulative_frame_num += frame_number
        return cumulative_frame_num

    def is_linked_to_universe(self):
        if self.universe:
            return True
        else:
            return False

    def compute_and_save_bond_distances(self, bonds, distance_fcn):
        for b in bonds:
            bond_label = "-".join( ["%s%d" % (atom.name, atom.index) for atom in b] )
            for epoch in self.completed_epochs:
                epoch.compute_and_save_bond_distances(
                    b, bond_label, distance_fcn)

    def compute_and_save_angle_measures(self, angles, measure_fcn):
        for a in angles:
            angle_label = "-".join( ["%s%d" % (atom.name, atom.index) for atom in a] )
            for epoch in self.completed_epochs:
                epoch.compute_and_save_angle_measures(
                    a, angle_label, measure_fcn)

    @property
    def num_frames(self):
        return sum( [len(t) for t in self.completed_epochs] )

    @property
    def num_epochs(self):
        return len(self.completed_epochs)

    @property
    def epoch_filenames(self):
        return [epoch.epoch_filename for epoch in self.completed_epochs]

    @property
    def epoch_basenames(self):
        return [epoch.basename_for_saving for epoch in self.completed_epochs]

    @property
    def energy(self):
        return [epoch.energy for epoch in self.completed_epochs]

    @property
    def temperature(self):
        return [epoch.temperature for epoch in self.completed_epochs]

    @property
    def pressure(self):
        return [epoch.pressure for epoch in self.completed_epochs]

    @property
    def epoch_boundaries(self):
        epoch_boundary_list = []
        for epoch_num in xrange(len(self.completed_epochs)):
            if epoch_num == 0:
                continue
            else:
                n = self.get_cumulative_frame_number(epoch_num, 0)
                epoch_boundary_list.append(n)
        return epoch_boundary_list

    @property
    def bonds(self):
        return [epoch.bonds for epoch in self.completed_epochs]

    @property
    def angles(self):
        return [epoch.angles for epoch in self.completed_epochs]


class EpochFactory(object):
    """docstring for EpochFactory"""
    def __init__(self, working_dir):
        self.working_dir = working_dir

    def new_epoch(self, mmtk_universe, epoch_num):
        epoch_filename = \
            join(self.working_dir, EPOCH_BASENAME + '%02d.nc' % \
             epoch_num)
        log_filename = \
            join(self.working_dir, EPOCH_BASENAME + '%02d_log.csv' % \
                 epoch_num)
        mmtk_trajectory = Trajectory(mmtk_universe, epoch_filename, 'w')
        epoch = Epoch(epoch_filename, log_filename,
                      mmtk_trajectory, mmtk_universe, EPOCH_BASENAME + '%02d' % epoch_num)
        return epoch

    def epoch_from_file(self, mmtk_universe, dcd_filename, file_prefix):
        dcd_basename = basename(dcd_filename).partition('.')[0]
        nc_filename = join(self.working_dir, dcd_basename + '.nc')
        mmtk_trajectory = Trajectory(mmtk_universe, nc_filename, "w")
        actions = [TrajectoryOutput(mmtk_trajectory, "all", 0, None, 1)]
        dcd_reader = DCDReader(mmtk_universe, dcd_file=dcd_filename,
                               actions=actions)
        dcd_reader()
        log_filename = join(dirname(file_prefix), dcd_basename + '.log')
        basename_for_saving = dcd_basename.split(basename(file_prefix) + '_')
        epoch = Epoch(nc_filename, log_filename, mmtk_trajectory, mmtk_universe,
                      basename_for_saving[-1])
        epoch.load_energies(file_prefix)
        epoch.load_environmental_vars(file_prefix)
        return epoch


class Epoch(object):
    def __init__(self, epoch_filename, log_filename, mmtk_trajectory,
                 mmtk_universe, basename_for_saving):
        self.epoch_filename = epoch_filename
        self.log_filename = log_filename
        self.mmtk_trajectory = mmtk_trajectory
        self.mmtk_universe = mmtk_universe
        self.basename_for_saving = basename_for_saving
        self.is_writable = True
        self._step = None
        self._time = None
        self._potential_energy = None
        self._kinetic_energy = None
        self._temperature = None
        self._pressure = None
        self._bonds = {}
        self._angles = {}

    def __len__(self):
        return len(self.mmtk_trajectory)

    def finish(self):
        self.mmtk_trajectory.flush()
        self.mmtk_trajectory.close()
        self.is_writable = False

    def open_for_reading(self):
        if self.is_writable:
            self.finish()
        else:
            t = Trajectory(self.mmtk_universe, 
                           self.epoch_filename, 'r')
            self.mmtk_trajectory = t

    def frame(self, frame_number):
        return self.mmtk_trajectory[frame_number]

    def load_energies(self, file_prefix):
        file_basename = basename(self.epoch_filename).partition('.')[0]
        load_dir = dirname(file_prefix)
        filename = join(load_dir, file_basename)
        self.potential_energy = npy_load(filename + '_potential.npy')
        self.kinetic_energy = npy_load(filename + '_kinetic.npy')

    def load_environmental_vars(self, file_prefix):
        file_basename = basename(self.epoch_filename).partition('.')[0]
        load_dir = dirname(file_prefix)
        filename = join(load_dir, file_basename)
        self.temperature = npy_load(filename + '_temperature.npy')
        self.pressure = npy_load(filename + '_pressure.npy')

    def export_energies(self, file_prefix):
        file_basename = self.basename_for_saving
        filename = file_prefix + '_' + file_basename
        npy_save(filename + '_potential.npy', self.potential_energy)
        npy_save(filename + '_kinetic.npy', self.kinetic_energy)

    def export_environmental_vars(self, file_prefix):
        file_basename = self.basename_for_saving
        filename = file_prefix + '_' + file_basename
        npy_save(filename + '_temperature.npy', self.temperature)
        npy_save(filename + '_pressure.npy', self.pressure)

    def export_bonds(self, log_writer, file_prefix):
        if self.bonds:
            file_basename = self.basename_for_saving
            filename = file_prefix + '_' + file_basename + '_bonds.csv'
            data_columns_to_write = self.bonds.values()
            data_types = [(k, float) for k in self.bonds.keys()]
            log_writer.write_data(filename, data_columns_to_write, data_types)
        else:
            pass

    def export_angles(self, log_writer, file_prefix):
        if self.angles:
            file_basename = self.basename_for_saving
            filename = file_prefix + '_' + file_basename + '_angles.csv'
            data_columns_to_write = self.angles.values()
            data_types = [(k, float) for k in self.angles.keys()]
            log_writer.write_data(filename, data_columns_to_write, data_types)
        else:
            pass

    def save_to_log(self, log_writer, destination):
        data_columns_to_write = [self.step, self.time,
                                 self.potential_energy, self.kinetic_energy,
                                 self.potential_energy + self.kinetic_energy,
                                 self.temperature, self.pressure]
        data_types = [ ('step', int), ('time (ns)', float),
                       ('potential (kJ/mol)', float),
                       ('kinetic (kJ/mol)', float),
                       ('total (kJ/mol)', float),
                       ('temperature (K)', float),
                       ('pressure (atm)', float) ]
        log_writer.write_data(destination, data_columns_to_write, data_types)

    def compute_and_save_bond_distances(self, bond, bond_label, distance_fcn):
        self.bonds[bond_label] = \
            distance_fcn(self.mmtk_trajectory, self.mmtk_universe, bond)

    def compute_and_save_angle_measures(self, angle, angle_label, measure_fcn):
        self.angles[angle_label] = \
            measure_fcn(self.mmtk_trajectory, self.mmtk_universe, angle)

    @property
    def universe(self):
        return self.mmtk_universe

    @property
    def step(self):
        if self._step is None:
            self._step = arange(0, len(self.mmtk_trajectory))
        return self._step
    @step.setter
    def step(self, value):
        self._step = value

    @property
    def time(self):
        if self._time is None:
            try:
                self._time = self.mmtk_trajectory.time
                self._time /= Units.ns # convert ps to ns
            except AttributeError:
                self._time = zeros_like(self.mmtk_trajectory.potential_energy)
        return self._time
    @time.setter
    def time(self, value):
        self._time = value

    @property
    def potential_energy(self):
        if self._potential_energy is None:
            self._potential_energy = \
                self.mmtk_trajectory.potential_energy
        return self._potential_energy
    @potential_energy.setter
    def potential_energy(self, value):
        self._potential_energy = value
    
    @property
    def kinetic_energy(self):
        if self._kinetic_energy is None:
            try:
                kinetic = self.mmtk_trajectory.kinetic_energy
            except AttributeError:
                kinetic = zeros_like(self.mmtk_trajectory.potential_energy)
            self._kinetic_energy = kinetic
        return self._kinetic_energy
    @kinetic_energy.setter
    def kinetic_energy(self, value):
        self._kinetic_energy = value

    @property
    def temperature(self):
        if self._temperature is None:
            try:
                T = self.mmtk_trajectory.temperature
            except AttributeError:
                T = zeros_like(self.potential_energy)
            self._temperature = T
        return self._temperature
    @temperature.setter
    def temperature(self, value):
        self._temperature = value

    @property
    def pressure(self):
        if self._pressure is None:
            try:
                P = self.mmtk_trajectory.pressure
            except AttributeError:
                P = zeros_like(self.potential_energy)
            self._pressure = P
        return self._pressure
    @pressure.setter
    def pressure(self, value):
        self._pressure = value

    @property
    def energy(self):
        total = self.potential_energy + self.kinetic_energy
        return {'potential': self.potential_energy,
                'kinetic': self.kinetic_energy,
                'total': total}

    @property
    def bonds(self):
        return self._bonds

    @property
    def angles(self):
        return self._angles


def writePDB(universe, configuration, pdb_file_name, occupancy_dict):
    if universe is not None:
        configuration = \
            universe.contiguousObjectConfiguration(None, configuration)
    pdb = PDBOutputFile(pdb_file_name, 'xplor')
    pdb.write(universe, configuration, occupancy_dict=occupancy_dict)
    sequence = pdb.atom_sequence
    pdb.close()
    return sequence

def writeDCDPDB(conf_list, dcd_file_name, pdb_file_name, occupancy_dict,
                delta_t=0.1):
    """
    Write a sequence of configurations to a DCD file and generate
    a compatible PDB file.

    :param conf_list: the sequence of configurations
    :type conf_list: sequence of :class:`~MMTK.ParticleProperties.Configuration`
    :param dcd_file_name: the name of the DCD file
    :type dcd_file_name: str
    :param pdb_file_name: the name of the PDB file
    :type pdb_file_name: str
    :param delta_t: the time step between two configurations
    :type delta_t: float
    """
    universe = conf_list[0].universe
    sequence = writePDB(universe, conf_list[0], pdb_file_name, occupancy_dict)
    indices = map(lambda a: a.index, sequence)
    writeDCD(conf_list, dcd_file_name, 1./Units.Ang, indices, delta_t, 1)
