from os.path import basename
from json import dump as json_dump
from json import load as json_load
from shutil import copyfile
from re import compile

from MMTK.PDB import PDBConfiguration, PDBOutputFile
from MMTK.Universe import CubicPeriodicUniverse, InfiniteUniverse
from MMTK.ForceFields import Amber99ForceField, Amber94ForceField
from MMTK.ForceFields import LennardJonesForceField

from .forcefields import GAFFForceField


MOLECULE_DICT = {'ETH': 'ethane', 'BUT': 'butane', 'NN': 'N2'}

FF_PARAM_EXPORT_NOTES = {
    "nonbonded":
    """
    excluded_pairs and one_four_pairs: lists of pairs of atom indices
    atom_subset: list of atom indices, or None for 'all atoms'
    """,
    "lennard_jones":
    """
    type: array of atom type numbers
    epsilon_sigma: 3D array. The first two indices are the atom type
                   numbers of the two atoms, the third index is 0
                   for epsilon and 1 for sigma
    """,
    "electrostatic":
    """
    charge: array of partial charges
    """,
    "harmonic_distance_term":
    """
    a list of tuples (atom_index1, atom_index2, length, force_constant)
    """,
    "harmonic_angle_term":
    """
    a list of tuples (atom_index1, atom_index2, atom_index3, angle, force_constant)
    """,
    "cosine_dihedral_term":
    """
    a list of tuples (atom_index1, atom_index2, atom_index3, atom_index4,
                      multiplicity, angle, force_constant)
    """,
    }


class MMTKUniverseFactory(object):
    """docstring for MMTKUniverseFactory"""

    FF_DICT = {'Amber99': Amber99ForceField, 'Amber94': Amber94ForceField,
               'Lennard Jones': LennardJonesForceField, 'GAFF': GAFFForceField}

    def __init__(self):
        LJ_pattern = r'(\w+), ([0-9]*\.[0-9]+|[0-9]+), ([0-9]*\.[0-9]+|[0-9]+)'
        self.LJ_mod_regex = compile(LJ_pattern)

    def get_force_fields(self):
        return self.FF_DICT.keys()

    def load_universe_from_pdb(self, pdb_filename, force_field_name,
                               ff_mod_filename, box_length, cutoff):
        ud = UniverseDetails(pdb_filename, force_field_name,
                             ff_mod_filename, box_length, cutoff)
        return self._build(ud)

    def load_universe_from_dir(self, file_prefix):
        ud = UniverseDetails()
        ud.load_from_dir(file_prefix)
        return self._build(ud)

    def _build(self, universe_details):
        force_field_name = universe_details.get('ff')
        ff_mod_filename = universe_details.get('ff_mod')
        pdb_filename = universe_details.get('pdb')
        box_length = universe_details.get('box_length')
        cutoff = universe_details.get('cutoff')
        ff_factory = self.FF_DICT[force_field_name]

        if force_field_name in ['Amber99', 'Amber94', 'GAFF'] and ff_mod_filename:
            ff = ff_factory(lj_options=cutoff, es_options=cutoff,
                            mod_files=[ff_mod_filename,])
        else:
            ff = ff_factory(cutoff=cutoff)

        # TODO: add rigid box ff term here?
        '''
        - LJ is not a compound ff, so might need to implement a compound version of it
        - Amber94, Amber99, and GAFF are all compound b/c they inherit from
          MMForceField, so just need to add box term to self.fflist
        '''

        if box_length <= 0.0:
            mmtk_universe = InfiniteUniverse(ff)
        else:
            mmtk_universe = CubicPeriodicUniverse(box_length, ff)

        with open(str(pdb_filename), 'r') as input:
            configuration = PDBConfiguration(input)
            mmtk_universe.addObject(
                configuration.createAll(molecule_names=MOLECULE_DICT))
            occupancy_dict = self._fix_atoms(configuration, mmtk_universe)

        if force_field_name in ['Lennard Jones'] and ff_mod_filename:
            self._apply_ff_modifications(mmtk_universe, ff_mod_filename)
        else:
            pass

        return MMTKUniverse(mmtk_universe, universe_details, occupancy_dict)

    def _fix_atoms(self, configuration, mmtk_universe):
        occupancy_dict = {}
        for r in configuration.residues:
            for atom in r:
                occupancy_dict[atom.number - 1] = atom.properties['occupancy']
                atom.occupancy = atom.properties['occupancy']
        for o in mmtk_universe:
            for a in o.atomList():
                if occupancy_dict[a.index] == 0.0:
                    a.addProperties({'fixed': 1.0})
        return occupancy_dict

    def _apply_ff_modifications(self, mmtk_universe, ff_mod_filename):
        with open(ff_mod_filename, 'r') as f:
            text = f.read()

        match_dict = {}
        for match in self.LJ_mod_regex.finditer(text):
            results = match.groups()
            atom_symbol = results[0]
            LJ_energy = float(results[1])
            LJ_rmin = float(results[2]) # nm
            LJ_sigma = 2 ** (-1./6) * LJ_rmin
            match_dict[atom_symbol] = {'LJ_energy': LJ_energy,
                                       'LJ_radius': LJ_sigma}

        atoms_to_modify = match_dict.keys()
        for o in mmtk_universe:
            for a in o.atomList():
                if a.symbol in atoms_to_modify:
                    a.LJ_energy = match_dict[a.symbol]['LJ_energy']
                    a.LJ_radius = match_dict[a.symbol]['LJ_radius']


class UniverseDetails(object):
    """docstring for UniverseDetails"""
    UNIVERSE_DETAILS_BASENAME = 'universe_details.json'

    def __init__(self, pdb_filename=None, force_field_name=None,
                 ff_mod_filename=None, box_length=10.0, cutoff=None):
        self.dict = {}
        if pdb_filename:
            self.dict['pdb'] = pdb_filename
        if force_field_name:
            self.dict['ff'] = force_field_name
        if ff_mod_filename:
            self.dict['ff_mod'] = ff_mod_filename
        self.dict['box_length'] = box_length # nm
        self.dict['cutoff'] = cutoff # nm

    def get(self, key):
        return self.dict.get(key, None)

    def load_from_dir(self, file_prefix):
        filename = file_prefix + '_' + self.UNIVERSE_DETAILS_BASENAME
        with open(filename, 'r') as f:
            self.dict = json_load(f)
        self.dict['pdb'] = file_prefix + '_' + self.dict['pdb']
        if self.dict['ff_mod']:
            self.dict['ff_mod'] = file_prefix + '_' + self.dict['ff_mod']

    def save_to_dir(self, file_prefix, pdbname):
        export_dict = {}
        export_dict['box_length'] = self.get('box_length')
        export_dict['cutoff'] = self.get('cutoff')
        destination = basename(pdbname)
        export_dict['pdb'] = destination

        source = self.get('ff_mod')
        export_dict['ff_mod'] = source
        if source:
            ff_mod_basename = basename(source)
            if '_' in ff_mod_basename:
                basename_without_old_prefix = ff_mod_basename.split('_')[1]
                destination = file_prefix + '_' + basename_without_old_prefix
                export_dict['ff_mod'] = basename_without_old_prefix
            else:
                destination = file_prefix + '_' + ff_mod_basename
                export_dict['ff_mod'] = ff_mod_basename
            copyfile(source, destination)

        export_dict['ff'] = self.get('ff')
        filename = file_prefix + '_' + self.UNIVERSE_DETAILS_BASENAME
        with open(filename, 'w') as f:
            json_dump(export_dict, f, indent=2)


class MMTKUniverse(object):
    """docstring for MMTKUniverseFactory"""

    EDGE_LENGTH = 10.
    FF_PARAMS_BASENAME = 'mmtk_output.log'

    def __init__(self, mmtk_universe, universe_details, occupancy_dict=None):
        self.mmtk_universe = mmtk_universe
        self.universe_details = universe_details
        self.occupancy_dict = occupancy_dict

    def get_force_fields(self):
        return self.force_field_dict.keys()

    def get_pdb_filename(self):
        return self.universe_details.get('pdb')

    def get_force_field(self):
        return self.universe_details.get('ff')

    def get_ff_mod_filename(self):
        return self.universe_details.get('ff_mod')

    def get_box_length(self):
        return self.universe_details.get('box_length')

    def get_occupancy_dict(self):
        return self.occupancy_dict

    def save(self, file_prefix, pdbname):
        self._export_ff_params(file_prefix)
        self.universe_details.save_to_dir(file_prefix, pdbname)

    def _export_ff_params(self, file_prefix):
        '''
        Adapted from a script provide by K. Hinsen at:
        http://dirac.cnrs-orleans.fr/MMTK/using-mmtk/mmtk-example-scripts
        '''
        filename = file_prefix + '_' + self.FF_PARAMS_BASENAME
        with open(filename, 'w') as f:
            parameters = self.mmtk_universe.energyEvaluatorParameters()
            for p_type in parameters:
                p = parameters[p_type]
                f.write("%s\n" % p_type)
                f.write("%s\n" % (len(p_type)*'-'))
                f.write("%s\n" % FF_PARAM_EXPORT_NOTES[p_type])
                if isinstance(p, list):
                    f.write("[\n")
                    for data in p:
                        f.write(" %s,\n" % str(data))
                    f.write(" ]\n")
                elif isinstance(p, dict):
                    f.write("{\n")
                    for key, value in p.items():
                        s = repr(value)
                        if len(key) + len(s) < 75:
                            f.write(" %s: %s,\n" % (key, value))
                        else:
                            f.write(" %s:   %s,\n" % (key, value))
                    f.write("}\n")
                else:
                    f.write("%s\n" % p)
                f.write("\n")

    def write_pdb(self, configuration, filename='test_output.pdb'):
        pdb_output = PDBOutputFile(filename)
        pdb_output.write(self.mmtk_universe, configuration)
        pdb_output.close()

    def objectList(self):
        return self.mmtk_universe.objectList()

    def atomList(self):
        return self.mmtk_universe.atomList()
