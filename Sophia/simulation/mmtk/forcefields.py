#
# Adapted from AmberForceField.py by Konrad Hinsen
#


from MMTK.ForceFields import MMForceField
from MMTK.ForceFields.Amber import AmberData
from MMTK import Database
import os.path
from MMTK.ForceFields.NonBondedInteractions import LJForceField


GAFF = None
this_directory = os.path.split(__file__)[0]

def fullModFilePath(modfile):
    if not isinstance(modfile, basestring):
        return modfile
    if os.path.exists(modfile):
        return modfile
    return os.path.join(this_directory, os.path.basename(modfile))

def readGAFF(main_file = None, mod_files = None):
    global GAFF
    if main_file is None and mod_files is None:
        if GAFF is None:
            paramfile = os.path.join(Database.path[0], "gaff.dat")
            GAFF = AmberData.AmberParameters(paramfile)
            GAFF.lennard_jones_1_4 = 0.5
            GAFF.electrostatic_1_4 = 1./1.2
            GAFF.default_ljpar_set = GAFF.ljpar_sets['MOD4']
            GAFF.atom_type_property = 'amber_atom_type'
            GAFF.charge_property = 'amber_charge'
        return GAFF
    else:
        if main_file is None:
            main_file = os.path.join(Database.path[0], "gaff.dat")
        mod_files = map(lambda mf: (fullModFilePath(mf), 'MOD4'), mod_files)
        params = AmberData.AmberParameters(main_file, mod_files)
        params.lennard_jones_1_4 = 0.5
        params.electrostatic_1_4 = 1./1.2
        params.default_ljpar_set = params.ljpar_sets['MOD4']
        params.atom_type_property = 'amber_atom_type'
        params.charge_property = 'amber_charge'
        return params


class GAFFForceField(MMForceField.MMForceField):
    """
    General Amber Force Field

    General comments about parameters for Lennard-Jones and electrostatic
    interactions:

    Pair interactions in periodic systems are calculated using the
    minimum-image convention; the cutoff should therefore never be
    larger than half the smallest edge length of the elementary
    cell.

    For Lennard-Jones interactions, all terms for pairs whose distance
    exceeds the cutoff are set to zero, without any form of correction.
    For electrostatic interactions, a charge-neutralizing surface charge
    density is added around the cutoff sphere in order to reduce
    cutoff effects (see Wolf et al., J. Chem. Phys. 17, 8254 (1999)).

    For Ewald summation, there are some additional parameters that can
    be specified by dictionary entries:

    * "beta" specifies the Ewald screening parameter
    * "real_cutoff" specifies the cutoff for the real-space sum.
       It should be significantly larger than 1/beta to ensure that
       the neglected terms are small.
    * "reciprocal_cutoff" specifies the cutoff for the reciprocal-space sum.
       Note that, like the real-space cutoff, this is a distance; it describes
       the smallest wavelength of plane waves to take into account.
       Consequently, a smaller value means a more precise (and more expensive)
       calculation.

    MMTK provides default values for these parameter which are calculated
    as a function of the system size. However, these parameters are
    exaggerated in most cases of practical interest and can lead to excessive
    calculation times for large systems. It is preferable to determine
    suitable values empirically for the specific system to be simulated.

    The method "screened" uses the real-space part of the Ewald sum
    with a charge-neutralizing surface charge density around the
    cutoff sphere, and no reciprocal sum (see article cited above).
    It requires the specification of the dictionary entries "cutoff"
    and "beta".
    """

    def __init__(self, lj_options = None, es_options = None,
                 bonded_scale_factor = 1., **kwargs):
        """
        :param lj_options: parameters for Lennard-Jones
                           interactions; one of:

                           * a number, specifying the cutoff
                           * None, meaning the default method
                             (no cutoff; inclusion of all
                             pairs, using the minimum-image
                             conventions for periodic universes)
                           * a dictionary with an entry "method"
                             which specifies the calculation
                             method as either "direct" (all pair
                             terms) or "cutoff", with the cutoff
                             specified by the dictionary
                             entry "cutoff".

        :param es_options: parameters for electrostatic
                           interactions; one of:

                           * a number, specifying the cutoff
                           * None, meaning the default method
                             (all pairs without cutoff for
                             non-periodic system, Ewald summation
                             for periodic systems)
                           * a dictionary with an entry "method"
                             which specifies the calculation
                             method as either "direct" (all pair
                             terms), "cutoff" (with the cutoff
                             specified by the dictionary
                             entry "cutoff"), "ewald" (Ewald
                             summation, only for periodic
                             universes), or "screened".

        :keyword mod_files: a list of parameter modification files. The file
                            format is the one defined by AMBER. Each item
                            in the list can be either a file object
                            or a filename, filenames are looked up
                            first relative to the current directory and then
                            relative to the directory containing MMTK's
                            AMBER parameter files.

        """
        main_file = kwargs.get('parameter_file', None)
        mod_files = kwargs.get('mod_files', None)
        parameters = readGAFF(main_file, mod_files)
        MMForceField.MMForceField.__init__(self, 'GAFF', parameters,
                                           lj_options, es_options,
                                           bonded_scale_factor)
        self.arguments = (lj_options, es_options, bonded_scale_factor)


class LennardJonesForceField(LJForceField):

    """
    Lennard-Jones force field

    The Lennard-Jones parameters are taken from the atom attributes
    LJ_radius and LJ_energy. The pair interaction energy has the form
    U(r)=4*LJ_energy*((LJ_radius/r)**12-(LJ_radius/r)**6).
    """

    def __init__(self, cutoff = None):
        """
        :param cutoff: a cutoff value or None, meaning no cutoff.
                       Pair interactions in periodic systems are calculated
                       using the minimum-image convention; the cutoff should
                       therefore never be larger than half the smallest edge
                       length of the elementary cell.
        :type cutoff: float
        """
        self.arguments = (cutoff, )
        LJForceField.__init__(self, 'LJ', cutoff)
        self.lj_14_factor = 1.

    def ready(self, global_data):
        return True

    def collectParameters(self, universe, global_data):
        if not hasattr(global_data, 'lj_parameters'):
            parameters = {}
            for o in universe:
                for a in o.atomList():
                    parameters[a.symbol] = (a.LJ_energy, a.LJ_radius, 0)
            global_data.lj_parameters = parameters

    def _atomType(self, object, atom, global_data):
        return atom.symbol

    def _ljParameters(self, type, global_data):
        return global_data.lj_parameters[type]

    def evaluatorParameters(self, universe, subset1, subset2, global_data):
        self.collectParameters(universe, global_data)
        return LJForceField.evaluatorParameters(self, universe, subset1,
                                                subset2, global_data)
