from os.path import join, dirname, abspath
from MMTK import Database
from .trajectory import MMTKTrajectoryFactory
from .universe import MMTKUniverseFactory
from .geometer import MMTKGeometer


def init_mmtk_db():
    parent_dir = dirname(abspath(__file__))
    DATABASE_PATH = join(parent_dir, 'Database')
    Database.path = [DATABASE_PATH]
init_mmtk_db()
