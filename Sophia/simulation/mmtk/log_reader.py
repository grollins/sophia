from re import compile, MULTILINE, DOTALL
from numpy import array, zeros_like


class MMTKLogReader(object):
    """docstring for MMTKLogReader"""
    def __init__(self):
        md_pattern = r'Step (\d+)\nTime: ([0-9]*\.[0-9]+|[0-9]+)\n' \
                      'Potential energy: ([-+]?[0-9]*\.[0-9]+|[0-9]+), ' \
                      'Kinetic energy: ([-+]?[0-9]*\.[0-9]+|[0-9]+)\n' \
                      'Temperature: ([0-9]*\.[0-9]+|[0-9]+)'
        pressure_pattern = r'Pressure: ([0-9]*\.[0-9]+|[0-9]+)'
        em_pattern = r'Step (\d+)\n' \
                      'Potential energy: ([-+]?[0-9]*\.[0-9]+|[0-9]+), ' \
                      'Gradient norm: ([-+]?[0-9]*\.[0-9]+|[0-9]+)'
        self.md_regex = compile(md_pattern, flags=MULTILINE|DOTALL)
        self.pressure_regex = compile(pressure_pattern)
        self.em_regex = compile(em_pattern, flags=MULTILINE|DOTALL)

    def read(self, log_filename):
        match_list = []

        with open(log_filename, 'r') as f:
            text = f.read()

        # EM logs
        for match in self.em_regex.finditer(text):
            results = match.groups()
            match_list.append((int(results[0]), float(results[1]),
                              float(results[2])))
        if len(match_list) > 0:
            data_array = array(match_list)
            return {'potential': data_array[:,1],
                    'temperature': zeros_like(data_array[:,1]),
                    'pressure': zeros_like(data_array[:,1])}

        # MD logs
        for match in self.md_regex.finditer(text):
            results = match.groups()
            row_data = (int(results[0]), float(results[1]),
                        float(results[2]), float(results[3]),
                        float(results[4]))
            match_list.append(row_data)
        data_array = array(match_list)
        
        P_list = []
        for match in self.pressure_regex.finditer(text):
            results = match.groups()
            P = float(results)
            P_list.append(P)
        if len(P_list) == 0:
            pressure_array = zeros_like(data_array[:,1])
        else:
            pressure_array = array(P_list)

        return {'potential': data_array[:,2],
                'kinetic': data_array[:,3],
                'temperature': data_array[:,4],
                'pressure': pressure_array}
