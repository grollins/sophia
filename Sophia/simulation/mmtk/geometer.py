from MMTK import Units


class MMTKGeometer(object):
    """docstring for MMTKGeometer"""
    def __init__(self):
        pass

    def add_bond_distances_to_trajectory(self, bonds, trajectory):
        trajectory.compute_and_save_bond_distances(bonds,
            distance_fcn=self._bond_distance_fcn)

    def _bond_distance_fcn(self, mmtk_trajectory, mmtk_universe, atom_list):
        distance_vs_frame = []
        for frame in mmtk_trajectory:
            this_configuration = frame['configuration']
            distance = \
                mmtk_universe.distance(*atom_list, conf=this_configuration)
            distance_vs_frame.append(distance)
        return distance_vs_frame

    def add_angle_measures_to_trajectory(self, angles, trajectory):
        trajectory.compute_and_save_angle_measures(angles,
            measure_fcn=self._angle_measure_fcn)

    def _angle_measure_fcn(self, mmtk_trajectory, mmtk_universe, atom_list):
        measure_vs_frame = []
        for frame in mmtk_trajectory:
            this_configuration = frame['configuration']
            if len(atom_list) == 3:
                measure = \
                    mmtk_universe.angle(*atom_list, conf=this_configuration)
            elif len(atom_list) == 4:
                measure = \
                    mmtk_universe.dihedral(*atom_list, conf=this_configuration)
            else:
                measure = 0.
            measure /= Units.deg # convert radians to degrees
            measure_vs_frame.append(measure)
        return measure_vs_frame
