from chimera import openModels

from MMTK import CubicPeriodicUniverse, Atom, Units
from MMTK.Geometry import SCLattice
from MMTK.ForceFields import LennardJonesForceField
from MMTK.Environment import NoseThermostat, AndersenBarostat
from MMTK.Trajectory import Trajectory, TrajectoryOutput, StandardLogOutput
from MMTK.Dynamics import VelocityVerletIntegrator, VelocityScaler, \
                          TranslationRemover, BarostatReset

from .MMTK2Molecule import convert


# =============
# = Constants =
# =============
EDGE_LENGTH = 1.2864
N = 3
T = 94.4 * Units.K
P = 1. * Units.atm


# ===========
# = Classes =
# ===========
class SimulationActions(list):
    """docstring for SimulationActions"""
    def __init__(self):
        super(SimulationActions, self).__init__()

    def add_action(self, action):
        self.append(action)


# =============
# = Functions =
# =============
def create_atoms():
    ff = LennardJonesForceField(cutoff=1.5)
    universe = CubicPeriodicUniverse(EDGE_LENGTH, ff)
    for point in SCLattice(cellsize=EDGE_LENGTH/N, cells=(N, N, N)):
        universe.addObject(Atom('Ar', position=point))
    return universe

def create_chimera_molecule(universe):
    # Create corresponding Chimera molecule
    molecule, atom_map = convert(universe)
    openModels.add([molecule])
    return molecule, atom_map

def initialize_temperature_and_pressure(universe):
    universe.initializeVelocitiesToTemperature(T)
    universe.thermostat = NoseThermostat(T)
    universe.barostat = AndersenBarostat(P)
    return universe

def create_integrator(universe):
    integrator = VelocityVerletIntegrator(universe, delta_t=10*Units.fs,
                                          background=False)
    return integrator

def create_trajectory(universe):
    trajectory = Trajectory(universe, "temp.nc", "w", "A simple test case")
    return trajectory

def create_sim_actions(trajectory):
    sa = SimulationActions()
    sa.add_action( TranslationRemover(0, None, 100) )
    sa.add_action( VelocityScaler(T, 0.1*T, 0, None, 100) )
    sa.add_action( BarostatReset(100) )
    sa.add_action( TrajectoryOutput(trajectory, data=("time", "configuration"),
                                      first=0, last=None, skip=10) )
    return sa

def start_md_thread(universe, atom_map, integrator, actions, num_steps=10000):
    thread = integrator(steps=num_steps, actions=actions)

def run_md(universe, atom_map):
    universe = initialize_temperature_and_pressure(universe)
    integrator = create_integrator(universe)
    trajectory = create_trajectory(universe)
    actions = create_sim_actions(trajectory)
    start_md_thread(universe, atom_map, integrator, actions, num_steps=1000)
    return trajectory
