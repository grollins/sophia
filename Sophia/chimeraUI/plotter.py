from collections import defaultdict
from chimera.mplDialog import MPLDialog

from .const import PLOTS, ALMOST_ZERO

PADDING_FACTOR = 0.05
COLORS = ["#B340B3", "#FFA25B", "#4F6BB8", "#CC5F5A", "#63FF7F"]

class PlotterFactory(object):
    """docstring for PlotterFactory"""
    def __init__(self):
        pass

    def build(self, trajectory):
        return Plotter(trajectory)


class Plotter(object):
    """docstring for Plotter"""
    def __init__(self, trajectory):
        self.trajectory = trajectory
        self.plots_dict = {}

        self.energy_labels = ['potential', 'kinetic', 'total']
        energy_per_epoch = self.trajectory.energy
        self.combined_energies = self._combine_energies(energy_per_epoch)
        self.epoch_boundaries = self.trajectory.epoch_boundaries
        ep = EnergyPlot(self.combined_energies, self.epoch_boundaries,
                        self.energy_labels)
        self.plots_dict[PLOTS.energies] = ep

        T_per_epoch = self.trajectory.temperature
        self.combined_T = self._combine_temperatures(T_per_epoch)
        # tp = TemperaturePlot(self.combined_T, self.epoch_boundaries)
        # self.plots_dict[PLOTS.temperature] = tp

        P_per_epoch = self.trajectory.pressure
        self.combined_P = self._combine_pressures(P_per_epoch)
        # pp = PressurePlot(self.combined_P, self.epoch_boundaries)
        # self.plots_dict[PLOTS.pressure] = pp

    def _combine_energies(self, energy_per_epoch):
        combined_energies = defaultdict(list)
        for E in energy_per_epoch:
            for L in self.energy_labels:
                combined_energies[L] += list(E[L])
        return combined_energies

    def _combine_temperatures(self, T_per_epoch):
        combined_T = []
        for T in T_per_epoch:
            combined_T += list(T)
        return combined_T

    def _combine_pressures(self, P_per_epoch):
        combined_P = []
        for P in P_per_epoch:
            combined_P += list(P)
        return combined_P

    def _combine_bonds(self, bonds_per_epoch):
        combined_bonds = defaultdict(list)
        for this_epoch in bonds_per_epoch:
            for bond_label in this_epoch.keys():
                combined_bonds[bond_label] += list(this_epoch[bond_label])
        return combined_bonds

    def _combine_angles(self, angles_per_epoch):
        combined_angles = defaultdict(list)
        for this_epoch in angles_per_epoch:
            for angle_label in this_epoch.keys():
                combined_angles[angle_label] += list(this_epoch[angle_label])
        return combined_angles

    def close_plots(self):
        plots_closed = []
        for plot_label, plot_obj in self.plots_dict.items():
            plots_closed.append(plot_label)
            plot_obj.Close()
        return plots_closed

    def restore_closed_plots(self, plots_closed):
        for plot_label in plots_closed:
            if plot_label == PLOTS.energies:
                # the energy plot is shown by default when Plotter gets
                # created, so restoring it here might duplicate the plot
                continue
            else:
                self.show_plot(plot_label)

    # ====================================
    # = Trajectory Viewer Event Listener =
    # ====================================
    def update_plots(self, frame_number):
        for p in self.plots_dict.values():
            p.highlight_data_point(frame_number)

    def show_plot(self, label):
        if label == PLOTS.energies:
            ep = EnergyPlot(self.combined_energies, self.epoch_boundaries,
                            self.energy_labels)
            self.plots_dict[label] = ep
        elif label == PLOTS.temperature:
            tp = TemperaturePlot(self.combined_T, self.epoch_boundaries)
            self.plots_dict[label] = tp
        elif label == PLOTS.pressure:
            pp = PressurePlot(self.combined_P, self.epoch_boundaries)
            self.plots_dict[label] = pp
        elif label == PLOTS.bonds:
            bonds_per_epoch = self.trajectory.bonds
            if len(bonds_per_epoch) == 0 or len(bonds_per_epoch[0]) == 0:
                pass
            else:
                self.combined_bonds = self._combine_bonds(bonds_per_epoch)
                bp = BondsPlot(self.combined_bonds, self.epoch_boundaries)
                self.plots_dict[label] = bp
        elif label == PLOTS.angles:
            angles_per_epoch = self.trajectory.angles
            if len(angles_per_epoch) == 0 or len(angles_per_epoch[0]) == 0:
                pass
            else:
                self.combined_angles = self._combine_angles(angles_per_epoch)
                ap = AnglesPlot(self.combined_angles, self.epoch_boundaries)
                self.plots_dict[label] = ap
        else:
            pass

    def hide_plot(self, label):
        try:
            self.plots_dict[label].Close()
            self.plots_dict.pop(label)
        except KeyError:
            # can only hide plots that exist
            pass


class BondsPlot(MPLDialog):
    """docstring for BondsPlot"""

    title = "Bonds Plot"

    class SubPlot(object):
        def __init__(self, bond_distance, ax, color, label):
            self.ax = ax
            self._bond_distance = bond_distance
            self.line_x = range(len(self._bond_distance))
            self.line_y = self._bond_distance
            self.line, = self.ax.plot(self.line_x, self.line_y, '-',
                                      color=color, lw=4, label=label)

        def bond_distance(self, frame_number):
            if frame_number >= len(self._bond_distance):
                return None
            else:
                return self._bond_distance[frame_number]

        def get_limits(self):
            xmin = min(self.line_x)
            ymin = min(self.line_y)
            xmax = max(self.line_x)
            ymax = max(self.line_y)
            return (xmin, xmax), (ymin, ymax)


    def __init__(self, bonds, epoch_boundaries):
        MPLDialog.__init__(self, showToolbar=True)
        self.bonds = bonds
        self.epoch_boundaries = epoch_boundaries
        self.subplots = []
        ax = self.add_subplot(1, 1, 1)
        ax.set_xlabel("Frame Number")
        ax.set_ylabel("Bond Distance (nm)")
        ax.grid(True)
        for label, this_bond in self.bonds.iteritems():
            subplot = BondsPlot.SubPlot(this_bond, ax, COLORS[3], label)
            self.subplots.append(subplot)
        self._set_limits(ax)
        self._show_legend(ax)
        self._draw_epoch_boundaries(ax)
        self.highlighted_points, = ax.plot([], [], 'o', color=COLORS[3], ms=8)
        self.highlight_line, = ax.plot([], [], '-', color='0.2', lw=2)
        self.ani = FuncAnimation(self.figureCanvas, self._redraw,
                                 init_func=self._clear, blit=True)
        self.ani._start()

        def on_xlims_change(axes):
            self.ani._handle_resize()
            self.ani._draw_next_frame(framedata=None, blit=False)
        ax.callbacks.connect('xlim_changed', on_xlims_change)

    def _set_limits(self, ax):
        for i, s in enumerate(self.subplots):
            x_limits, y_limits = s.get_limits()
            if i == 0:
                xmin, xmax = x_limits
                ymin, ymax = y_limits
            else:
                if x_limits[0] < xmin:
                    xmin = x_limits[0]
                if x_limits[1] > xmax:
                    xmax = x_limits[1]
                if y_limits[0] < ymin:
                    ymin = y_limits[0]
                if y_limits[1] > ymax:
                    ymax = y_limits[1]
        xscale_factor_for_padding = (xmax - xmin) * PADDING_FACTOR
        yscale_factor_for_padding = (ymax - ymin) * PADDING_FACTOR
        xmin -= xscale_factor_for_padding
        ymin -= yscale_factor_for_padding
        xmax += xscale_factor_for_padding
        ymax += yscale_factor_for_padding
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
        self.ymin = ymin
        self.ymax = ymax

    def _show_legend(self, ax):
        # Shink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5),
                  fancybox=True, shadow=True)

    def _draw_epoch_boundaries(self, ax):
        for eb in self.epoch_boundaries:
            x = [eb] * 2
            y = [self.ymin, self.ymax]
            ax.plot(x, y, '-', color='0.5', lw=1)

    def _clear(self):
        self.highlighted_points.set_data([], [])
        self.highlight_line.set_data([], [])
        items_to_clear = [s.line for s in self.subplots]
        items_to_clear += [self.highlighted_points, self.highlight_line]
        return items_to_clear

    def _redraw(self, framedata):
        y = [s.bond_distance(self.frame_number) for s in self.subplots]
        x = [self.frame_number] * len(y)
        self.highlighted_points.set_data(x, y)
        x = [self.frame_number] * 2
        y = [self.ymin, self.ymax]
        self.highlight_line.set_data(x, y)
        items_to_redraw = [s.line for s in self.subplots]
        items_to_redraw += [self.highlight_line, self.highlighted_points]
        return items_to_redraw

    def highlight_data_point(self, frame_number):
        self.frame_number = frame_number
        self.ani._step()


class AnglesPlot(MPLDialog):
    """docstring for AnglesPlot"""

    title = "Angles Plot"

    class SubPlot(object):
        def __init__(self, angle_measure, ax, color, label):
            self.ax = ax
            self._angle_measure = angle_measure
            self.line_x = range(len(self._angle_measure))
            self.line_y = self._angle_measure
            self.line, = self.ax.plot(self.line_x, self.line_y, '-',
                                      color=color, lw=4, label=label)

        def angle_measure(self, frame_number):
            if frame_number >= len(self._angle_measure):
                return None
            else:
                return self._angle_measure[frame_number]

        def get_limits(self):
            xmin = min(self.line_x)
            ymin = min(self.line_y)
            xmax = max(self.line_x)
            ymax = max(self.line_y)
            return (xmin, xmax), (ymin, ymax)


    def __init__(self, angles, epoch_boundaries):
        MPLDialog.__init__(self, showToolbar=True)
        self.angles = angles
        self.epoch_boundaries = epoch_boundaries
        self.subplots = []
        ax = self.add_subplot(1, 1, 1)
        ax.set_xlabel("Frame Number")
        ax.set_ylabel("Angle (degrees)")
        ax.grid(True)
        for label, this_angle in self.angles.iteritems():
            subplot = AnglesPlot.SubPlot(this_angle, ax, COLORS[1], label)
            self.subplots.append(subplot)
        self._set_limits(ax)
        self._show_legend(ax)
        self._draw_epoch_boundaries(ax)
        self.highlighted_points, = ax.plot([], [], 'o', color=COLORS[1], ms=8)
        self.highlight_line, = ax.plot([], [], '-', color='0.2', lw=2)
        self.ani = FuncAnimation(self.figureCanvas, self._redraw,
                                 init_func=self._clear, blit=True)
        self.ani._start()

        def on_xlims_change(axes):
            self.ani._handle_resize()
            self.ani._draw_next_frame(framedata=None, blit=False)
        ax.callbacks.connect('xlim_changed', on_xlims_change)

    def _set_limits(self, ax):
        for i, s in enumerate(self.subplots):
            x_limits, y_limits = s.get_limits()
            if i == 0:
                xmin, xmax = x_limits
                ymin, ymax = y_limits
            else:
                if x_limits[0] < xmin:
                    xmin = x_limits[0]
                if x_limits[1] > xmax:
                    xmax = x_limits[1]
                if y_limits[0] < ymin:
                    ymin = y_limits[0]
                if y_limits[1] > ymax:
                    ymax = y_limits[1]
        xscale_factor_for_padding = (xmax - xmin) * PADDING_FACTOR
        yscale_factor_for_padding = (ymax - ymin) * PADDING_FACTOR
        xmin -= xscale_factor_for_padding
        ymin -= yscale_factor_for_padding
        xmax += xscale_factor_for_padding
        ymax += yscale_factor_for_padding
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
        self.ymin = ymin
        self.ymax = ymax

    def _show_legend(self, ax):
        # Shink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5),
                  fancybox=True, shadow=True)

    def _draw_epoch_boundaries(self, ax):
        for eb in self.epoch_boundaries:
            x = [eb] * 2
            y = [self.ymin, self.ymax]
            ax.plot(x, y, '-', color='0.5', lw=1)

    def _clear(self):
        self.highlighted_points.set_data([], [])
        self.highlight_line.set_data([], [])
        items_to_clear = [s.line for s in self.subplots]
        items_to_clear += [self.highlighted_points, self.highlight_line]
        return items_to_clear

    def _redraw(self, framedata):
        y = [s.angle_measure(self.frame_number) for s in self.subplots]
        x = [self.frame_number] * len(y)
        self.highlighted_points.set_data(x, y)
        x = [self.frame_number] * 2
        y = [self.ymin, self.ymax]
        self.highlight_line.set_data(x, y)
        items_to_redraw = [s.line for s in self.subplots]
        items_to_redraw += [self.highlight_line, self.highlighted_points]
        return items_to_redraw

    def highlight_data_point(self, frame_number):
        self.frame_number = frame_number
        self.ani._step()


class TemperaturePlot(MPLDialog):
    """docstring for TemperaturePlot"""

    title = "Temperature Plot"

    class SubPlot(object):
        def __init__(self, temperature, ax, color):
            self.ax = ax
            self._temperature = temperature
            self.line_x = range(len(self._temperature))
            self.line_y = self._temperature
            self.line, = self.ax.plot(self.line_x, self.line_y, '-',
                                      color=color, lw=4)

        def temperature(self, frame_number):
            if frame_number >= len(self._temperature):
                return None
            else:
                return self._temperature[frame_number]

        def get_limits(self):
            xmin = min(self.line_x)
            ymin = min(self.line_y)
            xmax = max(self.line_x)
            ymax = max(self.line_y)
            return (xmin, xmax), (ymin, ymax)


    def __init__(self, temperature, epoch_boundaries):
        MPLDialog.__init__(self, showToolbar=True)
        self.temperature = temperature
        self.epoch_boundaries = epoch_boundaries
        self.subplots = []
        ax = self.add_subplot(1, 1, 1)
        ax.set_xlabel("Frame Number")
        ax.set_ylabel("Temperature (K)")
        ax.grid(True)
        subplot = TemperaturePlot.SubPlot(
                    self.temperature, ax, COLORS[3])
        self.subplots.append(subplot)
        self._set_limits(ax)
        self._draw_epoch_boundaries(ax)
        self.highlighted_points, = ax.plot([], [], 'o', color=COLORS[3], ms=8)
        self.highlight_line, = ax.plot([], [], '-', color='0.2', lw=2)
        self.ani = FuncAnimation(self.figureCanvas, self._redraw,
                                 init_func=self._clear, blit=True)
        self.ani._start()

        def on_xlims_change(axes):
            self.ani._handle_resize()
            self.ani._draw_next_frame(framedata=None, blit=False)
        ax.callbacks.connect('xlim_changed', on_xlims_change)

    def _set_limits(self, ax):
        for i, s in enumerate(self.subplots):
            x_limits, y_limits = s.get_limits()
            if i == 0:
                xmin, xmax = x_limits
                ymin, ymax = y_limits
            else:
                if x_limits[0] < xmin:
                    xmin = x_limits[0]
                if x_limits[1] > xmax:
                    xmax = x_limits[1]
                if y_limits[0] < ymin:
                    ymin = y_limits[0]
                if y_limits[1] > ymax:
                    ymax = y_limits[1]
        if ymax < ALMOST_ZERO and ymin < ALMOST_ZERO:
            ymax = 10.0
            ymin = 0.0
        else:
            pass
        xscale_factor_for_padding = (xmax - xmin) * PADDING_FACTOR
        yscale_factor_for_padding = (ymax - ymin) * PADDING_FACTOR
        xmin -= xscale_factor_for_padding
        ymin -= yscale_factor_for_padding
        xmax += xscale_factor_for_padding
        ymax += yscale_factor_for_padding
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
        self.ymin = ymin
        self.ymax = ymax

    def _draw_epoch_boundaries(self, ax):
        for eb in self.epoch_boundaries:
            x = [eb] * 2
            y = [self.ymin, self.ymax]
            ax.plot(x, y, '-', color='0.5', lw=1)

    def _clear(self):
        self.highlighted_points.set_data([], [])
        self.highlight_line.set_data([], [])
        items_to_clear = [s.line for s in self.subplots]
        items_to_clear += [self.highlighted_points, self.highlight_line]
        return items_to_clear

    def _redraw(self, framedata):
        y = [s.temperature(self.frame_number) for s in self.subplots]
        x = [self.frame_number] * len(y)
        self.highlighted_points.set_data(x, y)
        x = [self.frame_number] * 2
        y = [self.ymin, self.ymax]
        self.highlight_line.set_data(x, y)
        items_to_redraw = [s.line for s in self.subplots]
        items_to_redraw += [self.highlight_line, self.highlighted_points]
        return items_to_redraw

    def highlight_data_point(self, frame_number):
        self.frame_number = frame_number
        self.ani._step()


class PressurePlot(MPLDialog):
    """docstring for PressurePlot"""

    title = "Pressure Plot"

    class SubPlot(object):
        def __init__(self, pressure, ax, color):
            self.ax = ax
            self._pressure = pressure
            self.line_x = range(len(self._pressure))
            self.line_y = self._pressure
            self.line, = self.ax.plot(self.line_x, self.line_y, '-',
                                      color=color, lw=4)

        def pressure(self, frame_number):
            if frame_number >= len(self._pressure):
                return None
            else:
                return self._pressure[frame_number]

        def get_limits(self):
            xmin = min(self.line_x)
            ymin = min(self.line_y)
            xmax = max(self.line_x)
            ymax = max(self.line_y)
            return (xmin, xmax), (ymin, ymax)

    def __init__(self, pressure, epoch_boundaries):
        MPLDialog.__init__(self, showToolbar=True)
        self.pressure = pressure
        self.epoch_boundaries = epoch_boundaries
        self.subplots = []
        ax = self.add_subplot(1, 1, 1)
        ax.set_xlabel("Frame Number")
        ax.set_ylabel("Pressure (atm)")
        ax.grid(True)
        subplot = PressurePlot.SubPlot(
                    self.pressure, ax, COLORS[4])
        self.subplots.append(subplot)
        self._set_limits(ax)
        self._draw_epoch_boundaries(ax)
        self.highlighted_points, = ax.plot([], [], 'o', color=COLORS[4], ms=8)
        self.highlight_line, = ax.plot([], [], '-', color='0.2', lw=2)
        self.ani = FuncAnimation(self.figureCanvas, self._redraw,
                                 init_func=self._clear, blit=True)
        self.ani._start()

        def on_xlims_change(axes):
            self.ani._handle_resize()
            self.ani._draw_next_frame(framedata=None, blit=False)
        ax.callbacks.connect('xlim_changed', on_xlims_change)

    def _set_limits(self, ax):
        for i, s in enumerate(self.subplots):
            x_limits, y_limits = s.get_limits()
            if i == 0:
                xmin, xmax = x_limits
                ymin, ymax = y_limits
            else:
                if x_limits[0] < xmin:
                    xmin = x_limits[0]
                if x_limits[1] > xmax:
                    xmax = x_limits[1]
                if y_limits[0] < ymin:
                    ymin = y_limits[0]
                if y_limits[1] > ymax:
                    ymax = y_limits[1]
        if ymax < ALMOST_ZERO and ymin < ALMOST_ZERO:
            ymax = 1.0
            ymin = 0.0
        else:
            pass
        xscale_factor_for_padding = (xmax - xmin) * PADDING_FACTOR
        yscale_factor_for_padding = (ymax - ymin) * PADDING_FACTOR
        xmin -= xscale_factor_for_padding
        ymin -= yscale_factor_for_padding
        xmax += xscale_factor_for_padding
        ymax += yscale_factor_for_padding
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
        self.ymin = ymin
        self.ymax = ymax

    def _draw_epoch_boundaries(self, ax):
        for eb in self.epoch_boundaries:
            x = [eb] * 2
            y = [self.ymin, self.ymax]
            ax.plot(x, y, '-', color='0.5', lw=1)

    def _clear(self):
        self.highlighted_points.set_data([], [])
        self.highlight_line.set_data([], [])
        items_to_clear = [s.line for s in self.subplots]
        items_to_clear += [self.highlighted_points, self.highlight_line]
        return items_to_clear

    def _redraw(self, framedata):
        y = [s.pressure(self.frame_number) for s in self.subplots]
        x = [self.frame_number] * len(y)
        self.highlighted_points.set_data(x, y)
        x = [self.frame_number] * 2
        y = [self.ymin, self.ymax]
        self.highlight_line.set_data(x, y)
        items_to_redraw = [s.line for s in self.subplots]
        items_to_redraw += [self.highlight_line, self.highlighted_points]
        return items_to_redraw

    def highlight_data_point(self, frame_number):
        self.frame_number = frame_number
        self.ani._step()


class EnergyPlot(MPLDialog):
    title = "Energy Plot"

    class SubPlot(object):
        def __init__(self, energy, ax, label, color):
            self.ax = ax
            self.label = label
            self._energy = energy
            self._is_active = True
            self.line_x = range(len(self._energy))
            self.line_y = self._energy
            self.line, = self.ax.plot(self.line_x, self.line_y, '-',
                                      color=color, lw=4, label=label)

        def energy(self, frame_number):
            if frame_number >= len(self._energy):
                return None
            else:
                return self._energy[frame_number]

        def get_limits(self):
            xmin = min(self.line_x)
            ymin = min(self.line_y)
            xmax = max(self.line_x)
            ymax = max(self.line_y)
            return (xmin, xmax), (ymin, ymax)

        def clear(self):
            self.line.set_data([], [])

        def show_plot(self):
            self.line.set_data(self.line_x, self.line_y)
            self.is_active = True

        def hide_plot(self):
            self.is_active = False


    def __init__(self, energies, epoch_boundaries, labels):
        MPLDialog.__init__(self, showToolbar=True)
        self.energies = energies
        self.epoch_boundaries = epoch_boundaries
        ax = self.add_subplot(1, 1, 1)
        ax.set_xlabel("Frame Number")
        ax.set_ylabel("Energy (kJ/mol)")
        ax.grid(True)
        self.subplots = []
        colors = [COLORS[0], COLORS[1], COLORS[2]]
        self.highlighted_points = []
        for i, label in enumerate(labels):
            subplot = EnergyPlot.SubPlot(self.energies[label], ax, label,
                                         colors[i])
            self.subplots.append(subplot)
            self.highlighted_points.append(ax.plot([], [], 'o', color=colors[i], ms=8)[0])
        self._set_limits(ax)
        self._show_legend(ax)
        self._draw_epoch_boundaries(ax)
        # self.highlighted_points, = ax.plot([], [], 'o', color=COLORS[2], ms=8)
        self.highlight_line, = ax.plot([], [], '-', color='0.2', lw=2)
        self.frame_number = 0
        self.ani = FuncAnimation(self.figureCanvas, self._redraw,
                                 init_func=self._clear, blit=True)
        self.ani._start()

        def on_xlims_change(axes):
            self.ani._handle_resize()
            self.ani._draw_next_frame(framedata=None, blit=False)
        ax.callbacks.connect('xlim_changed', on_xlims_change)

    def _set_limits(self, ax):
        for i, s in enumerate(self.subplots):
            x_limits, y_limits = s.get_limits()
            if i == 0:
                xmin, xmax = x_limits
                ymin, ymax = y_limits
            else:
                if x_limits[0] < xmin:
                    xmin = x_limits[0]
                if x_limits[1] > xmax:
                    xmax = x_limits[1]
                if y_limits[0] < ymin:
                    ymin = y_limits[0]
                if y_limits[1] > ymax:
                    ymax = y_limits[1]
        xscale_factor_for_padding = (xmax - xmin) * PADDING_FACTOR
        yscale_factor_for_padding = (ymax - ymin) * PADDING_FACTOR
        xmin -= xscale_factor_for_padding
        ymin -= yscale_factor_for_padding
        xmax += xscale_factor_for_padding
        ymax += yscale_factor_for_padding
        ax.set_xlim(xmin, xmax)
        ax.set_ylim(ymin, ymax)
        self.ymin = ymin
        self.ymax = ymax

    def _show_legend(self, ax):
        # Shink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5),
                  fancybox=True, shadow=True)

    def _draw_epoch_boundaries(self, ax):
        for eb in self.epoch_boundaries:
            x = [eb] * 2
            y = [self.ymin, self.ymax]
            ax.plot(x, y, '-', color='0.5', lw=1)

    def _clear(self):
        for hp in self.highlighted_points:
            hp.set_data([], [])
        self.highlight_line.set_data([], [])
        items_to_clear =  [s.line for s in self.subplots]
        items_to_clear += [self.highlight_line,] + self.highlighted_points
        return items_to_clear

    def _redraw(self, framedata):
        for i, hp in enumerate(self.highlighted_points):
            y = [self.subplots[i].energy(self.frame_number),]
            x = [self.frame_number] * len(y)
            hp.set_data(x, y)
        x = [self.frame_number] * 2
        y = [self.ymin, self.ymax]
        self.highlight_line.set_data(x, y)
        items_to_redraw = [s.line for s in self.subplots]
        items_to_redraw += [self.highlight_line,] + self.highlighted_points
        return items_to_redraw

    def highlight_data_point(self, frame_number):
        self.frame_number = frame_number
        self.ani._step()


class Animation(object):
    '''
    This class wraps the creation of an animation using matplotlib. It is
    only a base class which should be subclassed to provide needed behavior.

    *fig* is the figure object that is used to get draw, resize, and any
    other needed events.

    *event_source* is a class that can run a callback when desired events
    are generated, as well as be stopped and started. Examples include timers
    (see :class:`TimedAnimation`) and file system notifications.

    *blit* is a boolean that controls whether blitting is used to optimize
    drawing.
    '''
    def __init__(self, fig, event_source, blit):
        self._fig = fig
        # Disables blitting for backends that don't support it.  This
        # allows users to request it if available, but still have a
        # fallback that works if it is not.
        self._blit = blit

        # These are the basics of the animation.  The frame sequence represents
        # information for each frame of the animation and depends on how the
        # drawing is handled by the subclasses. The event source fires events
        # that cause the frame sequence to be iterated.
        self.frame_seq = self.new_frame_seq()
        self.event_source = event_source

        # Instead of starting the event source now, we connect to the figure's
        # draw_event, so that we only start once the figure has been drawn.
        self._first_draw_id = fig.mpl_connect('draw_event', self._start)

        # Connect to the figure's close_event so that we don't continue to
        # fire events and try to draw to a deleted figure.
        self._close_id = self._fig.mpl_connect('close_event', self._stop)

        if self._blit:
            self._setup_blit()

    def _start(self, *args):
        '''
        Starts interactive animation. Adds the draw frame command to the GUI
        handler, calls show to start the event loop.
        '''
        # First disconnect our draw event handler
        self._fig.mpl_disconnect(self._first_draw_id)
        self._first_draw_id = None  # So we can check on save

        # Now do any initial draw
        self._init_draw()

        # Add our callback for stepping the animation and
        # actually start the event_source.
        # self.event_source.add_callback(self._step)
        # self.event_source.start()

    def _stop(self, *args):
        # On stop we disconnect all of our events.
        if self._blit:
            self._fig.mpl_disconnect(self._resize_id)
        self._fig.mpl_disconnect(self._close_id)
        # self.event_source.remove_callback(self._step)
        # self.event_source = None

    def _step(self, *args):
        '''
        Handler for getting events. By default, gets the next frame in the
        sequence and hands the data off to be drawn.
        '''
        # Returns True to indicate that the event source should continue to
        # call _step, until the frame sequence reaches the end of iteration,
        # at which point False will be returned.
        try:
            framedata = next(self.frame_seq)
            self._draw_next_frame(framedata, self._blit)
            return True
        except StopIteration:
            return False

    def new_frame_seq(self):
        'Creates a new sequence of frame information.'
        # Default implementation is just an iterator over self._framedata
        # return iter(self._framedata)
        return None

    def new_saved_frame_seq(self):
        'Creates a new sequence of saved/cached frame information.'
        # Default is the same as the regular frame sequence
        return self.new_frame_seq()

    def _draw_next_frame(self, framedata, blit):
        # Breaks down the drawing of the next frame into steps of pre- and
        # post- draw, as well as the drawing of the frame itself.
        self._pre_draw(framedata, blit)
        self._draw_frame(framedata)
        self._post_draw(framedata, blit)

    def _init_draw(self):
        # Initial draw to clear the frame. Also used by the blitting code
        # when a clean base is required.
        pass

    def _pre_draw(self, framedata, blit):
        # Perform any cleaning or whatnot before the drawing of the frame.
        # This default implementation allows blit to clear the frame.
        if blit:
            self._blit_clear(self._drawn_artists, self._blit_cache)

    def _draw_frame(self, framedata):
        # Performs actual drawing of the frame.
        raise NotImplementedError('Needs to be implemented by subclasses to'
                                  ' actually make an animation.')

    def _post_draw(self, framedata, blit):
        # After the frame is rendered, this handles the actual flushing of
        # the draw, which can be a direct draw_idle() or make use of the
        # blitting.
        if blit and self._drawn_artists:
            self._blit_draw(self._drawn_artists, self._blit_cache)
        else:
            self._fig.draw_idle()

    # The rest of the code in this class is to facilitate easy blitting
    def _blit_draw(self, artists, bg_cache):
        # Handles blitted drawing, which renders only the artists given instead
        # of the entire figure.
        updated_ax = []
        for a in artists:
            # If we haven't cached the background for this axes object, do
            # so now. This might not always be reliable, but it's an attempt
            # to automate the process.
            if a.axes not in bg_cache:
                bg_cache[a.axes] = a.figure.canvas.copy_from_bbox(a.axes.bbox)
            a.axes.draw_artist(a)
            updated_ax.append(a.axes)

        # After rendering all the needed artists, blit each axes individually.
        for ax in set(updated_ax):
            ax.figure.canvas.blit(ax.bbox)

    def _blit_clear(self, artists, bg_cache):
        # Get a list of the axes that need clearing from the artists that
        # have been drawn. Grab the appropriate saved background from the
        # cache and restore.
        axes = set(a.axes for a in artists)
        for a in axes:
            if a in bg_cache:
                a.figure.canvas.restore_region(bg_cache[a])

    def _setup_blit(self):
        # Setting up the blit requires: a cache of the background for the
        # axes
        self._blit_cache = dict()
        self._drawn_artists = []
        self._resize_id = \
            self._fig.mpl_connect('resize_event', self._handle_resize)
        self._post_draw(None, self._blit)

    def _handle_resize(self, *args):
        # On resize, we need to disable the resize event handling so we don't
        # get too many events. Also stop the animation events, so that
        # we're paused. Reset the cache and re-init. Set up an event handler
        # to catch once the draw has actually taken place.
        self._fig.mpl_disconnect(self._resize_id)
        # self.event_source.stop()
        self._blit_cache.clear()
        self._init_draw()
        self._resize_id = \
            self._fig.mpl_connect('draw_event', self._end_redraw)

    def _end_redraw(self, evt):
        # Now that the redraw has happened, do the post draw flushing and
        # blit handling. Then re-enable all of the original events.
        self._post_draw(None, self._blit)
        # self.event_source.start()
        self._fig.mpl_disconnect(self._resize_id)
        self._resize_id = \
            self._fig.mpl_connect('resize_event', self._handle_resize)


class FuncAnimation(Animation):
    '''
    Makes an animation by repeatedly calling a function *func*, passing in
    (optional) arguments in *fargs*.

    *frames* can be a generator, an iterable, or a number of frames.

    *init_func* is a function used to draw a clear frame. If not given, the
    results of drawing from the first item in the frames sequence will be
    used. This function will be called once before the first frame.

    If blit=True, *func* and *init_func* should return an iterable of
    drawables to clear.
    '''
    def __init__(self, fig, func, init_func, blit):
        self._func = func
        self._init_func = init_func
        Animation.__init__(self, fig, event_source=None, blit=blit)

    def _step(self):
        '''
        Handler for getting events. By default, gets the next frame in the
        sequence and hands the data off to be drawn.
        '''
        framedata = None
        self._draw_next_frame(framedata, self._blit)

    def _init_draw(self):
        # Initialize the drawing using the given init_func.
        # For blitting, the init_func should return a sequence of modified
        # artists.
        self._drawn_artists = self._init_func()
        if self._blit:
            if self._drawn_artists is None:
                raise RuntimeError('The init_func must return a '
                                   'sequence of Artist objects.')
            for a in self._drawn_artists:
                a.set_animated(self._blit)

    def _draw_frame(self, framedata):
        # Call the func with framedata and args. If blitting is desired,
        # func needs to return a sequence of any artists that were modified.
        self._drawn_artists = self._func(framedata)
        if self._blit:
            if self._drawn_artists is None:
                raise RuntimeError('The animation function must return a '
                                   'sequence of Artist objects.')
            self._drawn_artists = sorted(self._drawn_artists,
                                         key=lambda x: x.get_zorder())

            for a in self._drawn_artists:
                a.set_animated(self._blit)
