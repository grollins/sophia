from chimera import openModels, Coord
from .MMTK2Molecule import convert

class UniverseTranslatorFactory(object):
    """docstring for UniverseTranslatorFactory"""
    def __init__(self):
        pass

    def build(self, universe):
        # Create corresponding Chimera molecule
        chimera_model, mmtk2chimera_map, chimera2mmtk_map = convert(universe)
        openModels.add([chimera_model])
        return UniverseTranslator(mmtk2chimera_map, chimera2mmtk_map,
                                  chimera_model)


class UniverseTranslator(object):
    """docstring for UniverseTranslator"""
    def __init__(self, mmtk2chimera_map, chimera2mmtk_map, chimera_model):
        super(UniverseTranslator, self).__init__()
        self.mmtk2chimera_map = mmtk2chimera_map
        self.chimera2mmtk_map = chimera2mmtk_map
        self.mmtk_idx2chimera_map = {}
        for mmtk_atom, chimera_atom in self.mmtk2chimera_map.iteritems():
            self.mmtk_idx2chimera_map[mmtk_atom.index] = chimera_atom
        self.chimera_model = chimera_model

    def iter_mmtk2chimera_atom_map(self, universe):
        for ma in universe.atomList():
            yield ma, self.mmtk2chimera_map[ma]

    def translate_coords(self, configuration, universe_atom):
        # mmtk uses nm, chimera uses angstroms
        x, y, z = configuration[universe_atom] * 10
        return Coord(x, y, z)

    def map_atoms_chimera2mmtk(self, atoms):
        return [self.chimera2mmtk_map[a] for a in atoms]

    def map_atom_indices_mmtk2chimera(self, atom_indices):
        return [self.mmtk_idx2chimera_map[idx] for idx in atom_indices]
