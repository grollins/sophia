from chimera import runCommand, openModels

class RendererFactory(object):
    """docstring for RendererFactory"""
    def __init__(self):
        pass

    def build(self, universe_translator_factory):
        return Renderer(universe_translator_factory)


class Renderer(object):
    """docstring for Renderer"""
    def __init__(self, universe_translator_factory):
        self.universe_translator = None
        self.universe_translator_factory = universe_translator_factory

    def render_new_universe(self, universe):
        # Currently, this creates and renders a new molecule in Chimera
        # as a side effect. Need to refactor to make it explicit.
        self.universe_translator = \
            self.universe_translator_factory.build(universe)
        self._prettify_view()
        self._update_radii(universe)

    def _prettify_view(self):
        runCommand("set bgColor #f0f0f0")
        runCommand("set projection orthographic")
        runCommand("unset depthCue")
        runCommand("set subdivision 3.0")
        runCommand("lighting mode two-point")
        runCommand("lighting reflectivity 0.3")
        runCommand("lighting sharpness 80.0")
        runCommand("center")

    def _update_radii(self, universe):
        iterator = \
                self.universe_translator.iter_mmtk2chimera_atom_map(universe)
        for universe_atom, chimera_atom in iterator:
            # set chimera atom radius to mmtk LJ value
            # need to convert nm (mmtk) to angstroms (chimera)
            # chimera_atom.radius = universe_atom.LJ_radius * 10
            # print universe_atom.LJ_radius * 10
            chimera_atom.radius *= 1.0

    def render_universe_update(self, universe, new_configuration):
        if self.universe_translator is None:
            pass
        else:
            iterator = \
                self.universe_translator.iter_mmtk2chimera_atom_map(universe)
            for universe_atom, chimera_atom in iterator:
                chimera_coords = \
                    self.universe_translator.translate_coords(
                        new_configuration, universe_atom)
                chimera_atom.setCoord(chimera_coords)

    def stop_rendering(self):
        if self.universe_translator is None:
            pass
        else:
            model_list = openModels.list()
            for m in model_list:
                openModels.remove(m)
            self.universe_translator = None

    def map_atoms_chimera2mmtk(self, atoms):
        return self.universe_translator.map_atoms_chimera2mmtk(atoms)

    def map_atom_indices_mmtk2chimera(self, atom_indices):
        return self.universe_translator.map_atom_indices_mmtk2chimera(atom_indices)
