# Plotting quantities
PLOTS = type('plots', (object,), {'energies': 'energies',
             'temperature': 'temperature', 'pressure': 'pressure',
             'bonds': 'bonds', 'angles': 'angles'})()

ALMOST_ZERO = 1e-6
SOPHIA_VERSION = '0.7.1'
BOX_NAME = 'sophia_box'
