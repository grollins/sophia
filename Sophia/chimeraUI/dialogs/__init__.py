from .main import build_main_dialog_class
from .sim import build_simulation_dialog_class, IngredientsWidgetFactory, \
                 RecipeWidgetFactory
from .traj import build_trajectory_viewer_dialog_class
from .universe_dialog import build_universe_dialog_class, UniverseWidgetFactory

PMW_FONT_SIZE_ADJUSTMENT = -4

def create_dialog_classes(mediator):
    uwf = UniverseWidgetFactory(padx=0, pady=7)
    iwf = IngredientsWidgetFactory()
    rwf = RecipeWidgetFactory()
    UniverseDialog = \
        build_universe_dialog_class(mediator, uwf, PMW_FONT_SIZE_ADJUSTMENT)
    SimulationDialog = \
        build_simulation_dialog_class(mediator, iwf, rwf,
                                      PMW_FONT_SIZE_ADJUSTMENT)

    MainDialog = build_main_dialog_class(mediator)
    TrajectoryViewerDialog = \
        build_trajectory_viewer_dialog_class(mediator, PMW_FONT_SIZE_ADJUSTMENT)
    return MainDialog, SimulationDialog, TrajectoryViewerDialog, UniverseDialog
