from Tkinter import IntVar, Label, Checkbutton, Frame, Button
from Pmw import ButtonBox, EntryField, Group, logicalfont
from chimera import triggers
from chimera.baseDialog import ModelessDialog

from ..const import PLOTS

#  =============
#  = Constants =
#  =============
PLAY_TRIGGER = 'new frame' # this value can't be changed, it's a specific
                           # Chimera trigger name
FRAME_DELAY = 20 # milliseconds


def build_trajectory_viewer_dialog_class(mediator, font_size=0):

    class SophiaTrajectoryViewer(ModelessDialog):
        name = "sophia_trajectory_viewer"
        buttons = ("Close")
        # help = ("SophiaUI.html", SophiaUI)
        title = "Sophia Trajectory Viewer"
        provideStatus = True

        def __init__(self):
            super(SophiaTrajectoryViewer, self).__init__()

        def fillInUI(self, parent):
            self.mediator = mediator
            self.mediator.connect_to_trajectory_viewer_gui(self)

            # initialize variables related to trajectory frame numbers
            self.current_epoch_num = IntVar(parent)
            self.last_epoch_num = IntVar(parent)
            self.current_frame_num = IntVar(parent)
            self.last_frame_num = IntVar(parent)

            # Initialize variables related to plotting options
            self.show_energies = IntVar(parent)
            self.show_temperature = IntVar(parent)
            self.show_pressure = IntVar(parent)
            self.show_bonds = IntVar(parent)
            self.show_angles = IntVar(parent)

            f = Frame(parent)
            f.grid(row=0, column=0, padx=15, pady=5, sticky='w')
            g = Group(f, tag_text='Viewer Controls',
                      tag_font=logicalfont('Helvetica', font_size,
                                           weight='bold'))
            g.pack()
            f2 = Frame(g.interior())
            f2.pack(padx=15, pady=15)
            # initialize labels to display the values of the trajectory variables
            Label(f2, text='Epoch').grid(row=0, column=0, columnspan=2)
            L = Label(f2, textvariable=self.current_epoch_num)
            L.grid(row=0, column=2)
            Label(f2, text='of').grid(row=0, column=3)
            L = Label(f2, textvariable=self.last_epoch_num)
            L.grid(row=0, column=4)
            Label(f2, text='Frame').grid(row=1, column=0, columnspan=2)
            L = Label(f2, textvariable=self.current_frame_num)
            L.grid(row=1, column=2)
            Label(f2, text='of').grid(row=1, column=3)
            L = Label(f2, textvariable=self.last_frame_num)
            L.grid(row=1, column=4)

            # Create a vertical button box
            bb = ButtonBox(f2, orient='horizontal', padx=3, pady=5)
            bb.grid(row=2, column=0, columnspan=5)

            # Add buttons to the ButtonBox.
            bb.add(u"\u21E4", command=self._show_prev_epoch)
            bb.add(u"\u2190", command=self._show_prev_frame)
            bb.add(u"\u25B6", command=self._play_movie)
            bb.add(u"\u2192", command=self._show_next_frame)
            bb.add(u"\u21E5", command=self._show_next_epoch)

            # Make all the buttons the same width.
            bb.alignbuttons()

            # Options widget
            f = Frame(parent)
            f.grid(row=1, column=0, padx=15, pady=5, sticky='w')
            g = Group(f, tag_text='Viewing Options',
                      tag_font=logicalfont('Helvetica', font_size,
                                           weight='bold'))
            g.pack()
            f2 = Frame(g.interior())
            f2.pack(padx=15, pady=2)

            self.stop_at_epoch_end = IntVar()
            cb = Checkbutton(f2, text="Stop playing at end of epoch",
                             variable=self.stop_at_epoch_end)
            cb.grid(row=0, column=0, padx=0, pady=3, sticky='w')

            ef = EntryField(f2, labelpos='e', labelmargin=10,
                            entry_width=5, value='1',
                            label_text = 'Step size (frames)',
                            validate = {'validator': 'numeric', 'min': 1,
                                        'minstrict': 1})
            ef.grid(row=1, column=0, padx=0, pady=3, sticky='w')
            self.step_size = ef

            # Plotting options widget
            f = Frame(parent)
            f.grid(row=2, column=0, padx=15, pady=5, sticky='w')
            g = Group(f, tag_text='Plotting Options',
                      tag_font=logicalfont('Helvetica', font_size,
                                           weight='bold'))
            g.pack()
            f2 = Frame(g.interior())
            f2.pack(padx=15, pady=2)

            cb = Checkbutton(f2, text="Energies",
                             variable=self.show_energies,
                             command=self._energies_cb)
            cb.grid(row=0, column=0, padx=0, pady=3, sticky='w')
            cb = Checkbutton(f2, text="Temperature",
                             variable=self.show_temperature,
                             command=self._temperature_cb)
            cb.grid(row=1, column=0, padx=0, pady=3, sticky='w')
            cb = Checkbutton(f2, text="Pressure",
                             variable=self.show_pressure,
                             command=self._pressure_cb)
            cb.grid(row=2, column=0, padx=0, pady=3, sticky='w')
            cb = Checkbutton(f2, text="Bonds",
                             variable=self.show_bonds,
                             command=self._bonds_cb)
            cb.grid(row=3, column=0, padx=0, pady=3, sticky='w')
            cb = Checkbutton(f2, text="Angles/Torsions",
                             variable=self.show_angles,
                             command=self._angles_cb)
            cb.grid(row=4, column=0, padx=0, pady=3, sticky='w')

            B = Button(f2, text="Structure Measurements...",
                       command=self._open_structure_measurements_dialog,
                       padx=15)
            B.grid(row=5, column=0, padx=0, pady=3, sticky='w')

            # initialize trajectory player
            self._init_trajectory_player()

        def reset(self):
            self.current_epoch_num.set(0)
            self.last_epoch_num.set(0)
            self.current_frame_num.set(0)
            self.last_frame_num.set(0)

            self.show_energies.set(1)
            self.show_temperature.set(0)
            self.show_pressure.set(0)
            self.show_bonds.set(0)
            self.show_angles.set(0)

            self._init_trajectory_player()

        def load_trajectory(self, num_epochs, first_epoch_length):
            self.current_epoch_num.set(0)
            self.last_epoch_num.set(num_epochs - 1)
            self.current_frame_num.set(0)
            self.last_frame_num.set(first_epoch_length - 1)

        def _energies_cb(self):
            plot_name = PLOTS.energies
            my_var = self.show_energies
            if my_var.get():
                self.mediator.show_plot(plot_name)
            else:
                self.mediator.hide_plot(plot_name)

        def _temperature_cb(self):
            plot_name = PLOTS.temperature
            my_var = self.show_temperature
            if my_var.get():
                self.mediator.show_plot(plot_name)
            else:
                self.mediator.hide_plot(plot_name)

        def _pressure_cb(self):
            plot_name = PLOTS.pressure
            my_var = self.show_pressure
            if my_var.get():
                self.mediator.show_plot(plot_name)
            else:
                self.mediator.hide_plot(plot_name)

        def _bonds_cb(self):
            plot_name = PLOTS.bonds
            my_var = self.show_bonds
            if my_var.get():
                self.mediator.show_plot(plot_name)
            else:
                self.mediator.hide_plot(plot_name)

        def _angles_cb(self):
            plot_name = PLOTS.angles
            my_var = self.show_angles
            if my_var.get():
                self.mediator.show_plot(plot_name)
            else:
                self.mediator.hide_plot(plot_name)

        def _open_structure_measurements_dialog(self):
            self.mediator.open_structure_measurements_dialog()

        def _init_trajectory_player(self):
            self.trigger = None
            self.movie_running = False
            self.in_delay = False
            self._in_trigger_CB = False

        def _load_frame(self, epoch_num, frame_num):
            self.current_epoch_num.set(epoch_num)
            this_epoch_length = self.mediator.get_epoch_length(epoch_num)
            last_frame_num = this_epoch_length - 1
            self.last_frame_num.set(last_frame_num)
            if frame_num == -1:
                self.current_frame_num.set(last_frame_num)
                self.mediator.load_frame(epoch_num, last_frame_num)
            else:
                self.current_frame_num.set(frame_num)
                self.mediator.load_frame(epoch_num, frame_num)

        def _show_prev_epoch(self):
            if not self._is_safe_to_update():
                self._stop_movie()
            epoch_num = self.current_epoch_num.get()
            frame_num = self.current_frame_num.get()
            if epoch_num == 0:
                pass
            elif epoch_num == self.last_epoch_num.get() and \
                 frame_num == self.last_frame_num.get():
                 pass
            else:
                epoch_num -= 1
            self._load_frame(epoch_num, 0)

        def _show_prev_frame(self):
            if not self._is_safe_to_update():
                self._stop_movie()
            epoch_num = self.current_epoch_num.get()
            frame_num = self.current_frame_num.get()
            new_frame_num = frame_num - int(self.step_size.getvalue())
            if new_frame_num < 0 and epoch_num == 0:
                pass
            elif new_frame_num < 0 and epoch_num > 0:
                self._load_frame(epoch_num - 1, -1)
            else:
                self._load_frame(epoch_num, new_frame_num)

        def _show_next_frame(self):
            if not self._is_safe_to_update():
                self._stop_movie()
            epoch_num = self.current_epoch_num.get()
            frame_num = self.current_frame_num.get()
            last_frame_num = self.last_frame_num.get()
            last_epoch_num = self.last_epoch_num.get()
            new_frame_num = frame_num + int(self.step_size.getvalue())
            if new_frame_num <= last_frame_num:
                self._load_frame(epoch_num, new_frame_num)
            elif new_frame_num > last_frame_num and \
                 epoch_num < last_epoch_num:
                self._load_frame(epoch_num + 1, 0)
            else:
                pass

        def _show_next_epoch(self):
            if not self._is_safe_to_update():
                self._stop_movie()
            epoch_num = self.current_epoch_num.get()
            if epoch_num < self.last_epoch_num.get():
                self._load_frame(epoch_num + 1, 0)
            elif epoch_num == self.last_epoch_num.get():
                last_frame_num = self.last_frame_num.get()
                self._load_frame(epoch_num, last_frame_num)

        def _play_movie(self):
            if self.movie_running:
                self._stop_movie()
            else:
                self.movie_running = True
                self.trigger = triggers.addHandler(PLAY_TRIGGER, self._update_movie_callback, None)

        def _update_movie_callback(self, triggerName, myData, triggerData):
            if self._is_safe_to_update():
                self._in_trigger_CB = True
                self.uiMaster().after(FRAME_DELAY, self._play_next_frame)
                self.in_delay = True
                self._in_trigger_CB = False
                return
            else:
                return

        def _play_next_frame(self):
            self.in_delay = False
            epoch_num = self.current_epoch_num.get()
            frame_num = self.current_frame_num.get()
            new_frame_num = frame_num + int(self.step_size.getvalue())
            if new_frame_num > self.last_frame_num.get():
                if self.stop_at_epoch_end.get():
                    self._stop_movie()
                else:
                    if epoch_num == self.last_epoch_num.get():
                        self._stop_movie()
                    else:
                        epoch_num += 1
                        self._load_frame(epoch_num, 0)
            else:
                self._load_frame(epoch_num, new_frame_num)

        def _is_safe_to_update(self):
            if not self.movie_running or self.in_delay or self._in_trigger_CB:
                return False
            else:
                return True

        def _stop_movie(self):
            if self.trigger is not None:
                triggers.deleteHandler(PLAY_TRIGGER, self.trigger)
            self._init_trajectory_player()
            return

    return SophiaTrajectoryViewer
