from os.path import basename
from Tkinter import StringVar, Frame, Button, Label
from Pmw import Group, OptionMenu, EntryField
from Pmw import logicalfont, ScrolledFrame, MessageBar
from tkFileDialog import askopenfilename
from chimera.baseDialog import ModelessDialog


UNIVERSE_STATUS = \
    type('universe_status', (object,), {'not_loaded': 'Not yet loaded',
         'loaded': 'Successfully loaded', 'error': 'Error while loading'})()


def build_universe_dialog_class(mediator, universe_widget_factory, font_size=0):
    class SophiaUniverse(ModelessDialog):
        """docstring for SophiaUniverse"""
        name = "sophia_universe"
        buttons = ("Close")
        title = "Sophia Universe"
        provideStatus = True

        def __init__(self):
            super(SophiaUniverse, self).__init__()

        def fillInUI(self, parent):
            self.parent = parent
            self.mediator = mediator
            self.mediator.connect_to_universe_gui(self)

            self.padx = 10
            self.pady = 10

            # a scrollable frame to hold all the widgets
            scrolled_frame = \
                ScrolledFrame(parent, usehullsize=True, borderframe=False,
                              hull_width=435, hull_height=378)
            scrolled_frame.grid(row=0, column=0, sticky='w',
                                padx=self.padx, pady=self.pady)
            sf = scrolled_frame.interior()

            #  =========================
            #  = Create Universe Group =
            #  =========================
            row_index = 0
            f = Frame(sf)
            f.grid(row=row_index, column=0, columnspan=2,
                   padx=self.padx, pady=self.pady)
            g = Group(f, tag_text='Universe', tagindent=0,
                      tag_font=logicalfont('Helvetica', font_size,
                                           weight='bold'))
            g.pack()
            self.universe_widget = \
                universe_widget_factory.build(g.interior(), self.mediator)
            self.universe_widget.pack(padx=15, pady=15)

        #  ============================
        #  = Parent Mediator Listener =
        #  ============================
        def reset(self):
            self.universe_widget.reset()

        def universe_loaded(self):
            self.universe_widget.universe_loaded()

        def universe_error(self):
            self.universe_widget.universe_error()

        #  =================================
        #  = SophiaMediator Event Listener =
        #  =================================
        def display_universe_details(self, universe):
            self.universe_widget.display_universe_details(universe)

        def lock_universe_panel(self):
            self.universe_widget.lock()

    return SophiaUniverse

class UniverseWidgetFactory(object):
    """docstring for UniverseWidgetFactory"""
    def __init__(self, padx, pady):
        self.padx = padx
        self.pady = pady
        self.base_bwidth = 28

    def build(self, parent_frame, mediator):
        uw = UniverseWidget(parent_frame)
        uw.connect_to_mediator(mediator)

        row_index = 0

        #  ========================================
        #  = Create label and button for pdb file =
        #  ========================================
        L = Label(uw.frame, text="PDB File")
        L.grid(row=0, column=0, padx=self.padx, pady=(0,self.pady), sticky='e')
        select_pdb_button = \
            Button(uw.frame, command=uw._select_pdb_file,
                   textvariable=uw.pdb_label, width=self.base_bwidth,
                   justify='left')
        select_pdb_button.grid(
            row=row_index, column=1, padx=self.padx, pady=(0,self.pady))
        row_index += 1

        #  ==============================================
        #  = Create a combobox for choosing force field =
        #  ==============================================
        uw.sim_params['force_field'] = StringVar()
        uw.sim_params['force_field'].set('')
        ff_options = uw.mediator.get_force_fields()
        L = Label(uw.frame, text="Force Field")
        L.grid(row=row_index, column=0, padx=self.padx, pady=self.pady,
               sticky='e')
        ff_menu = OptionMenu(uw.frame, items=ff_options,
                    menubutton_textvariable=uw.sim_params['force_field'],
                    menubutton_width=self.base_bwidth+2)
        ff_menu.grid(row=row_index, column=1, padx=self.padx, pady=self.pady)
        row_index += 1

        #  =============================================
        #  = Create a label and button for ff mod file =
        #  =============================================
        L = Label(uw.frame, text="FF Mod File\n(optional)")
        L.grid(row=row_index, column=0, padx=self.padx, pady=(0,self.pady),
               sticky='e')
        select_ff_mod_button = \
            Button(uw.frame, command=uw._select_ff_mod_file,
                   textvariable=uw.ff_mod_label, width=self.base_bwidth,
                   justify='left')
        select_ff_mod_button.grid(
            row=row_index, column=1, padx=self.padx, pady=(0,self.pady))
        row_index += 1

        #  ==========================
        #  = Length of Periodic Box =
        #  ==========================
        ef = EntryField(uw.frame, labelpos='w', value='10.0',
                        label_text='Box Length\n(nm)',
                        entry_width=29,
                        labelmargin=7,
                        validate = {'validator': 'real'})
        ef.grid(row=row_index, column=0, columnspan=2, padx=self.padx,
                pady=(0,self.pady), sticky='w')
        uw.box_length_entryfield = ef
        row_index += 1

        #  ====================
        #  = Length of Cutoff =
        #  ====================
        ef = EntryField(uw.frame, labelpos='w', value='1.0',
                        label_text='Non-bond\nCutoff (nm)',
                        entry_width=29,
                        labelmargin=7,
                        validate = {'validator': 'real'})
        ef.grid(row=row_index, column=0, columnspan=2, padx=self.padx,
                pady=(0,self.pady), sticky='w')
        uw.cutoff_entryfield = ef
        row_index += 1

        #  ============================================
        #  = Create a button for loading the universe =
        #  ============================================
        B = Button(uw.frame, text="Load Universe",
                   command=uw._load_universe_from_pdb,
                   width=self.base_bwidth+8)
        B.grid(row=row_index, column=0, columnspan=2, padx=self.padx,
               pady=(0,self.pady))
        row_index += 1

        #  ==========================================================
        #  = Create a button for reloading the most recent universe =
        #  ==========================================================
        B = Button(uw.frame, text="Reset Universe",
                   command=uw._reset_universe,
                   width=self.base_bwidth+8)
        B.grid(row=row_index, column=0, columnspan=2, padx=self.padx,
               pady=(0,self.pady))
        row_index += 1

        #  =======================================================
        #  = Label that tells user that universe has been loaded =
        #  =======================================================
        mb = MessageBar(uw.frame, entry_width=self.base_bwidth-2,
                        entry_relief='groove', labelpos='w',
                        label_text='Universe Status:', silent=True,
                        labelmargin=5)
        mb.grid(row=row_index, column=0, columnspan=2, padx=self.padx,
                pady=(self.pady,0), sticky='w')
        mb.message('state', UNIVERSE_STATUS.not_loaded)
        uw.message_bar = mb
        row_index += 1

        return uw


class UniverseWidget(object):
    """docstring for UniverseWidget"""
    def __init__(self, parent_frame):
        self.frame = Frame(parent_frame)
        self.mediator = None
        self.sim_params = {}
        self.sim_params['force_field'] = None
        self.pdb_file = StringVar()
        self.pdb_label = StringVar()
        self.pdb_file.set(None)
        self.pdb_label.set('Select PDB File...')
        self.ff_mod_file = StringVar()
        self.ff_mod_file.set(None)
        self.ff_mod_label = StringVar()
        self.ff_mod_label.set('Select File...')
        self.is_active = True
        self.message_bar = None
        self.box_length_entryfield = None
        self.cutoff_entryfield = None

    def _select_pdb_file(self):
        if self.is_active:
            filename = askopenfilename(multiple=False)
            if filename == '':
                pass
            else:
                self.pdb_file.set(filename)
                self.pdb_label.set(basename(filename))
        else:
            pass

    def _select_ff_mod_file(self):
        if self.is_active:
            filename = askopenfilename(multiple=False)
            if filename == '':
                pass
            else:
                self.ff_mod_file.set(filename)
                self.ff_mod_label.set(basename(filename))
        else:
            pass

    def _load_universe_from_pdb(self):
        if self.is_active:
            pdb_file = self.pdb_file.get()
            if pdb_file:
                ff = self.sim_params['force_field'].get()
                ff_mod_file = self.ff_mod_file.get()
                if ff_mod_file in ['', 'None']:
                    ff_mod_file = None
                else:
                    pass
                box_length = self.box_length_entryfield.getvalue()
                if box_length is '':
                    box_length = 0.0
                else:
                    box_length = float(box_length)
                cutoff = self.cutoff_entryfield.getvalue()
                if cutoff is '':
                    cutoff = 0.0
                else:
                    cutoff = float(cutoff)
                self.mediator.load_universe_from_pdb(pdb_file, ff, ff_mod_file,
                                                     box_length, cutoff)
            else:
                pass
        else:
            pass

    def _reset_universe(self):
        self.mediator.reset_universe()

    def pack(self, padx, pady):
        self.frame.pack(padx=padx, pady=pady)

    def connect_to_mediator(self, mediator):
        self.mediator = mediator

    def reset(self):
        self.is_active = True
        self.pdb_file.set(None)
        self.pdb_label.set('Select PDB File...')
        self.sim_params['force_field'].set('')
        self.ff_mod_file.set(None)
        self.ff_mod_label.set('Select File...')
        self.message_bar.message('state', UNIVERSE_STATUS.not_loaded)

    def display_universe_details(self, universe):
        self.pdb_label.set(basename(universe.get_pdb_filename()))
        self.sim_params['force_field'].set(universe.get_force_field())
        if universe.get_ff_mod_filename():
            self.ff_mod_label.set(basename(universe.get_ff_mod_filename()))

    def lock(self):
        self.is_active = False

    def universe_loaded(self):
        self.message_bar.message('state', UNIVERSE_STATUS.loaded)

    def universe_error(self):
        self.message_bar.message('state', UNIVERSE_STATUS.error)
