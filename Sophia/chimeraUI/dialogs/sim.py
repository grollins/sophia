from Tkinter import IntVar, StringVar, Frame, Button, Label, Checkbutton
from ttk import Treeview
from Pmw import Group, OptionMenu, NoteBook, EntryField, ButtonBox
from Pmw import alignlabels, logicalfont, ScrolledFrame
from tkFileDialog import askopenfilename, asksaveasfilename
from chimera.baseDialog import ModelessDialog


def build_simulation_dialog_class(mediator, ingredients_widget_factory,
                                  recipe_widget_factory, font_size=0):

    class SophiaSimulation(ModelessDialog):
        """docstring for SophiaSimulation"""
        name = "sophia_simulation"
        buttons = ("Close")
        # help = ("SophiaUI.html", SophiaUI)
        title = "Sophia Recipe"
        provideStatus = True

        def __init__(self):
            super(SophiaSimulation, self).__init__()

        def fillInUI(self, parent):
            self.parent = parent
            self.mediator = mediator
            self.mediator.connect_to_simulation_gui(self)

            self.padx = 10
            self.pady = 10

            # a scrollable frame to hold all the widgets
            scrolled_frame = \
                ScrolledFrame(parent, usehullsize=True, borderframe=False,
                              hull_width=780, hull_height=455)
            scrolled_frame.grid(row=0, column=0, sticky='w',
                                padx=self.padx, pady=self.pady)
            sf = scrolled_frame.interior()
            row_index = 0

            #  ========================
            #  = Create Recipe Widget =
            #  ========================
            f = Frame(sf)
            f.grid(row=row_index, column=0, columnspan=2,
                   padx=self.padx, pady=self.pady, sticky='nw')
            g = Group(f, tag_text='Recipe', tagindent=0,
                      tag_font=logicalfont('Helvetica', font_size,
                                           weight='bold'))
            g.pack()
            bb = ButtonBox(g.interior(), orient='horizontal', padx=2, pady=0)
            bb.grid(row=0, column=0, padx=15, pady=(15,5), sticky='w')
            bb.add("Load Recipe...", command=self._load_recipe)
            bb.add("Save Recipe...", command=self._save_recipe)
            self.recipe_widget = \
                recipe_widget_factory.build(g.interior(), self)
            self.recipe_widget.grid(row=1, column=0, padx=15, pady=(5,5))
            bb = ButtonBox(g.interior(), orient='horizontal', padx=2, pady=0)
            bb.grid(row=2, column=0, padx=15, pady=(5,5))
            bb.add("New", command=self._create_new_ingredient)
            bb.add("Clone", command=self._clone_ingredient)
            bb.add("Delete", command=self._delete_ingredient)
            bb.add(u"\u2191", command=self._move_ingredient_up)
            bb.add(u"\u2193", command=self._move_ingredient_down)
            bb.alignbuttons()
            self.all_ingredients_deletable = IntVar()
            cb = Checkbutton(g.interior(), text="Make all Ingredients deletable",
                             variable=self.all_ingredients_deletable,
                             command=self._ingr_del_checkbutton_cb)
            cb.grid(row=3, column=0, padx=15, pady=(0,15), sticky='e')

            #  =============================
            #  = Create Ingredients Widget =
            #  =============================
            f = Frame(sf)
            f.grid(row=row_index, column=2, columnspan=1,
                   padx=self.padx, pady=self.pady, sticky='nw')
            g = Group(f, tag_text='Ingredient Parameters',
                      tag_font=logicalfont('Helvetica', font_size,
                                           weight='bold'),
                      tagindent=0,
                      ring_borderwidth=0)
            g.pack()
            self.ingredients_widget = \
                ingredients_widget_factory.build(
                    g.interior(), self,
                    self.mediator.is_atomic_mc_available(),
                    self.mediator.is_langevin_available()
                )
            self.ingredients_widget.pack(padx=0, pady=15)
            row_index += 1

            #  ====================================
            #  = Create buttons to run simulation =
            #  ====================================
            f = Frame(sf)
            f.grid(row=row_index, column=0, columnspan=3,
                   padx=self.padx, pady=(0,0))
            B = Button(f, text="Run Next Ingredient",
                       command=self._start_simulation_single,
                       font=logicalfont('Helvetica', 4), padx=15)
            B.grid(row=0, column=0, padx=(0,10), pady=0)
            B = Button(f, text="Run All Ingredients",
                       command=self._start_simulation_all,
                       font=logicalfont('Helvetica', 4), padx=15)
            B.grid(row=0, column=1, padx=(0,10), pady=0)
            # B = Button(f, text="Stop Simulation",
            #            command=self._stop_simulation,
            #            font=logicalfont('Helvetica', 4), padx=15)
            # B.grid(row=0, column=2, padx=0, pady=0)
            L = Label(f, text="Simulation Progress:")
            L.grid(row=1, column=0, padx=0, pady=(0,0))
            self.progress_variable = StringVar()
            self.progress_variable.set('')
            L = Label(f, textvariable=self.progress_variable)
            L.grid(row=1, column=1, padx=0, pady=(0,0))
            row_index += 1

        def _start_simulation_single(self):
            self.mediator.start_simulation('next')

        def _start_simulation_all(self):
            self.mediator.start_simulation('all')

        def _stop_simulation(self):
            self.mediator.stop_simulation()

        def _save_structure(self):
            filename = asksaveasfilename()
            if filename == '':
                pass
            else:
                self.mediator.save_structure(filename)

        def _move_ingredient_up(self):
            ingr_id = self.recipe_widget.get_selected_ingredient()
            if ingr_id:
                self.mediator.move_ingredient_up(ingr_id)
            else:
                pass

        def _move_ingredient_down(self):
            ingr_id = self.recipe_widget.get_selected_ingredient()
            if ingr_id:
                self.mediator.move_ingredient_down(ingr_id)
            else:
                pass

        def _load_recipe(self):
            filename = askopenfilename()
            if filename == '':
                pass
            else:
                self.mediator.load_recipe(filename)

        def _save_recipe(self):
            filename = asksaveasfilename()
            if filename == '':
                pass
            else:
                self.mediator.save_recipe(filename)

        def _create_new_ingredient(self):
            algorithm_name = self.ingredients_widget.get_current_tab()
            self.mediator.create_new_ingredient(algorithm_name)

        def _clone_ingredient(self):
            ingr_id = self.recipe_widget.get_selected_ingredient()
            if ingr_id:
                self.mediator.clone_ingredient(ingr_id)
            else:
                pass

        def _delete_ingredient(self):
            ingr_id = self.recipe_widget.get_selected_ingredient()
            if ingr_id:
                self.mediator.delete_ingredient(ingr_id)
            else:
                pass

        def _ingr_del_checkbutton_cb(self):
            my_var = self.all_ingredients_deletable
            if my_var.get():
                self.mediator.set_ingredients_deletability(True)
            else:
                self.mediator.set_ingredients_deletability(False)

        #  ==========================
        #  = Recipe Widget Listener =
        #  ==========================
        def ingredient_selected(self):
            ingr_id = self.recipe_widget.get_selected_ingredient()
            current_algorithm = self.recipe_widget.get_algorithm(ingr_id)
            self.ingredients_widget.switch_to_tab(current_algorithm)
            self.mediator.ingredient_selected(ingr_id)

        #  ===============================
        #  = Ingredients Widget Listener =
        #  ===============================
        def update_ingredient_parameter(self, name, value, value_str=None,
                                        is_float=False):
            ingr_id = self.recipe_widget.get_selected_ingredient()
            if ingr_id:
                current_algorithm = self.recipe_widget.get_algorithm(ingr_id)
                current_num_steps = self.recipe_widget.get_num_steps(ingr_id)
                if name == 'algorithm' and current_algorithm == value:
                    pass
                elif name == 'num_steps' and current_num_steps == value:
                    pass
                else:
                    # print "Change %s to %s" % (name, str(value))
                    self.mediator.update_ingredient_parameter(ingr_id, name,
                        value, value_str, is_float)
            else:
                pass

        #  ============================
        #  = Parent Mediator Listener =
        #  ============================
        def reset(self):
            self.recipe_widget.reset()
            self.ingredients_widget.reset()

        #  =============================
        #  = Simulation Event Listener =
        #  =============================
        def display_new_ingredient(self, ingredient):
            self.recipe_widget.add_ingredient(ingredient)

        def hide_deleted_ingredient(self, ingredient_id):
            self.recipe_widget.delete_ingredient(ingredient_id)

        def refresh_ingredient_labels(self, updated_label_dict):
            self.recipe_widget.refresh_ingredient_labels(updated_label_dict)

        def refresh_ingredient_display(self, ingredient_id, name, value):
            self.recipe_widget.refresh_ingredient_display(
                ingredient_id, name, value)
            # if name in ['num_steps', 'output_interval']:
            self.ingredients_widget.change_value_on_all_pages(name, value)

        def display_ingredient_parameters(self, ingredient):
            self.ingredients_widget.display_ingredient_parameters(ingredient)

        def update_ingredient_position(self, ingredient_id, new_position):
            self.recipe_widget.update_ingredient_position(
                                ingredient_id, new_position)

        #  ============================
        #  = Simulator Event Listener =
        #  ============================
        def update_progress(self, msg):
            self.progress_variable.set(msg)

    return SophiaSimulation


def convert_string_to_float(s):
    if s in ('-', '+'):
        return 0.0
    else:
        return float(s)

class IngredientsWidgetFactory(object):
    """docstring for IngredientsWidgetFactory"""
    def __init__(self, padx=0, pady=0):
        self.padx = padx
        self.pady = pady
        self.pady_elements = 3
        self.entry_width = 9
        self.label_margin = 10
        self.scrolled_frame_width = 290
        self.scrolled_frame_height = 262
        self.format_fcn_dict = {}

    def _entry_callback_factory(self, entry_field, entry_key,
                                ingredients_widget, format_fcn_dict):
        def on_entry_change():
            format_fcn = format_fcn_dict[entry_key]
            raw_value = entry_field.getvalue()
            if raw_value == None or raw_value == '':
                pass
            else:
                value = format_fcn(raw_value)
                is_float = (format_fcn is convert_string_to_float)
                ingredients_widget.parent_dialog.update_ingredient_parameter(
                    entry_key, value, raw_value, is_float)
        return on_entry_change

    def _option_menu_callback_factory(self, option_menu, option_menu_key,
                                      ingredients_widget, format_fcn_dict):
        def on_entry_change(arg):
            format_fcn = format_fcn_dict[option_menu_key]
            raw_value = option_menu.getvalue()
            if raw_value == None:
                pass
            else:
                value = format_fcn(raw_value)
                ingredients_widget.parent_dialog.update_ingredient_parameter(
                    option_menu_key, value)
        return on_entry_change

    def build(self, parent_frame, parent_dialog, is_atomic_mc_available,
              is_langevin_available):
        iw = IngredientsWidget(parent_frame)
        iw.connect_to_parent_dialog(parent_dialog)
        self.build_md_widget(iw)
        self.build_em_widget(iw)
        if is_atomic_mc_available:
            self.build_mc_widget(iw)
        else:
            pass
        if is_langevin_available:
            self.build_ld_widget(iw)
        else:
            pass
        iw.format_fcn_dict = self.format_fcn_dict
        iw.notebook.tab('MD').focus_set()
        iw.notebook.setnaturalsize()
        return iw

    def build_md_widget(self, ingredients_widget):
        md_page = ingredients_widget.notebook.add('MD')
        sf = ScrolledFrame(md_page, usehullsize=True,
                           hull_width=self.scrolled_frame_width,
                           hull_height=self.scrolled_frame_height)
        sf.grid(row=0, column=0, sticky='w')
        f = sf.interior()

        md_entries = {}
        md_option_menus = {}
        entry_key = 'num_steps'
        m = EntryField(f, labelpos='w', value='10000',
                       label_text = 'Num Steps',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 1,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        md_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'output_interval'
        m = EntryField(f, labelpos='w', value='100',
                       label_text = 'Output Interval (steps)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        md_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'time_step'
        m = EntryField(f, labelpos='w', value='1',
                       label_text = 'Time Step (fs)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0.001,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        md_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        option_menu_key = 'initialize_velocities'
        f2 = Frame(f)
        L = Label(f2, text='Initialize Velocities')
        L.grid(row=0, column=0, padx=(3,30), sticky='w')
        m = OptionMenu(f2, items=['Off', 'x', 'xy', 'xyz'],
                       menubutton_textvariable=ingredients_widget.velocities,
                       menubutton_width=self.entry_width)
        ingredients_widget.velocities.set('xyz')
        self.format_fcn_dict[option_menu_key] = str
        callback_fcn = self._option_menu_callback_factory(
                        m, option_menu_key, ingredients_widget,
                        self.format_fcn_dict)
        m.configure(command=callback_fcn)
        md_option_menus[option_menu_key] = m
        m.grid(row=0, column=1, sticky='w')
        f2.pack(padx=self.padx, pady=self.pady_elements)

        option_menu_key = 'thermostat'
        f2 = Frame(f)
        L = Label(f2, text='Thermostat')
        L.grid(row=0, column=0, padx=(3,75), sticky='w')
        m = OptionMenu(f2, items=['Off', 'Rescale', 'Nose'],
                       menubutton_textvariable=ingredients_widget.thermostat,
                       menubutton_width=self.entry_width)
        ingredients_widget.thermostat.set('Rescale')
        self.format_fcn_dict[option_menu_key] = str
        callback_fcn = self._option_menu_callback_factory(
                        m, option_menu_key, ingredients_widget,
                        self.format_fcn_dict)
        m.configure(command=callback_fcn)
        md_option_menus[option_menu_key] = m
        m.grid(row=0, column=1, sticky='w')
        f2.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'initial_T'
        m = EntryField(f, labelpos='w', value='1.0',
                       label_text = 'Initial Temperature (K)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        md_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'final_T'
        m = EntryField(f, labelpos='w', value='100.0',
                       label_text = 'Final Temperature (K)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        md_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'thermostat_interval'
        m = EntryField(f, labelpos='w', value='50',
                       label_text = 'Rescale Interval (steps)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        md_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'thermostat_relaxation_time'
        m = EntryField(f, labelpos='w', value='0.2',
                       label_text = 'Nose Thermostat\nRelaxation Time',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0.0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        md_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        option_menu_key = 'barostat'
        f2 = Frame(f)
        L = Label(f2, text='Barostat')
        L.grid(row=0, column=0, padx=(5,95), sticky='w')
        m = OptionMenu(f2, items=['Off', 'Andersen'],
                       menubutton_textvariable=ingredients_widget.barostat,
                       menubutton_width=self.entry_width)
        ingredients_widget.barostat.set('Off')
        self.format_fcn_dict[option_menu_key] = str
        callback_fcn = self._option_menu_callback_factory(
                        m, option_menu_key, ingredients_widget,
                        self.format_fcn_dict)
        m.configure(command=callback_fcn)
        md_option_menus[option_menu_key] = m
        m.grid(row=0, column=1, sticky='w')
        f2.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'pressure'
        m = EntryField(f, labelpos='w', value='1.0',
                       label_text = 'Pressure (atm)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0.0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        md_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'barostat_relaxation_time'
        m = EntryField(f, labelpos='w', value='1.5',
                       label_text = 'Andersen Barostat\nRelaxation Time',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0.0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        md_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'remove_translation_interval'
        m = EntryField(f, labelpos='w', value='100',
                       label_text = 'Remove Translation\nInterval (steps)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        md_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'remove_rotation_interval'
        m = EntryField(f, labelpos='w', value='100',
                       label_text = 'Remove Rotation\nInterval (steps)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        md_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        alignlabels(md_entries.values())
        ingredients_widget.md_entries = md_entries
        ingredients_widget.md_option_menus = md_option_menus

    def build_ld_widget(self, ingredients_widget):
        ld_page = ingredients_widget.notebook.add('LD')
        sf = ScrolledFrame(ld_page, usehullsize=True,
                           hull_width=self.scrolled_frame_width,
                           hull_height=self.scrolled_frame_height)
        sf.grid(row=0, column=0, sticky='w')
        f = sf.interior()

        ld_entries = {}
        ld_option_menus = {}
        entry_key = 'num_steps'
        m = EntryField(f, labelpos='w', value='10000',
                       label_text = 'Num Steps',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 1,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'output_interval'
        m = EntryField(f, labelpos='w', value='100',
                       label_text = 'Output Interval (steps)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'time_step'
        m = EntryField(f, labelpos='w', value='1.0',
                       label_text = 'Time Step (fs)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0.001,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        option_menu_key = 'initialize_velocities'
        f2 = Frame(f)
        L = Label(f2, text='Initialize Velocities')
        L.grid(row=0, column=0, padx=(3,30), sticky='w')
        m = OptionMenu(f2, items=['Off', 'x', 'xy', 'xyz'],
                       menubutton_textvariable=ingredients_widget.velocities,
                       menubutton_width=self.entry_width)
        ingredients_widget.velocities.set('xyz')
        self.format_fcn_dict[option_menu_key] = str
        callback_fcn = self._option_menu_callback_factory(
                        m, option_menu_key, ingredients_widget,
                        self.format_fcn_dict)
        m.configure(command=callback_fcn)
        ld_option_menus[option_menu_key] = m
        m.grid(row=0, column=1, sticky='w')
        f2.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'friction'
        m = EntryField(f, labelpos='w', value='0.1',
                       label_text = 'Friction (1/ps)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0.0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        option_menu_key = 'thermostat'
        f2 = Frame(f)
        L = Label(f2, text='Thermostat')
        L.grid(row=0, column=0, padx=(3,75), sticky='w')
        m = OptionMenu(f2, items=['Off', 'Rescale', 'Nose'],
                       menubutton_textvariable=ingredients_widget.thermostat,
                       menubutton_width=self.entry_width)
        ingredients_widget.thermostat.set('Rescale')
        self.format_fcn_dict[option_menu_key] = str
        callback_fcn = self._option_menu_callback_factory(
                        m, option_menu_key, ingredients_widget,
                        self.format_fcn_dict)
        m.configure(command=callback_fcn)
        ld_option_menus[option_menu_key] = m
        m.grid(row=0, column=1, sticky='w')
        f2.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'initial_T'
        m = EntryField(f, labelpos='w', value='1.0',
                       label_text = 'Initial Temperature (K)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'final_T'
        m = EntryField(f, labelpos='w', value='100.0',
                       label_text = 'Final Temperature (K)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'thermostat_interval'
        m = EntryField(f, labelpos='w', value='50',
                       label_text = 'Rescale Interval (steps)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'thermostat_relaxation_time'
        m = EntryField(f, labelpos='w', value='0.2',
                       label_text = 'Nose Thermostat\nRelaxation Time',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0.0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        option_menu_key = 'barostat'
        f2 = Frame(f)
        L = Label(f2, text='Barostat')
        L.grid(row=0, column=0, padx=(5,95), sticky='w')
        m = OptionMenu(f2, items=['Off', 'Andersen'],
                       menubutton_textvariable=ingredients_widget.barostat,
                       menubutton_width=self.entry_width)
        ingredients_widget.barostat.set('Off')
        self.format_fcn_dict[option_menu_key] = str
        callback_fcn = self._option_menu_callback_factory(
                        m, option_menu_key, ingredients_widget,
                        self.format_fcn_dict)
        m.configure(command=callback_fcn)
        ld_option_menus[option_menu_key] = m
        m.grid(row=0, column=1, sticky='w')
        f2.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'pressure'
        m = EntryField(f, labelpos='w', value='1.0',
                       label_text = 'Pressure (atm)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0.0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'barostat_relaxation_time'
        m = EntryField(f, labelpos='w', value='1.5',
                       label_text = 'Andersen Barostat\nRelaxation Time',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0.0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'remove_translation_interval'
        m = EntryField(f, labelpos='w', value='100',
                       label_text = 'Remove Translation\nInterval (steps)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'remove_rotation_interval'
        m = EntryField(f, labelpos='w', value='100',
                       label_text = 'Remove Rotation\nInterval (steps)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        ld_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        alignlabels(ld_entries.values())
        ingredients_widget.ld_entries = ld_entries
        ingredients_widget.ld_option_menus = ld_option_menus

    def build_em_widget(self, ingredients_widget):
        em_page = ingredients_widget.notebook.add('EM')
        sf = ScrolledFrame(em_page, usehullsize=True,
                           hull_width=self.scrolled_frame_width,
                           hull_height=self.scrolled_frame_height)
        sf.grid(row=0, column=0, sticky='w')
        f = sf.interior()
        em_entries = {}
        em_option_menus = {}

        option_menu_key = 'minimization_type'
        f2 = Frame(f)
        L = Label(f2, text='Algorithm')
        L.grid(row=0, column=0, padx=(3,83))
        m = OptionMenu(
                f2, items=['SD', 'CG'],
                menubutton_textvariable=ingredients_widget.minimization_type,
                menubutton_width=self.entry_width
            )
        ingredients_widget.minimization_type.set('SD')
        self.format_fcn_dict[option_menu_key] = str
        callback_fcn = self._option_menu_callback_factory(
                        m, option_menu_key, ingredients_widget,
                        self.format_fcn_dict)
        m.configure(command=callback_fcn)
        em_option_menus[option_menu_key] = m
        m.grid(row=0, column=1)
        f2.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'num_steps'
        m = EntryField(f, labelpos='w', value='10000',
                       label_text = 'Num Steps',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 1,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        em_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'output_interval'
        m = EntryField(f, labelpos='w', value='100',
                       label_text = 'Output Interval (steps)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        em_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'initial_step_size'
        m = EntryField(f, labelpos='w', value='0.1',
                       label_text = 'Initial Step Size (nm)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0.0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        em_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'convergence'
        m = EntryField(f, labelpos='w', value='-9',
                       label_text = 'Convergence (10^x)\n(kJ/mol/nm)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'max': -1,
                                   'maxstrict': True})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        em_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        alignlabels(em_entries.values())
        ingredients_widget.em_entries = em_entries
        ingredients_widget.em_option_menus = em_option_menus

    def build_mc_widget(self, ingredients_widget):
        mc_page = ingredients_widget.notebook.add('MC')
        sf = ScrolledFrame(mc_page, usehullsize=True,
                           hull_width=self.scrolled_frame_width,
                           hull_height=self.scrolled_frame_height)
        sf.grid(row=0, column=0, sticky='w')
        f = sf.interior()
        mc_entries = {}
        mc_option_menus = {}

        entry_key = 'num_steps'
        m = EntryField(f, labelpos='w', value='10000',
                       label_text = 'Num Steps',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 1,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        mc_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'output_interval'
        m = EntryField(f, labelpos='w', value='100',
                       label_text = 'Output Interval (steps)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'numeric', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = int
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        mc_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'initial_step_size'
        m = EntryField(f, labelpos='w', value='0.1',
                       label_text = 'Initial Step Size (nm)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0.0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        mc_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'initial_T'
        m = EntryField(f, labelpos='w', value='1.0',
                       label_text = 'Initial Temperature (K)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        mc_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        entry_key = 'final_T'
        m = EntryField(f, labelpos='w', value='100.0',
                       label_text = 'Final Temperature (K)',
                       entry_width=self.entry_width,
                       labelmargin=self.label_margin,
                       validate = {'validator': 'real', 'min': 0,
                                   'minstrict': False})
        self.format_fcn_dict[entry_key] = convert_string_to_float
        callback_fcn = self._entry_callback_factory(
                        m, entry_key, ingredients_widget, self.format_fcn_dict)
        m.configure(modifiedcommand=callback_fcn)
        mc_entries[entry_key] = m
        m.pack(padx=self.padx, pady=self.pady_elements)

        alignlabels(mc_entries.values())
        ingredients_widget.mc_entries = mc_entries
        ingredients_widget.mc_option_menus = mc_option_menus


class IngredientsWidget(object):
    """docstring for IngredientsWidget"""
    def __init__(self, parent_frame):
        self.notebook = \
            NoteBook(parent_frame, raisecommand=self._on_page_switch,
                     pagemargin=15, borderwidth=2)
        self.parent_dialog = None
        self.md_entries = None
        self.md_option_menus = None
        self.em_entries = None
        self.em_option_menus = None
        self.ld_entries = None
        self.ld_option_menus = None
        self.mc_entries = None
        self.mc_option_menus = None
        self.minimization_type = StringVar()
        self.velocities = StringVar()
        self.thermostat = StringVar()
        self.barostat = StringVar()
        self.format_fcn_dict = None

    def _on_page_switch(self, page_name):
        self.parent_dialog.update_ingredient_parameter('algorithm', page_name)

    def connect_to_parent_dialog(self, parent_dialog):
        self.parent_dialog = parent_dialog

    def pack(self, padx, pady):
        self.notebook.pack(padx=padx, pady=pady)

    def reset(self):
        pass

    def display_ingredient_parameters(self, ingredient):
        if self.md_entries:
            for entry_key, entry in self.md_entries.iteritems():
                ingr_value = ingredient.get(entry_key)
                format_fcn = self.format_fcn_dict[entry_key]
                entry_value = format_fcn(entry.getvalue())
                if ingr_value == entry_value:
                    pass
                else:
                    self.md_entries[entry_key].setvalue(ingr_value)
        if self.md_option_menus:
            for option_menu_key, option_menu in self.md_option_menus.iteritems():
                ingr_value = ingredient.get(option_menu_key)
                format_fcn = self.format_fcn_dict[option_menu_key]
                option_menu_value = format_fcn(option_menu.getvalue())
                if ingr_value == option_menu_value:
                    pass
                else:
                    self.md_option_menus[option_menu_key].setvalue(ingr_value)
        if self.em_entries:
            for entry_key, entry in self.em_entries.iteritems():
                ingr_value = ingredient.get(entry_key)
                format_fcn = self.format_fcn_dict[entry_key]
                entry_value = format_fcn(entry.getvalue())
                if ingr_value == entry_value:
                    pass
                else:
                    self.em_entries[entry_key].setvalue(ingr_value)
        if self.em_option_menus:
            for option_menu_key, option_menu in self.em_option_menus.iteritems():
                ingr_value = ingredient.get(option_menu_key)
                format_fcn = self.format_fcn_dict[option_menu_key]
                option_menu_value = format_fcn(option_menu.getvalue())
                if ingr_value == option_menu_value:
                    pass
                else:
                    self.em_option_menus[option_menu_key].setvalue(ingr_value)
        if self.ld_entries:
            for entry_key, entry in self.ld_entries.iteritems():
                ingr_value = ingredient.get(entry_key)
                format_fcn = self.format_fcn_dict[entry_key]
                entry_value = format_fcn(entry.getvalue())
                if ingr_value == entry_value:
                    pass
                else:
                    self.ld_entries[entry_key].setvalue(ingr_value)
        if self.ld_option_menus:
            for option_menu_key, option_menu in self.ld_option_menus.iteritems():
                ingr_value = ingredient.get(option_menu_key)
                format_fcn = self.format_fcn_dict[option_menu_key]
                option_menu_value = format_fcn(option_menu.getvalue())
                if ingr_value == option_menu_value:
                    pass
                else:
                    self.ld_option_menus[option_menu_key].setvalue(ingr_value)
        if self.mc_entries:
            for entry_key, entry in self.mc_entries.iteritems():
                ingr_value = ingredient.get(entry_key)
                format_fcn = self.format_fcn_dict[entry_key]
                entry_value = format_fcn(entry.getvalue())
                if ingr_value == entry_value:
                    pass
                else:
                    self.mc_entries[entry_key].setvalue(ingr_value)
        if self.mc_option_menus:
            for option_menu_key, option_menu in self.mc_option_menus.iteritems():
                ingr_value = ingredient.get(option_menu_key)
                format_fcn = self.format_fcn_dict[option_menu_key]
                option_menu_value = format_fcn(option_menu.getvalue())
                if ingr_value == option_menu_value:
                    pass
                else:
                    self.mc_option_menus[option_menu_key].setvalue(ingr_value)

    def change_value_on_all_pages(self, name, value):
        try:
            self.md_entries[name].setvalue(value)
        except:
            pass
        try:
            self.em_entries[name].setvalue(value)
        except:
            pass
        try:
            self.ld_entries[name].setvalue(value)
        except:
            pass
        try:
            self.mc_entries[name].setvalue(value)
        except:
            pass

    def switch_to_tab(self, current_algorithm):
        self.notebook.selectpage(current_algorithm)

    def get_current_tab(self):
        return self.notebook.getcurselection()


class RecipeWidgetFactory(object):
    """docstring for RecipeWidgetFactory"""
    def __init__(self):
        self.tree_col_width = 100

    def build(self, parent_frame, parent_dialog):
        rw = RecipeWidget(parent_frame, columns=('#','algorithm',
                          'num_steps', 'status'), selectmode='browse')
        rw.connect_to_parent_dialog(parent_dialog)
        rw.tree.column('#', width=40, anchor='center')
        rw.tree.column('algorithm', width=self.tree_col_width, anchor='center')
        rw.tree.column('num_steps', width=self.tree_col_width, anchor='center')
        rw.tree.column('status', width=self.tree_col_width, anchor='center')
        rw.tree.heading('#', text='#')
        rw.tree.heading('algorithm', text='Algorithm')
        rw.tree.heading('num_steps', text='Steps')
        rw.tree.heading('status', text='Status')
        return rw


class RecipeWidget(Treeview):
    """docstring for RecipeWidget"""
    def __init__(self, parent_frame, columns, selectmode):
        self.tree = Treeview(parent_frame, columns=columns,
                              selectmode=selectmode, show='headings',
                              height=10)
        self.tree.bind('<<TreeviewSelect>>', self._item_selected)
        self.column_names = columns
        self.parent_dialog = None

    def _item_selected(self, *args, **kwargs):
        self.parent_dialog.ingredient_selected()

    def connect_to_parent_dialog(self, parent_dialog):
        self.parent_dialog = parent_dialog

    def grid(self, row, column, padx, pady):
        self.tree.grid(row=row, column=column, padx=padx, pady=pady)

    def reset(self):
        for c in self.tree.get_children():
            self.tree.delete(c)

    def add_ingredient(self, ingredient):
        self.tree.insert('', 'end', ingredient.id)
        self.tree.set(ingredient.id, '#', ingredient.label)
        self.tree.set(ingredient.id, 'algorithm', ingredient.get('algorithm'))
        self.tree.set(ingredient.id, 'num_steps', ingredient.get('num_steps'))
        self.tree.set(ingredient.id, 'status', ingredient.get('status'))
        self.tree.selection_set(ingredient.id)

    def delete_ingredient(self, ingredient_id):
        self.tree.delete(ingredient_id)

    def get_selected_ingredient(self):
        try:
            selected_ingredient_id = self.tree.selection()[0]
        except IndexError:
            selected_ingredient_id = None
        return selected_ingredient_id

    def get_algorithm(self, ingredient_id):
        # tree.item(id, 'values') returns a list with same
        # order as columns so algorithm is the second value
        return self.tree.item(ingredient_id, 'values')[1]

    def get_num_steps(self, ingredient_id):
        # tree.item(id, 'values') returns a list with same
        # order as columns so num_steps is the third value
        return self.tree.item(ingredient_id, 'values')[2]

    def refresh_ingredient_labels(self, updated_label_dict):
        for id, label in updated_label_dict.iteritems():
            self.tree.set(id, '#', label)

    def refresh_ingredient_display(self, ingredient_id, name, value):
        if name in self.column_names:
            self.tree.set(ingredient_id, name, value)
        else:
            pass

    def update_ingredient_position(self, ingredient_id, new_position):
        self.tree.move(ingredient_id, '', new_position)
