from os.path import splitext
from Pmw import ButtonBox, MessageBar, logicalfont
from tkFileDialog import asksaveasfilename, askopenfilename
from chimera.baseDialog import ModelessDialog

from ..const import SOPHIA_VERSION


def build_main_dialog_class(mediator):

    class SophiaMain(ModelessDialog):
        """docstring for SophiaMain"""
        name = "sophia_main"
        buttons = ("Close")
        # help = ("SophiaUI.html", SophiaUI)
        title = "Sophia v%s" % SOPHIA_VERSION
        provideStatus = True
        BUTTON_WIDTH = 19

        def __init__(self):
            super(SophiaMain, self).__init__()

        def fillInUI(self, parent):
            self.parent = parent
            self.mediator = mediator
            self.mediator.connect_to_main_gui(self)

            self.padx = 10

            # Create a vertical button box
            bb = ButtonBox(parent, orient='vertical', padx=self.padx,
                           pady=2)
            bb.grid(row=0, column=0)

            # Add buttons to the ButtonBox.
            bb.add('New Simulation', command=self._new_sim, width=self.BUTTON_WIDTH,
                   font=logicalfont('Helvetica'))
            bb.add('Load Simulation...', command=self._load_sim,
                    font=logicalfont('Helvetica'))
            bb.add('Save Simulation...', command=self._save_sim,
                    font=logicalfont('Helvetica'))
            bb.add('Quit Sophia', command=self._quit_sophia,
                    font=logicalfont('Helvetica'))

            # Make all the buttons the same width.
            bb.alignbuttons()

            mb = MessageBar(parent, entry_width=20,
                            entry_relief='groove', labelpos = 'w',
                            label_text = 'Status:', silent=True,
                            labelmargin=5)
            mb.grid(row=1, column=0, padx=self.padx, pady=(15,0),
                    sticky='w')
            self.message_bar = mb

        def _new_sim(self):
            self.mediator.new_simulation()

        def _load_sim(self):
            filename = askopenfilename(defaultextension='.sophia')
            if filename is None or filename == '':
                pass
            else:
                file_prefix, file_extension = splitext(filename)
                self.mediator.load_simulation(file_prefix)

        def _save_sim(self):
            filename = asksaveasfilename()
            if filename == '':
                pass
            else:
                file_prefix, file_extension = splitext(filename)
                self.mediator.save_simulation(file_prefix)

        def _quit_sophia(self):
            self.mediator.quit_sophia()

        def status(self, message):
            self.message_bar.message('state', message)

    return SophiaMain
