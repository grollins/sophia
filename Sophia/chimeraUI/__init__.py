from os.path import split, join, exists
import cPickle as pickle
from chimera import runCommand, openModels
import chimera.dialogs
import chimera.tkgui
from StructMeasure import gui as struct_measure_gui
from StructMeasure.DistMonitor import removeDistance, addDistance

from .dialogs import create_dialog_classes
from .translators import UniverseTranslatorFactory
from .renderer import RendererFactory
from .plotter import PlotterFactory
from .const import PLOTS, BOX_NAME


class ToolbarManager(object):
    """docstring for ToolbarManager."""
    def __init__(self):
        # assume icons are stored in same directory as this module
        self.icon_directory, _ = split(__file__)
        self.sophia_button = None
        self.reset_view_button = None
        self.bounding_box_button = None

    def create_sophia_button(self, main_gui_name):
        sophia_icon = join(self.icon_directory, 'sophia_icon.tiff')

        # create function to display Sophia dialogs
        def display_sophia_main():
            chimera.dialogs.display(main_gui_name)

        # Create the Chimera toolbar button that displays the Sophia dialogs
        # when pressed.
        self.sophia_button = \
            chimera.tkgui.app.toolbar.add(sophia_icon, display_sophia_main,
                                          'Sophia', None)

    def create_reset_view_button(self):
        reset_view_icon = join(self.icon_directory, 'reset_view.tiff')

        def reset_chimera_view():
            runCommand('reset')

        self.reset_view_button = \
            chimera.tkgui.app.toolbar.add(reset_view_icon, reset_chimera_view,
                                         'Reset View', None)

    def create_draw_box_button(self, mediator):
        draw_box_icon = join(self.icon_directory, 'draw_box.tiff')

        def draw_box_2d():
            if BOX_NAME in [m.name for m in openModels.list()]:
                pass
            else:
                box_length_in_nm = mediator.get_box_length()
                box_length_in_angstroms = box_length_in_nm * 10.
                cmd = 'shape rectangle width %.2f height %.2f color white modelName %s' \
                      % (box_length_in_angstroms, box_length_in_angstroms, BOX_NAME)
                runCommand(cmd)

        self.bounding_box_button = \
            chimera.tkgui.app.toolbar.add(draw_box_icon, draw_box_2d,
                                          'Draw 2D Box', None)


class ChimeraUI(object):
    """
    ChimeraUI encapsulates how the various Chimera interface components interact with
    each other. It sends UI signals to and receives simulation signals from
    the SophiaMediator.
    """

    def __init__(self):
        self.renderer_factory = RendererFactory()
        self.plotter_factory = PlotterFactory()
        self.renderer = None
        self.plotter = None
        self.mediator = None
        self.universe = None
        self.trajectory = None
        self.main_gui = None
        self.trajectory_viewer_gui = None
        self.simulation_gui = None
        self.universe_gui = None

        # build dialog classes, injecting ChimeraUI as mediator
        dialog_classes = create_dialog_classes(self)
        MainDialog = dialog_classes[0]
        SimulationDialog = dialog_classes[1]
        TrajectoryViewerDialog = dialog_classes[2]
        UniverseDialog = dialog_classes[3]
        self.main_gui_name = MainDialog.name
        self.trajectory_viewer_gui_name = TrajectoryViewerDialog.name
        self.sim_gui_name = SimulationDialog.name
        self.universe_gui_name = UniverseDialog.name

        # register Sophia dialogs with Chimera
        chimera.dialogs.register(self.main_gui_name, MainDialog)
        chimera.dialogs.register(self.sim_gui_name, SimulationDialog)
        chimera.dialogs.register(self.trajectory_viewer_gui_name,
                                 TrajectoryViewerDialog)
        chimera.dialogs.register(self.universe_gui_name, UniverseDialog)

        self.toolbar_manager = ToolbarManager()
        self.toolbar_manager.create_sophia_button(self.main_gui_name)
        self.toolbar_manager.create_reset_view_button()

    def connect_to_mediator(self, mediator):
        self.mediator = mediator
        self.toolbar_manager.create_draw_box_button(mediator)

    #  =================================
    #  = SophiaMediator Event Listener =
    #  =================================
    def status(self, message):
        self.main_gui.status(message)
        if message == 'Universe loaded':
            self.universe_gui.universe_loaded()
        elif message == 'Universe error':
            self.universe_gui.universe_error()

    def display_universe_details(self, universe):
        self.universe_gui.display_universe_details(universe)

    def lock_universe_panel(self):
        self.universe_gui.lock_universe_panel()

    def save_bonds(self, file_prefix):
        dialog_name = struct_measure_gui.StructMeasure.name
        d = chimera.dialogs.find(dialog_name)
        if d is None:
            pass
        else:
            mmtk_bonds = []
            for bond in d.distances:
                chimera_atom_list = bond.atoms
                mmtk_atom_list = \
                    self.renderer.map_atoms_chimera2mmtk(chimera_atom_list)
                mmtk_atom_indices = [a.index for a in mmtk_atom_list]
                mmtk_bonds.append(mmtk_atom_indices)
            filename = file_prefix + '_bonds.pkl'
            with open(filename, 'w') as f:
                pickle.dump(mmtk_bonds, f)

    def restore_bonds(self, file_prefix):
        filename = file_prefix + '_bonds.pkl'
        if not exists(filename):
            pass
        else:
            with open(filename, 'r') as f:
                mmtk_bonds = pickle.load(f)
            for mmtk_atom_indices in mmtk_bonds:
                chimera_atom_list = \
                    self.renderer.map_atom_indices_mmtk2chimera(
                        mmtk_atom_indices)
                addDistance(chimera_atom_list[0], chimera_atom_list[1])

    def save_angles(self, file_prefix):
        dialog_name = struct_measure_gui.StructMeasure.name
        d = chimera.dialogs.find(dialog_name)
        if d is None:
            pass
        else:
            mmtk_angles = []
            for chimera_atom_list in d.angleInfo:
                mmtk_atom_list = \
                    self.renderer.map_atoms_chimera2mmtk(chimera_atom_list)
                mmtk_atom_indices = [a.index for a in mmtk_atom_list]
                mmtk_angles.append(mmtk_atom_indices)
            filename = file_prefix + '_angles.pkl'
            with open(filename, 'w') as f:
                pickle.dump(mmtk_angles, f)

    def restore_angles(self, file_prefix):
        filename = file_prefix + '_angles.pkl'
        if not exists(filename):
            pass
        else:
            with open(filename, 'r') as f:
                mmtk_angles = pickle.load(f)
            chimera_angles = []
            for mmtk_atom_indices in mmtk_angles:
                chimera_atom_list = \
                    self.renderer.map_atom_indices_mmtk2chimera(mmtk_atom_indices)
                chimera_angles.append(chimera_atom_list)

            if chimera_angles:
                dialog_name = struct_measure_gui.StructMeasure.name
                d = chimera.dialogs.find(dialog_name)
                if d is None:
                    self.open_structure_measurements_dialog()
                d = chimera.dialogs.find(dialog_name)
                d.angleInfo = chimera_angles
                d.angleTable.setData(d.angleInfo)

    # =============================
    # = Simulation Event Listener =
    # =============================
    def display_new_ingredient(self, ingredient):
        self.simulation_gui.display_new_ingredient(ingredient)

    def hide_deleted_ingredient(self, ingredient_id, ingr_is_done):
        self.simulation_gui.hide_deleted_ingredient(ingredient_id)
        if ingr_is_done:
            # update the plots and trajectory rendering to account
            # for the epoch data that was deleted
            if self.plotter:
                self.plotter.close_plots()
            else:
                pass
            self.trajectory_viewer_gui.reset()
            chimera.dialogs.display(self.trajectory_viewer_gui_name)
            length_of_first_epoch = self.trajectory.get_epoch_length(0)
            if length_of_first_epoch is None:
                # we've deleted all of the epochs
                pass
            else:
                self.trajectory_viewer_gui.load_trajectory(
                self.trajectory.num_epochs, length_of_first_epoch)
                self.plotter = self.plotter_factory.build(self.trajectory)
        else:
            # ingredient wasn't done, so it didn't have any epoch data
            # associated with it
            pass

    def refresh_ingredient_labels(self, updated_label_dict):
        self.simulation_gui.refresh_ingredient_labels(updated_label_dict)

    def refresh_ingredient_display(self, ingredient_id, name, value):
        self.simulation_gui.refresh_ingredient_display(
            ingredient_id, name, value)

    def display_ingredient_parameters(self, ingredient):
        self.simulation_gui.display_ingredient_parameters(ingredient)

    def update_ingredient_position(self, ingredient_id, new_position):
        self.simulation_gui.update_ingredient_position(
                                ingredient_id, new_position)

    # =============================
    # = Simulator Event Listener =
    # =============================
    def render_new_universe(self, universe):
        self.universe = universe
        self.renderer = self.renderer_factory.build(UniverseTranslatorFactory())
        self.renderer.render_new_universe(universe)

    def render_trajectory(self, trajectory):
        chimera.dialogs.display(self.trajectory_viewer_gui_name)
        length_of_first_epoch = trajectory.get_epoch_length(0)
        self.trajectory_viewer_gui.load_trajectory(trajectory.num_epochs,
                                                   length_of_first_epoch)
        if self.plotter:
            plots_closed = self.plotter.close_plots()
            self.plotter = self.plotter_factory.build(trajectory)
            self.plotter.restore_closed_plots(plots_closed)
        else:
            self.plotter = self.plotter_factory.build(trajectory)

        self.trajectory = trajectory

    def reset_dialogs(self):
        chimera.dialogs.display(self.sim_gui_name)
        chimera.dialogs.display(self.universe_gui_name)
        chimera.dialogs.display(self.trajectory_viewer_gui_name)
        self.universe_gui.reset()
        self.simulation_gui.reset()
        self.trajectory_viewer_gui.reset()

    def update_progress(self, msg):
        self.simulation_gui.update_progress(msg)

    # ===========================
    # = Main GUI Event Listener =
    # ===========================
    def connect_to_main_gui(self, main_gui):
        self.main_gui = main_gui

    def new_simulation(self):
        if self.renderer:
            self.renderer.stop_rendering()
        if self.plotter:
            self.plotter.close_plots()
        self._remove_bonds()
        self._remove_angles()
        self.mediator.new_simulation()

    def load_simulation(self, file_prefix):
        if self.renderer:
            self.renderer.stop_rendering()
        if self.plotter:
            self.plotter.close_plots()
        self._remove_bonds()
        self._remove_angles()
        self.mediator.load_simulation(file_prefix)

    def save_simulation(self, file_prefix):
        self.mediator.save_simulation(file_prefix)

    def quit_sophia(self):
        if self.main_gui:
            self.main_gui.Close()
        if self.universe_gui:
            self.universe_gui.Close()
        if self.simulation_gui:
            self.simulation_gui.Close()
        if self.trajectory_viewer_gui:
            self.trajectory_viewer_gui.Close()
        if self.renderer:
            self.renderer.stop_rendering()
        if self.plotter:
            self.plotter.close_plots()
        self._remove_bonds()
        self._remove_angles()
        self.mediator.quit_sophia()

    def _remove_bonds(self):
        dialog_name = struct_measure_gui.StructMeasure.name
        d = chimera.dialogs.find(dialog_name)
        if d is None:
            pass
        else:
            for b in d.distances:
                removeDistance(b)

    def _remove_angles(self):
        dialog_name = struct_measure_gui.StructMeasure.name
        d = chimera.dialogs.find(dialog_name)
        if d is None:
            pass
        else:
            d.angleInfo = []
            d.angleTable.setData(d.angleInfo)

    #  ===============================
    #  = Universe GUI Event Listener =
    #  ===============================
    def connect_to_universe_gui(self, universe_gui):
        self.universe_gui = universe_gui

    def load_universe_from_pdb(self, pdb_filename, force_field_name,
                               ff_mod_filename, box_length, cutoff):
        self.mediator.load_universe_from_pdb(pdb_filename, force_field_name,
                                             ff_mod_filename, box_length, cutoff)

    def reset_universe(self):
        self.mediator.reset_universe()

    #  =================================
    #  = Simulation GUI Event Listener =
    #  =================================
    def connect_to_simulation_gui(self, simulation_gui):
        self.simulation_gui = simulation_gui

    def get_topologies(self):
        return self.mediator.get_topologies()

    def get_force_fields(self):
        return self.mediator.get_force_fields()

    def start_simulation(self, mode):
        self.mediator.start_simulation(mode)

    def stop_simulation(self):
        self.mediator.stop_simulation()

    def save_structure(self, filename):
        frame_number = self.trajectory_viewer_gui.current_frame_num.get()
        self.mediator.save_structure(frame_number, filename)

    def move_ingredient_up(self, ingredient_id):
        self.mediator.move_ingredient_up(ingredient_id)

    def move_ingredient_down(self, ingredient_id):
        self.mediator.move_ingredient_down(ingredient_id)

    def load_recipe(self, filename):
        self.simulation_gui.reset()
        self.mediator.load_recipe(filename)

    def save_recipe(self, filename):
        self.mediator.save_recipe(filename)

    def create_new_ingredient(self, algorithm_name):
        self.mediator.create_new_ingredient(algorithm_name)

    def clone_ingredient(self, ingredient_id):
        self.mediator.clone_ingredient(ingredient_id)

    def delete_ingredient(self, ingredient_id):
        self.mediator.delete_ingredient(ingredient_id)

    def update_ingredient_parameter(self, ingredient_id, name, value,
                                    value_str=None, is_float=False):
        self.mediator.update_ingredient_parameter(ingredient_id, name, value,
                                                  value_str, is_float)

    def ingredient_selected(self, ingredient_id):
        self.mediator.ingredient_selected(ingredient_id)

    def set_ingredients_deletability(self, are_deletable):
        self.mediator.set_ingredients_deletability(are_deletable)

    def is_langevin_available(self):
        return self.mediator.is_langevin_available()

    def is_atomic_mc_available(self):
        return self.mediator.is_atomic_mc_available()

    # ====================================
    # = Trajectory Viewer Event Listener =
    # ====================================
    def connect_to_trajectory_viewer_gui(self, trajectory_viewer_gui):
        self.trajectory_viewer_gui = trajectory_viewer_gui

    def get_epoch_length(self, epoch_number):
        return self.trajectory.get_epoch_length(epoch_number)

    def load_frame(self, epoch_number, frame_number):
        frame = self.trajectory.get_frame(epoch_number, frame_number)
        self.renderer.render_universe_update(self.universe, frame.configuration)
        cumulative_frame_number = self.trajectory.get_cumulative_frame_number(
                                    epoch_number, frame_number)
        self.plotter.update_plots(cumulative_frame_number)

    def show_plot(self, label):
        if label == PLOTS.bonds:
            self._handle_bonds_plot()
        elif label == PLOTS.angles:
            self._handle_angles_plot()
        else:
            pass
        if self.plotter:
            self.plotter.show_plot(label)

    def hide_plot(self, label):
        if self.plotter:
            self.plotter.hide_plot(label)

    def open_structure_measurements_dialog(self):
        dialog_name = struct_measure_gui.StructMeasure.name
        chimera.dialogs.display(dialog_name)

    def _handle_bonds_plot(self):
        dialog_name = struct_measure_gui.StructMeasure.name
        d = chimera.dialogs.find(dialog_name)
        if d is None:
            pass
        else:
            pseudobonds = d.distances
            mmtk_pseudobonds = []
            for bond in pseudobonds:
                mmtk_atoms = self.renderer.map_atoms_chimera2mmtk(bond.atoms)
                mmtk_pseudobonds.append(mmtk_atoms)
            self.mediator.compute_bond_distances(mmtk_pseudobonds)

    def _handle_angles_plot(self):
        dialog_name = struct_measure_gui.StructMeasure.name
        d = chimera.dialogs.find(dialog_name)
        if d is None:
            pass
        else:
            angles = d.angleInfo
            mmtk_angles = []
            for atoms in angles:
                mmtk_atoms = self.renderer.map_atoms_chimera2mmtk(atoms)
                mmtk_angles.append(mmtk_atoms)
            self.mediator.compute_angle_measures(mmtk_angles)
