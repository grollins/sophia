from chimera import Molecule, Element, Coord

# ===========
# = Classes =
# ===========
class _Converter:
    def __init__(self, universe):
        self.universe = universe

    def convert(self, defaultCS=None, usePDBnames=True):
        # First create the reference molecule
        self.usePDBnames = usePDBnames
        m = Molecule()
        if defaultCS is not None:
            m.activeCoordSet = m.newCoordSet(defaultCS)
        self._wholeSeq = 1
        self._mmtk2chimera = {}  # For updating coordinates
        self._chimera2mmtk = {}
        for obj in self.universe.objectList():
            if not obj.is_chemical_object:
                continue
            # Add the atoms
            try:
                resList = obj.residues()
            except:
                # No residues in the object, so just
                # fake one for the entire object
                self._addWholeMolecule(m, obj)
            else:
                # Add residues one by one
                self._addResidues(m, resList)
            # Add the bonds
            self._addBonds(m, obj)
        return m, self._mmtk2chimera, self._chimera2mmtk

    def _addWholeMolecule(self, m, obj):
        # This code is untested.  It "should" work.
        r = m.newResidue("UNK", "het", self._wholeSeq, " ")
        self._wholeSeq += 1
        self._addAtoms(m, r, obj)

    def _addResidues(self, m, resList):
        # This code is untested.  It "should" work.
        # This code has only been tested with "bala1" which happens
        # to be a protein whose atom names map well to PDB
        # conventions.  Changes will be needed for chains whose
        # residues are not amino acids or nucleic bases.
        for mr in resList:
            seq = mr.sequence_number
            rtype = mr.symbol
            if self.usePDBnames:
                rtype = rtype.upper()[:3]
            if mr.parent.is_chain:
                chainId = mr.parent.name
                if self.usePDBnames:
                    chainId = chainId.replace("chain", "")
                    chainId = chainId.upper()[:1]
            else:
                chainId = ' '
            r = m.newResidue(rtype, chainId, seq, " ")
            self._addAtoms(m, r, mr)

    def _addAtoms(self, m, r, obj):
        # The MMTK atom names look like "C_beta" while Chimera really
        # expects something like "CB".  So we pull the conversion
        # map out from the residue type's "pdbmap" attribute.
        # The data structures are a little convoluted, but constructed
        # "mmtk2pdb" is a dictionary whose keys are MMTK names
        # and whose values are PDB names.  If an MMTK name
        # is not found in mmtk2pdb, we just use the MMTK name and
        # hope for the best.
        mmtk2pdb = {}
        try:
            if not self.usePDBnames:
                raise AttributeError("skip PDB name code")
            for _, atomMap in obj.type.pdbmap:
                for name, atom in atomMap.iteritems():
                    mname = obj.type.atoms[atom.number].name
                    mmtk2pdb[mname] = name
        except AttributeError:
            pass
        for ma in obj.atomList():
            aname = mmtk2pdb.get(ma.name, ma.name)
            e = Element(ma.type.symbol)
            a = m.newAtom(aname, e)
            r.addAtom(a)
            x, y, z = ma.position() * 10
            # Multiply coordinates by 10 to convert from MMTK nm
            # to Chimera Angstrom
            a.setCoord(Coord(x, y, z))
            # Cache MMTK->Chimera atom mapping.  We can do this
            # using an attribute in ma, but I prefer to leave
            # MMTK instances pristine
            self._mmtk2chimera[ma] = a
            self._chimera2mmtk[a] = ma

    def _addBonds(self, m, obj):
        for mbu in obj.bondedUnits():
            try:
                bonds = mbu.bonds
            except AttributeError:
                continue
            for mb in bonds:
                try:
                    a1 = self._mmtk2chimera[mb.a1]
                    a2 = self._mmtk2chimera[mb.a2]
                except KeyError:
                    pass
                else:
                    b = m.newBond(a1, a2)


# =============
# = Functions =
# =============
def convert(universe):
    """Accepts an MMTK universe and returns (Chimera molecule,
    map from MMTK atom to Chimera atom, map from Chimera atom
    to MMTK atom) tuple"""
    molecule, mmtk2chimera_map, chimera2mmtk_map = _Converter(universe).convert()
    return molecule, mmtk2chimera_map, chimera2mmtk_map

