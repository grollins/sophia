from chimera.preset import preset
preset(allAtoms=True)
preset(publication=True, depthCued=False, scalingName="licorice")
