set bgColor #f0f0f0
set projection orthographic
unset depthCue
set subdivision 3.0
lighting mode two-point
lighting reflectivity 0.3
lighting sharpness 80.0
setattr a drawMode 1
