# Notes on setting up environment to compile C extensions

To create pyenv environment:
```
git clone https://github.com/pyenv/pyenv.git ~/.pyenv
git clone https://github.com/pyenv/pyenv-virtualenv.git \
  $HOME/.pyenv/plugins/pyenv-virtualenv

# add these lines to bash_profile
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

pyenv install 2.7.14
pyenv virtualenv 2.7.14 sophia2
```

If pyenv not running yet:
```
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"
pyenv activate sophia2
```

If another env is already active:
```
pyenv activate --unset
pyenv activate sophia2
```

python dependencies
```
pip install numpy==1.7.2
pip install cython==0.25.2
```

netCDF
```
brew install netcdf
```

Scientific
```
python setup.py build
python setup.py install
```

MMTK
```
MMTK_USE_CYTHON=1 python setup.py build
python setup.py install
```

Compiling langevin
```
python setup.py build_ext --inplace
```

Compiling monte carlo
(verify setup.py include_dirs path to mmtk)
```
python setup.py build_ext --inplace
```
