import sys
import subprocess

CHIMERA = "/Applications/Chimera.app/Contents/MacOS/chimera"
PYTHON = "/Applications/Chimera.app/Contents/MacOS/python2.7"

script_name = sys.argv[1]
chimera_cmd = [CHIMERA,
               "--nogui", "--silent", "--script", "%s" % script_name]
print(subprocess.check_output(chimera_cmd, stderr=subprocess.STDOUT))
