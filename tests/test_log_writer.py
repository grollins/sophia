import unittest
from numpy import linspace, zeros_like
from Sophia.simulation import LogWriter


class TestWriteDataToFile(unittest.TestCase):
    def setUp(self):
        self.filename = './temp.csv'
        time = linspace(0.0, 1.0, 100)
        potential = zeros_like(time) + -1.0
        kinetic = zeros_like(time) + 0.01
        self.data_columns_to_write = [time, potential, kinetic]
        self.data_types = [('time', float), ('potential', float),
                           ('kinetic', float)]

    def test_written_data_has_correct_number_of_columns(self):
        LW = LogWriter()
        LW.write_data(self.filename, self.data_columns_to_write,
                      self.data_types)
        with open(self.filename, 'r') as f:
            lines = f.readlines()
        self.assertEqual( len(lines), len(self.data_columns_to_write[0])+1 )
        self.assertEqual( len(lines), len(self.data_columns_to_write[1])+1 )
        self.assertEqual( len(lines), len(self.data_columns_to_write[2])+1 )


suite = unittest.TestLoader().loadTestsFromTestCase(TestWriteDataToFile)
unittest.TextTestRunner(verbosity=2).run(suite)
