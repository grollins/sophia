import os.path
import subprocess

CHIMERA = "/Applications/Chimera.app/Contents/MacOS/chimera"
PYTHON = "/Applications/Chimera.app/Contents/MacOS/python2.7"

cmd = [CHIMERA, "--nogui", "--silent", "--script", "tests/test_log_writer.py"]
print(subprocess.check_output(cmd, stderr=subprocess.STDOUT))
