from Tkinter import IntVar, StringVar, Frame, Button, Label, Checkbutton
from ttk import Treeview
from Pmw import Group, OptionMenu, NoteBook, EntryField, ButtonBox
from Pmw import alignlabels, logicalfont, ScrolledFrame
from tkFileDialog import askopenfilename, asksaveasfilename
from chimera.baseDialog import ModelessDialog


class SophiaSimulation(ModelessDialog):
    name = "sophia_simulation"
    buttons = ("Close")
    title = "Sophia Recipe"
    provideStatus = True

    def __init__(self):
        # super(SophiaSimulation, self).__init__()
        ModelessDialog.__init__(self)

    def fillInUI(self, parent):
        self.parent = parent

        self.padx = 10
        self.pady = 10

        # a scrollable frame to hold all the widgets
        scrolled_frame = \
            ScrolledFrame(parent, usehullsize=True, borderframe=False,
                          hull_width=780, hull_height=455)
        scrolled_frame.grid(row=0, column=0, sticky='w',
                            padx=self.padx, pady=self.pady)
        sf = scrolled_frame.interior()
        row_index = 0

        #  ========================
        #  = Create Recipe Widget =
        #  ========================
        f = Frame(sf)
        f.grid(row=row_index, column=0, columnspan=2,
               padx=self.padx, pady=self.pady, sticky='nw')
        g = Group(f, tag_text='Recipe', tagindent=0,
                  tag_font=logicalfont('Helvetica', 4))
        g.pack()
        bb = ButtonBox(g.interior(), orient='horizontal', padx=2, pady=0)
        bb.grid(row=0, column=0, padx=15, pady=(15,5), sticky='w')
        bb.add("Load Recipe...")
        bb.add("Save Recipe...")
        self.recipe_widget = \
            recipe_widget_factory.build(g.interior(), self)
        self.recipe_widget.grid(row=1, column=0, padx=15, pady=(5,5))
        bb = ButtonBox(g.interior(), orient='horizontal', padx=2, pady=0)
        bb.grid(row=2, column=0, padx=15, pady=(5,5))
        bb.add("New")
        bb.add("Clone")
        bb.add("Delete")
        bb.add(u"\u2191")
        bb.add(u"\u2193")
        bb.alignbuttons()


class RecipeWidgetFactory(object):
    """docstring for RecipeWidgetFactory"""
    def __init__(self):
        self.tree_col_width = 100

    def build(self, parent_frame, parent_dialog):
        rw = RecipeWidget(parent_frame, columns=('#','algorithm',
                          'num_steps', 'status'), selectmode='browse')
        rw.connect_to_parent_dialog(parent_dialog)
        rw.tree.column('#', width=40, anchor='center')
        rw.tree.column('algorithm', width=self.tree_col_width, anchor='center')
        rw.tree.column('num_steps', width=self.tree_col_width, anchor='center')
        rw.tree.column('status', width=self.tree_col_width, anchor='center')
        rw.tree.heading('#', text='#')
        rw.tree.heading('algorithm', text='Algorithm')
        rw.tree.heading('num_steps', text='Steps')
        rw.tree.heading('status', text='Status')
        return rw


class RecipeWidget(Treeview):
    """docstring for RecipeWidget"""
    def __init__(self, parent_frame, columns, selectmode):
        self.tree = Treeview(parent_frame, columns=columns,
                              selectmode=selectmode, show='headings',
                              height=10)
        # self.tree.bind('<<TreeviewSelect>>', self._item_selected)
        self.column_names = columns
        self.parent_dialog = None

    def connect_to_parent_dialog(self, parent_dialog):
        self.parent_dialog = parent_dialog

    def grid(self, row, column, padx, pady):
        self.tree.grid(row=row, column=column, padx=padx, pady=pady)

    def reset(self):
        for c in self.tree.get_children():
            self.tree.delete(c)

    def add_ingredient(self, ingredient):
        self.tree.insert('', 'end', ingredient['id'])
        self.tree.set(ingredient['id'], '#', '1')
        self.tree.set(ingredient['id'], 'algorithm', 'MD')
        self.tree.set(ingredient['id'], 'num_steps', '100')
        self.tree.set(ingredient['id'], 'status', 'OK')
        self.tree.focus(ingredient['id'])


dialog_name = 'test-sim-dialog'
chimera.dialogs.register(dialog_name, SophiaSimulation)
recipe_widget_factory = RecipeWidgetFactory()
chimera.dialogs.reregister(dialog_name, SophiaSimulation)
chimera.dialogs.display(dialog_name)
ss.recipe_widget.add_ingredient({'id': 'abc'})
ss.recipe_widget.add_ingredient({'id': 'xyz'})
