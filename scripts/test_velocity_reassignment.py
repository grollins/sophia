import numpy as np
import re
from os import getenv

from Scientific import N
from MMTK import Units, Random, ParticleProperties
from MMTK.Environment import NoseThermostat, AndersenBarostat
from MMTK.Trajectory import Trajectory, TrajectoryOutput, LogOutput, \
                            StandardLogOutput
from MMTK.Dynamics import VelocityVerletIntegrator, VelocityScaler, \
                          TranslationRemover, RotationRemover, Heater
from MMTK.PDB import PDBConfiguration, PDBOutputFile
from MMTK.Universe import CubicPeriodicUniverse, InfiniteUniverse
from MMTK.ForceFields import Amber99ForceField, Amber94ForceField, \
                             LennardJonesForceField
from MMTK.DCD import DCDReader, writeDCD
from MMTK import Database
Database.path = ['/home/geoff/src/sophia/Sophia/simulation/mmtk/Database']
MOLECULE_DICT = {'ETH': 'ethane', 'BUT': 'butane', 'NN': 'N2'}

'''
self.simulation.load_universe_from_pdb(
    pdb_filename, force_field_name, ff_mod_filename,
    box_length, cutoff)

which leads to the following steps in universe.py
'''

HOME = getenv('HOME')
pdb_filename = '%s/.aaa/sophia_tutorials/5-LJ10_MD/LJ10-0.pdb' % HOME
ff_mod_filename = '%s/.aaa/sophia_tutorials/5-LJ10_MD/LJ_E0_0.1.frcmod' % HOME
cutoff = 1.0 # nm
box_length = 2.0 # nm
ff = LennardJonesForceField(cutoff=cutoff)
universe = CubicPeriodicUniverse(box_length, ff)

with open(pdb_filename, 'r') as pdb_input:
    configuration = PDBConfiguration(pdb_input)
    universe.addObject(
        configuration.createAll(molecule_names=MOLECULE_DICT))

LJ_pattern = r'(\w+), ([0-9]*\.[0-9]+|[0-9]+), ([0-9]*\.[0-9]+|[0-9]+)'
LJ_mod_regex = re.compile(LJ_pattern)

with open(ff_mod_filename, 'r') as f:
    text = f.read()

    match_dict = {}
    for match in LJ_mod_regex.finditer(text):
        results = match.groups()
        atom_symbol = results[0]
        LJ_energy = float(results[1])
        LJ_rmin = float(results[2]) # nm
        LJ_sigma = 2 ** (-1./6) * LJ_rmin
        match_dict[atom_symbol] = {'LJ_energy': LJ_energy,
                                   'LJ_radius': LJ_sigma}

    atoms_to_modify = match_dict.keys()
    for o in universe:
        for a in o.atomList():
            if a.symbol in atoms_to_modify:
                a.LJ_energy = match_dict[a.symbol]['LJ_energy']
                a.LJ_radius = match_dict[a.symbol]['LJ_radius']

print universe.atomList()

'''

'''
initial_T = 100.0 * Units.K
universe.initializeVelocitiesToTemperature(initial_T)
print universe.temperature()

universe.configuration()
masses = universe.masses()
if universe._atom_properties.has_key('velocity'):
    del universe._atom_properties['velocity']
fixed = universe.getParticleBoolean('fixed')
num_points = universe.numberOfPoints()
velocities = N.zeros((num_points, 3), N.Float)
for i in xrange(num_points):
    m = masses[i]
    if m > 0. and not fixed[i]:
        v = Random.randomVelocity(initial_T, m)
        velocities[i] = np.array([v.x(), v.y(), 0.0])
universe._atom_properties['velocity'] = \
    ParticleProperties.ParticleVector(universe, velocities)
universe.adjustVelocitiesToConstraints()
print universe.temperature()

factor = np.sqrt(initial_T / universe.temperature())
universe.velocities().scaleBy(factor)
print universe.temperature()
