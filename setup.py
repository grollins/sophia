from distutils.core import setup

LONG_DESCRIPTION = \
"""
Sophia is a plugin to Chimera, a molecular modeling application developed at
UCSF. To run molecular simulations, Sophia uses the Molecular Modeling Toolkit
(MMTK), which is embedded as a library in Chimera.
"""

setup(name='sophia',
      version='0.7.1-mac',
      description='Sophia is an educational molecular simulation program',
      long_description=LONG_DESCRIPTION,
      author='Geoff Rollins',
      author_email='geoff@geoffrollins.com',
      url='http://sophia-web.appspot.com',
      license='MIT',
      platforms=['MacOS',],
      classifiers=['License :: OSI Approved :: MIT License',
                   'Operating System :: MacOS :: MacOS X',
                   'Operating System :: MacOS',
                   'Programming Language :: Python',
                   'Programming Language :: Python :: 2.7']
      )
