#!/bin/bash

wget http://storage.googleapis.com/sophia-dist/Sophia-0.5.1-linux.tar.gz
wget http://storage.googleapis.com/sophia-dist/Sophia-0.5.1-mac.tar.gz
wget http://storage.googleapis.com/sophia-dist/Sophia-0.5.1-win.tar.gz

tar xzf Sophia-0.5.1-linux.tar.gz
mv Sophia-0.5.1 linux_Sophia-0.5.2
tar xzf Sophia-0.5.1-mac.tar.gz
mv Sophia-0.5.1 mac_Sophia-0.5.2
tar xzf Sophia-0.5.1-win.tar.gz
mv Sophia-0.5.1 win_Sophia-0.5.2

cp ~/src/sophia/Sophia/chimeraUI/dialogs/sim.py linux_Sophia-0.5.2/Sophia/chimeraUI/dialogs/sim.py
cp ~/src/sophia/Sophia/chimeraUI/dialogs/sim.py mac_Sophia-0.5.2/Sophia/chimeraUI/dialogs/sim.py
cp ~/src/sophia/Sophia/chimeraUI/dialogs/sim.py win_Sophia-0.5.2/Sophia/chimeraUI/dialogs/sim.py

zip -r Sophia-0.5.2-linux.zip linux_Sophia-0.5.2
zip -r Sophia-0.5.2-mac.zip mac_Sophia-0.5.2
zip -r Sophia-0.5.2-win.zip win_Sophia-0.5.2

rm Sophia-0.5.1-linux.tar.gz
rm Sophia-0.5.1-mac.tar.gz
rm Sophia-0.5.1-win.tar.gz
rm -rf linux_Sophia-0.5.2
rm -rf mac_Sophia-0.5.2
rm -rf win_Sophia-0.5.2
