if __name__ == '__main__':
    raise RuntimeError('Sophia must be launched within Chimera')
else:
    from Sophia import launch_sophia_in_chimera
    launch_sophia_in_chimera()
